const data = [
    {
        "username": "Jeannie_23",
        "firstName": "Jeannie",
        "lastName": "Randolph",
        "password": "$3z&Se8Ph8",
    },
    {
        "username": "k_brown",
        "firstName": "Kathie",
        "lastName": "Brown",
        "password": "*7Gf5iNy7@",
    },
    {
        "username": "mega_powers",
        "firstName": "Powers",
        "lastName": "Justice",
        "password": "8*DkE87jb&",
    },
    {
        "username": "o.neal",
        "firstName": "Morales",
        "lastName": "Oneal",
        "password": "68Zp6-@vUp",
    },
    {
        "username": "A-dela",
        "firstName": "Adela",
        "lastName": "Heath",
        "password": "83-2pgJSj$",
    }];

const div = document.getElementById('root');

const form = document.createElement('form');
form.setAttribute('name', 'AuthorizationForm')
form.setAttribute('class', 'form');

const form__modal_wrapper = document.createElement('div');
form__modal_wrapper.setAttribute('class', 'form__modal_wrapper');

const firstName = document.createElement('input');
firstName.setAttribute('class', 'form__firstName form__input');
firstName.setAttribute('placeholder', 'firstname');

const lastName = document.createElement('input');
lastName.setAttribute('class', 'form__lastName form__input');
lastName.setAttribute('placeholder', 'lastname');

const password = document.createElement('input');
password.setAttribute('class', 'form__password form__input');
password.setAttribute('type', 'password');
password.setAttribute('placeholder', 'password');
password.setAttribute('required', 'true');

const button = document.createElement('button');
button.setAttribute('class', 'form__btn');
button.setAttribute('type', 'button');

const textnode = document.createTextNode('authorization');

button.append(textnode);

form__modal_wrapper.append(firstName);
form__modal_wrapper.append(lastName);
form__modal_wrapper.append(password);
form__modal_wrapper.append(button);

form.append(form__modal_wrapper);

root.append(form);

button.addEventListener('click', () => {
    sendData();
});

firstName.addEventListener('input', (e)=> {
    const regex = /^[A-Za-z-]/;
    let checkFirstName = regex.test(lastName.value);
    if (e.target.value.length < 2) {
        firstName.style.borderColor = 'red';
        firstName.style.boxShadow = '0 0 5px 5px red';
    } else {
        firstName.style.borderColor = 'green';
        firstName.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 10) {
        firstName.value = e.target.value.substr(0, 10);
    }
    if (checkFirstName !== true){
        firstName.style.borderColor = 'red';
        firstName.style.boxShadow = '0 0 5px 5px red';
    }
});

lastName.addEventListener('input', (e) => {
    const regex = /^[A-Za-z-]/;
    let checkLastName = regex.test(lastName.value);
    if (e.target.value.length < 2) {
        lastName.style.borderColor = 'red';
        lastName.style.boxShadow = '0 0 5px 5px red';
    } else {
        lastName.style.borderColor = 'green';
        lastName.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 20) {
        lastName.value = e.target.value.substr(0, 20);
    }
    if (checkLastName !== true) {
        lastName.style.borderColor = 'red';
        lastName.style.boxShadow = '0 0 5px 5px red';
    }
});

password.addEventListener('input', (e) => {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]/;
    let checkPassword = regex.test(password.value);
    if (e.target.value.length < 5) {
        password.style.borderColor = 'red';
        password.style.boxShadow = '0 0 5px 5px red';
    } else {
        password.style.borderColor = 'green';
        password.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 10) {
        password.value = e.target.value.substr(0, 10);
    }
    if (checkPassword !== true) {
        password.style.borderColor = 'red';
        password.style.boxShadow = '0 0 5px 5px red';
    }
});

window.addEventListener('keypress', (e) => {
    if (e.key === 'Enter') {
        sendData();
    }
})

function sendData() {
    let firstNameValue = firstName.value;
    let lastNameValue = lastName.value;
    let passwordValue = password.value;

    if (!firstName.value || !lastName.value || !password.value){
        if (!firstName.value) {
            firstName.style.borderColor = 'red';
        }
        if (!lastName.value) {
            lastName.style.borderColor = 'red';
        }
        if (!password.value) {
            password.style.borderColor = 'red';
        }
        alert('You input nothing!');
    } else {
       auth(firstNameValue, lastNameValue, passwordValue);
    }
}
function auth(firstName, lastName, password) {
    let response = false;
    let i = 0;
    let userName = '';
    while (i < data.length) {
        if (data[i].firstName === firstName && data[i].lastName === lastName && data[i].password === password) {
            userName = data[i].username;
            response = true;
            i = data.length + 1;
        }
        i++;
    }
    if (response === true) {
        alert('You have successfully as: ' + firstName + ", " + lastName);
    } else {
        alert('Auth error!!!');
    }
}