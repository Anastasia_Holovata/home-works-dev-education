import java.util.Map;

public class Request {

    private HttpMethod method;
    private Map<String, String> headers;
    private String body;

    public Request(HttpMethod method, Map<String, String> headers) {
        this.method = method;
        this.headers = headers;
    }

    public Request(HttpMethod method, Map<String, String> headers, String body) {
        this(method, headers);
        this.body = body;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Request{" +
               "method=" + method +
               ", headers=" + headers +
               ", body='" + body + '\'' +
               '}';
    }
}
