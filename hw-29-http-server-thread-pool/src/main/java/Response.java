import java.util.Optional;

public class Response {

    private String result;
    public Response() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        String body = Optional.ofNullable(result).orElse("");
        return "HTTP/1.1 200 OK\n\r" +
               "Content-Length: " + (body.length() + 1) + "\n\r" +
               "Content-Type: text/html; charset=utf-8\n\r\n\r" + body + "\n\r\n\r";
    }
}
