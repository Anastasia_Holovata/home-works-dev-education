import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Server {

    private ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        ThreadLogger.log("Created server socket on the port: " + port);
    }

    public void acceptClients() throws IOException {
        ExecutorService threadPool = new ThreadPoolExecutor(0, 5, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Socket clientSocket = serverSocket.accept();
                ThreadLogger.log("Accepted client socket.");
                threadPool.submit(new ClientHandler(clientSocket));
            }
        } finally {
            threadPool.shutdown();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }

    private static class ClientHandler implements Runnable {

        private final Socket clientSocket;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        private void handle() throws IOException {
            ThreadLogger.log("Try to parse client response.");
            Request request = RequestParser.parse(clientSocket.getInputStream());

            ThreadLogger.log("Try to process request: " + request);
            Response response = HttpController.processRequest(request);

            ThreadLogger.log("Try to send response: " + response);
            sendResponse(response);
        }

        private void sendResponse(Response response) throws IOException {
            OutputStreamWriter writer = new OutputStreamWriter(clientSocket.getOutputStream());
            writer.write(response.toString());
            writer.flush();
        }

        public void run() {
            try {
                handle();
            } catch(IOException e) {
                ThreadLogger.log("Caught exception when process client request. Exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
