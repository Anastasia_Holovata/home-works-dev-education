public class HttpController {

    public static Response processRequest(Request request) {
        Response response = new Response();
        switch (request.getMethod()) {
            case GET: get(request, response); break;
            case POST: post(request, response); break;
            case PUT: put(request, response); break;
            default: throw new RuntimeException("Bad request method!");
        }

        return response;
    }

    private static void get(Request request, Response response) {
        response.setResult("Response result after process http request: " + request.getMethod());
    }

    private static void post(Request request, Response response) {
        response.setResult("Response result after process http request: " + request.getMethod());
    }

    private static void put(Request request, Response response) {
        response.setResult("Response result after process http request: " + request.getMethod());
    }
}
