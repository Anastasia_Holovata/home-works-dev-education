import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class RequestParser {

    public static Request parse(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = bufferedReader.readLine();
        HttpMethod httpMethod = parseTopHeader(line);

        Map<String, String> headers = new HashMap<>();
        while (!(line = bufferedReader.readLine().trim()).equals("")) {
            parseCommonHeader(headers, line);
        }

        if (httpMethod == HttpMethod.POST || httpMethod == HttpMethod.PUT) {
            if (headers.containsKey("Content-Length")) {
                String contentLengthAsString = headers.get("Content-Length");
                if (contentLengthAsString != null) {
                    int contentLength = Integer.parseInt(contentLengthAsString);

                    if (contentLength > 0) {
                        return new Request(httpMethod, headers, new String(readBody(bufferedReader, contentLength)));
                    }
                }
            }
        }

        return new Request(httpMethod, headers);
    }

    private static HttpMethod parseTopHeader(String line) {
        String[] parts = line.split("\\s+", 3);
        if (parts.length != 3
            || !parts[1].startsWith("/")
            || !parts[2].toUpperCase().startsWith("HTTP/")) {
            throw new RuntimeException("Bad request! Incorrect top header.");
        }

        return HttpMethod.valueOf(parts[0]);
    }

    private static void parseCommonHeader(Map<String, String> headers, String line) {
        String[] pair = line.split(":", 2);
        if (pair.length != 2) {
            throw new RuntimeException("Bad request! Incorrect common header.");
        }
        String name = pair[0].replaceAll(" ", "");
        String value = pair[1].trim();
        headers.put(name, value);
    }

    private static byte[] readBody(BufferedReader inputStream, int contentLength) throws IOException {
        ByteArrayOutputStream bodyStream = new ByteArrayOutputStream();

        int nextByte;
        while (bodyStream.size() < contentLength) {
            nextByte = inputStream.read();
            bodyStream.write(nextByte);
        }
        return bodyStream.toByteArray();
    }
}
