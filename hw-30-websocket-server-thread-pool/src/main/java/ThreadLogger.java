public class ThreadLogger {

    public static void log(String message) {
        System.out.println("[Thread id: " + Thread.currentThread().getId() + "] " + message);
    }
}
