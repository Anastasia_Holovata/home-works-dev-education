import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Server {

    private ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        ThreadLogger.log("Created server socket on the port: " + port);
    }

    public void acceptClients() throws IOException {
        ExecutorService threadPool = new ThreadPoolExecutor(0, 5, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Socket clientSocket = serverSocket.accept();
                ThreadLogger.log("Accepted client socket.");
                threadPool.submit(new ClientHandler(clientSocket));
            }
        } finally {
            threadPool.shutdown();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }

    private static class ClientHandler implements Runnable {

        private final Socket clientSocket;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        private void handle() throws IOException, NoSuchAlgorithmException {
            InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream();

            WebSocketHandler.handShake(inputStream, outputStream);
            outputStream.write(WebSocketHandler.encode("Handshake done."));
            outputStream.flush();

            while (true) {
                try {
                    String message = WebSocketHandler.decode(inputStream);
                    if (message.isEmpty()) continue;
                    ThreadLogger.log("Message from client: " + message);
                    outputStream.write(WebSocketHandler.encode("Message from server: " + message));
                } catch(SocketTimeoutException e) {
                    ThreadLogger.log("Connection time out!");
                } catch(IOException e) {
                }
            }
        }

        public void run() {
            try {
                handle();
            } catch(IOException e) {
                ThreadLogger.log("Caught exception when process client request. Exception: " + e.getMessage());
                e.printStackTrace();
            } catch(NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }
}
