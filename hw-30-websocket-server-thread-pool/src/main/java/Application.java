import java.io.IOException;

public class Application {

    private static final int PORT = 8081;

    public static void main(String[] args) throws IOException {
        Server server = null;
        try {
            server = new Server(PORT);
            server.acceptClients();
        } catch(IOException e) {
            if (server != null) {
                server.stop();
            }
            e.printStackTrace();
        }
    }
}
