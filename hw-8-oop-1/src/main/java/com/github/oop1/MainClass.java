package com.github.oop1;

import com.github.oop1.device.Device;
import com.github.oop1.filters.DeviceFilter;
import com.github.oop1.memory.Memory;
import com.github.oop1.memory.MemoryParamName;
import com.github.oop1.processor.*;

import java.util.Arrays;

public class MainClass {

    public static void main(String[] args) {
        ProcessorArm processorArmOne = new ProcessorArm(3.5, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmTwo = new ProcessorArm(2.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorArm processorArmThree = new ProcessorArm(3.4, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorArm processorArmFour = new ProcessorArm(3.3, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmFive = new ProcessorArm(3.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        ProcessorX86 processorX86One = new ProcessorX86(3.1, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Two = new ProcessorX86(2.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorX86 processorX86Three = new ProcessorX86(3.3, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorX86 processorX86Four = new ProcessorX86(3.7, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Five = new ProcessorX86(3.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        Device[] devices = new Device[10];
        devices[0] = new Device(processorArmOne, new Memory());
        devices[1] = new Device(processorArmTwo, new Memory());
        devices[2] = new Device(processorArmThree, new Memory());
        devices[3] = new Device(processorArmFour, new Memory());
        devices[4] = new Device(processorArmFive, new Memory());
        devices[5] = new Device(processorX86One, new Memory());
        devices[6] = new Device(processorX86Two, new Memory());
        devices[7] = new Device(processorX86Three, new Memory());
        devices[8] = new Device(processorX86Four, new Memory());
        devices[9] = new Device(processorX86Five, new Memory());

        devices[0].save(new String[] {"1"});
        devices[1].save(new String[] {"1", "2"});
        devices[2].save(new String[] {"1", "2", "3"});
        devices[3].save(new String[] {"1", "2", "3", "4"});
        devices[4].save(new String[] {"1", "2", "3", "4", "5"});
        devices[5].save(new String[] {"1", "2", "3", "4", "5", "6"});
        devices[6].save(new String[] {"1", "2", "3", "4", "5", "6", "7"});
        devices[7].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8"});
        devices[8].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9"});
        devices[9].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});

        DeviceFilter filter = new DeviceFilter();

        System.out.println("Devices with processor architecture ARM: ");
        printArrayDevices(
                filter.filterByProcessorParam(
                        devices,
                        DeviceParamName.ARCHITECTURE,
                        ProcessorArchitectureType.ARM.name()));

        System.out.println("Devices with processor architecture X86: ");
        printArrayDevices(
                filter.filterByProcessorParam(
                        devices,
                        DeviceParamName.ARCHITECTURE,
                        ProcessorArchitectureType.X86.name()));

        System.out.println("Devices with processor frequency: ");
        printArrayDevices(
                filter.filterByProcessorParam(
                        devices,
                        DeviceParamName.FREQUENCY,
                        "3.3"));

        System.out.println("Devices with processor cache type L1: ");
        printArrayDevices(
                filter.filterByProcessorParam(
                        devices,
                        DeviceParamName.CACHE,
                        ProcessorCacheType.L1.name()));

        System.out.println("Devices with processor bit capacity 16: ");
        printArrayDevices(
                filter.filterByProcessorParam(
                        devices,
                        DeviceParamName.BIT_CAPACITY,
                        ProcessorBitCapacity.BIT_16.name()));

        System.out.println("Devices with COUNT_OF_AVAILABLE_CELLS greater then 0: ");
        printArrayDevices(
                filter.filterByMemoryParam(
                        devices,
                        MemoryParamName.COUNT_OF_AVAILABLE_CELLS,
                        0,
                        DeviceFilter.CompareType.GREATER));

        System.out.println("Devices with COUNT_OF_AVAILABLE_CELLS less then 3: ");
        printArrayDevices(
                filter.filterByMemoryParam(
                        devices,
                        MemoryParamName.COUNT_OF_AVAILABLE_CELLS,
                        3,
                        DeviceFilter.CompareType.LESS));

        System.out.println("Devices with PERCENT_OF_FULLNESS_MEMORY greater then 50: ");
        printArrayDevices(
                filter.filterByMemoryParam(
                        devices,
                        MemoryParamName.PERCENT_OF_FULLNESS_MEMORY,
                        50.0,
                        DeviceFilter.CompareType.GREATER));

        System.out.println("Devices with PERCENT_OF_FULLNESS_MEMORY less then 50: ");
        printArrayDevices(
                filter.filterByMemoryParam(
                        devices,
                        MemoryParamName.PERCENT_OF_FULLNESS_MEMORY,
                        50.0,
                        DeviceFilter.CompareType.LESS));
    }

    private static void printArrayDevices(Device[] devices) {
        Arrays.asList(devices).forEach(x -> System.out.println(x.getSystemInfo()));
    }
}
