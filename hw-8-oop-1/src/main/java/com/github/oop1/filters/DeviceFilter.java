package com.github.oop1.filters;

import com.github.oop1.device.Device;
import com.github.oop1.memory.MemoryParamName;
import com.github.oop1.processor.DeviceParamName;

import java.util.Arrays;

public class DeviceFilter {

    public enum CompareType {
        GREATER, LESS, EQUALS;
    }

    public Device[] filterByProcessorParam(Device[] devices, DeviceParamName paramName, String paramValue) {
        return Arrays.stream(devices)
                .filter(device -> paramValue.equals(parseAndGetProcessorValueByParam(device.getSystemInfo(), paramName.name())))
                .toArray(Device[]::new);
    }

    public Device[] filterByMemoryParam(
            Device[] devices,
            MemoryParamName paramName,
            double paramValue,
            CompareType compareType) {
        return Arrays.stream(devices)
                .filter(device -> {
                    double value = Double.parseDouble(parseAndGetProcessorValueByParam(device.getSystemInfo(), paramName.name()));
                    boolean isMatch = false;
                    switch (compareType) {
                        case LESS: isMatch = paramValue > value; break;
                        case GREATER: isMatch =  paramValue < value; break;
                        case EQUALS: isMatch=  paramValue == value; break;
                    }

                    return isMatch;
                }).toArray(Device[]::new);
    }

    private static String parseAndGetProcessorValueByParam(String processorDetails, String param) {
        String[] paramsNameAndValue = processorDetails.split(" ");
        for (String paramNameAndValue : paramsNameAndValue) {
            String[] paramNameAndValueAfterSplit = paramNameAndValue.split(":");
            String paramName = paramNameAndValueAfterSplit[0];
            String paramValue = paramNameAndValueAfterSplit[1];

            if (param.equals(paramName)) {
                return paramValue;
            }
        }

        throw new RuntimeException("Can't parse and processor params!");
    }
}
