package com.github.oop1.device;

import com.github.oop1.memory.Memory;
import com.github.oop1.memory.MemoryInfo;
import com.github.oop1.processor.Processor;
import com.github.oop1.utils.ElementInformationUtil;

import java.util.Objects;

public class Device {

    private Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data) {
        for (String datum : data) {
            memory.save(datum);
        }
    }

    public String[] readAll() {
        MemoryInfo memoryInfo = memory.getMemoryInfo();
        int countOfAvailableCells = memoryInfo.getCountOfAvailableCells();
        String[] memoryData = new String[countOfAvailableCells];

        for (int i = countOfAvailableCells - 1; i >= 0; i--) {
            memoryData[i] = memory.readLast();
            memory.removeLast();
        }

        return memoryData;
    }

    public void dataProcessing() {
        String[] memoryData = readAll();

        for (int i = 0; i < memoryData.length; i++) {
            memoryData[i] = processor.dataProcess(memoryData[i]);
        }

        for (int i = 0; i < memoryData.length; i++) {
            memory.removeLast();
        }

        for (String memoryDatum : memoryData) {
            memory.save(memoryDatum);
        }
    }

    public String getSystemInfo() {
        return new ElementInformationUtil(processor.getDetails())
                .addInfo(
                        "COUNT_OF_AVAILABLE_CELLS",
                        String.valueOf(memory.getMemoryInfo().getCountOfAvailableCells()))
                .addInfo(
                        "PERCENT_OF_FULLNESS_MEMORY",
                        String.valueOf(memory.getMemoryInfo().getPercentOfFullnessMemory()))
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Objects.equals(processor, device.processor) &&
                Objects.equals(memory, device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }
}
