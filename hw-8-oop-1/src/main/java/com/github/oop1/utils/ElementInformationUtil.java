package com.github.oop1.utils;

import java.util.ArrayList;
import java.util.List;

public class ElementInformationUtil {

    private List<String> paramsAsString = new ArrayList<>();

    public ElementInformationUtil() {}

    public ElementInformationUtil(String startInformation) {
        paramsAsString.add(startInformation);
    }


    public ElementInformationUtil addInfo(String paramName, String paramValue) {
        paramsAsString.add(paramName + ":" + paramValue + " ");
        return this;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        paramsAsString.forEach(sb::append);

        return sb.toString();
    }
}
