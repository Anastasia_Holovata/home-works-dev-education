package com.github.oop1.memory;

import java.util.Arrays;

public class Memory {

    private String[] memoryCell;

    public Memory() {
        memoryCell = null;
    }

    public String readLast() {
        initMemoryIfNotInitialized();
        String lastElement = null;
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                lastElement = memoryCell[i];
                break;
            }
        }
        return lastElement;
    }

    public String removeLast() {
        initMemoryIfNotInitialized();
        String lastElement = null;
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                lastElement = memoryCell[i];
                memoryCell[i] = null;
                break;
            }
        }
        return lastElement;
    }

    public boolean save(String data) {
        initMemoryIfNotInitialized();
        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] == null) {
                memoryCell[i] = data;
                return true;
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo() {
        initMemoryIfNotInitialized();
        int notEmptyCells = 0;

        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] != null) {
                notEmptyCells++;
            }
        }

        int countOfAvailableCells = memoryCell.length - notEmptyCells;
        double percentOfFullnessMemory = 100 / memoryCell.length * notEmptyCells;

        return new MemoryInfo(
                countOfAvailableCells,
                percentOfFullnessMemory);
    }

    private void initMemoryIfNotInitialized() {
        if (memoryCell == null) {
            memoryCell = new String[5];
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Memory memory = (Memory) o;
        return Arrays.equals(memoryCell, memory.memoryCell);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(memoryCell);
    }

    @Override
    public String toString() {
        return "Memory{" +
                "memoryCell=" + Arrays.toString(memoryCell) +
                '}';
    }
}
