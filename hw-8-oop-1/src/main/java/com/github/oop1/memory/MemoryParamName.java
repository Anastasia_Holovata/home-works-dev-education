package com.github.oop1.memory;

public enum MemoryParamName {
    COUNT_OF_AVAILABLE_CELLS,
    PERCENT_OF_FULLNESS_MEMORY;
}
