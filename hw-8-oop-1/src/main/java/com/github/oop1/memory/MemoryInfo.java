package com.github.oop1.memory;

import java.util.Objects;

public class MemoryInfo {

    private int countOfAvailableCells;
    private double percentOfFullnessMemory;

    public MemoryInfo(int countOfAvailableCells, double percentOfFullnessMemory) {
        this.countOfAvailableCells = countOfAvailableCells;
        this.percentOfFullnessMemory = percentOfFullnessMemory;
    }

    public int getCountOfAvailableCells() {
        return countOfAvailableCells;
    }

    public void setCountOfAvailableCells(int countOfAvailableCells) {
        this.countOfAvailableCells = countOfAvailableCells;
    }

    public double getPercentOfFullnessMemory() {
        return percentOfFullnessMemory;
    }

    public void setPercentOfFullnessMemory(double percentOfFullnessMemory) {
        this.percentOfFullnessMemory = percentOfFullnessMemory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryInfo that = (MemoryInfo) o;
        return countOfAvailableCells == that.countOfAvailableCells &&
                Double.compare(that.percentOfFullnessMemory, percentOfFullnessMemory) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(countOfAvailableCells, percentOfFullnessMemory);
    }
}
