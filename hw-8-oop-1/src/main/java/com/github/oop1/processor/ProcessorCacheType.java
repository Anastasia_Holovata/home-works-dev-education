package com.github.oop1.processor;

public enum ProcessorCacheType {
    L1, L2, L3;
}
