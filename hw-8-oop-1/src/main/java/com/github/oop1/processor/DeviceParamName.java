package com.github.oop1.processor;

public enum DeviceParamName {
    ARCHITECTURE,
    FREQUENCY,
    CACHE,
    BIT_CAPACITY,
}
