package com.github.oop1.processor;

import com.github.oop1.utils.ElementInformationUtil;

import java.util.Objects;

public abstract class Processor {

    private double frequency;
    private ProcessorCacheType cache;
    private ProcessorBitCapacity bitCapacity;

    public Processor(double frequency, ProcessorCacheType cache, ProcessorBitCapacity bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public String getDetails() {
        return new ElementInformationUtil()
                .addInfo(
                        DeviceParamName.FREQUENCY.name(),
                        String.valueOf(frequency))
                .addInfo(
                        DeviceParamName.CACHE.name(),
                        cache.name())
                .addInfo(
                        DeviceParamName.BIT_CAPACITY.name(),
                        bitCapacity.name())
                .build();
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor processor = (Processor) o;
        return Double.compare(processor.frequency, frequency) == 0 &&
                cache == processor.cache &&
                bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }

    @Override
    public String toString() {
        return "Processor{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }
}
