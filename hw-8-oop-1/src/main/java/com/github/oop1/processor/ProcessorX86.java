package com.github.oop1.processor;

import com.github.oop1.utils.ElementInformationUtil;

public class ProcessorX86 extends Processor {

    public static final String ARCHITECTURE = ProcessorArchitectureType.X86.name();

    public ProcessorX86(double frequency, ProcessorCacheType cache, ProcessorBitCapacity bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String getDetails() {
        return new ElementInformationUtil(super.getDetails())
                .addInfo(
                        DeviceParamName.ARCHITECTURE.name(),
                        ARCHITECTURE)
                .build();
    }

    @Override
    public String dataProcess(String data) {
        return "Data process in ProcessorX86 data: " + data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return "Long data in ProcessorX86: " + data;
    }
}
