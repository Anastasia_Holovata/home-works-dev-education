package com.github.oop1.processor;

public enum ProcessorBitCapacity {

    BIT_16(16), BIT_32(32), BIT_64(64);

    private int bitCapacity;

    ProcessorBitCapacity(int bitCapacity) {
        this.bitCapacity = bitCapacity;
    }

    public int getBitCapacity() {
        return bitCapacity;
    }

    public static ProcessorBitCapacity fromString(String text) {
        for (ProcessorBitCapacity processorBitCapacity : ProcessorBitCapacity.values()) {
            if (processorBitCapacity.bitCapacity == Integer.parseInt(text)) {
                return processorBitCapacity;
            }
        }
        return null;
    }
}
