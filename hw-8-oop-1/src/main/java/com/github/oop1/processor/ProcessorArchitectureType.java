package com.github.oop1.processor;

public enum ProcessorArchitectureType {
    ARM, X86;
}
