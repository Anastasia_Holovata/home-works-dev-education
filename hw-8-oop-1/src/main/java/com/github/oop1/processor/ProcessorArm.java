package com.github.oop1.processor;

import com.github.oop1.utils.ElementInformationUtil;

public class ProcessorArm extends Processor {

    private static final String ARCHITECTURE = ProcessorArchitectureType.ARM.name();

    public ProcessorArm(double frequency, ProcessorCacheType cache, ProcessorBitCapacity bitCapacity) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    public String getDetails() {
        return new ElementInformationUtil(super.getDetails())
                        .addInfo(
                                DeviceParamName.ARCHITECTURE.name(),
                                ARCHITECTURE)
                        .build();
    }

    @Override
    public String dataProcess(String data) {
        return "Data process in ProcessorArm data: " + data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return "Long data in ProcessorArm: " + data;
    }
}
