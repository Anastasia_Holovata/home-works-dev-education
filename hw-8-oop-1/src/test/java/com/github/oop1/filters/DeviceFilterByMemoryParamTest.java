package com.github.oop1.filters;

import com.github.oop1.device.Device;
import com.github.oop1.memory.Memory;
import com.github.oop1.memory.MemoryParamName;
import com.github.oop1.processor.ProcessorArm;
import com.github.oop1.processor.ProcessorBitCapacity;
import com.github.oop1.processor.ProcessorCacheType;
import com.github.oop1.processor.ProcessorX86;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class DeviceFilterByMemoryParamTest {

    private Device[] passedDevices;
    private MemoryParamName paramName;
    private double paramValue;
    private DeviceFilter.CompareType compareType;
    private Device[] expectedDevices;

    public DeviceFilterByMemoryParamTest(
            Device[] devices,
            MemoryParamName paramName,
            double paramValue,
            DeviceFilter.CompareType compareType,
            Device[] expectedDevices) {
        this.passedDevices = devices;
        this.paramName = paramName;
        this.paramValue = paramValue;
        this.compareType = compareType;
        this.expectedDevices = expectedDevices;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        Device[] devices = prepareDevices();
        return Arrays.asList(new Object[][]{
                {
                        devices,
                        MemoryParamName.COUNT_OF_AVAILABLE_CELLS,
                        0,
                        DeviceFilter.CompareType.GREATER,
                        new Device[]{devices[0], devices[1], devices[2], devices[3]}
                },
                {
                        devices,
                        MemoryParamName.COUNT_OF_AVAILABLE_CELLS,
                        3.0,
                        DeviceFilter.CompareType.LESS,
                        new Device[]{devices[2], devices[3], devices[4], devices[5], devices[6], devices[7], devices[8], devices[9]}
                },
                {
                        devices,
                        MemoryParamName.PERCENT_OF_FULLNESS_MEMORY,
                        50,
                        DeviceFilter.CompareType.GREATER,
                        new Device[]{devices[2], devices[3], devices[4], devices[5], devices[6], devices[7], devices[8], devices[9]}
                },
                {
                        devices,
                        MemoryParamName.PERCENT_OF_FULLNESS_MEMORY,
                        50,
                        DeviceFilter.CompareType.LESS,
                        new Device[]{devices[0], devices[1]}
                },
        });
    }

    @Test
    public void filterByProcessorParam() {
        assertArrayEquals(
                expectedDevices,
                new DeviceFilter()
                        .filterByMemoryParam(
                                passedDevices,
                                paramName,
                                paramValue,
                                compareType));
    }

    public static Device[] prepareDevices() {
        ProcessorArm processorArmOne = new ProcessorArm(3.5, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmTwo = new ProcessorArm(2.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorArm processorArmThree = new ProcessorArm(3.4, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorArm processorArmFour = new ProcessorArm(3.3, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmFive = new ProcessorArm(3.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        ProcessorX86 processorX86One = new ProcessorX86(3.1, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Two = new ProcessorX86(2.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorX86 processorX86Three = new ProcessorX86(3.3, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorX86 processorX86Four = new ProcessorX86(3.7, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Five = new ProcessorX86(3.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        Device[] devices = new Device[10];
        devices[0] = new Device(processorArmOne, new Memory());
        devices[1] = new Device(processorArmTwo, new Memory());
        devices[2] = new Device(processorArmThree, new Memory());
        devices[3] = new Device(processorArmFour, new Memory());
        devices[4] = new Device(processorArmFive, new Memory());
        devices[5] = new Device(processorX86One, new Memory());
        devices[6] = new Device(processorX86Two, new Memory());
        devices[7] = new Device(processorX86Three, new Memory());
        devices[8] = new Device(processorX86Four, new Memory());
        devices[9] = new Device(processorX86Five, new Memory());

        devices[0].save(new String[] {"1"});
        devices[1].save(new String[] {"1", "2"});
        devices[2].save(new String[] {"1", "2", "3"});
        devices[3].save(new String[] {"1", "2", "3", "4"});
        devices[4].save(new String[] {"1", "2", "3", "4", "5"});
        devices[5].save(new String[] {"1", "2", "3", "4", "5", "6"});
        devices[6].save(new String[] {"1", "2", "3", "4", "5", "6", "7"});
        devices[7].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8"});
        devices[8].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9"});
        devices[9].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});

        return devices;
    }
}