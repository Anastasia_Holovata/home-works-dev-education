package com.github.oop1.filters;

import com.github.oop1.device.Device;
import com.github.oop1.memory.Memory;
import com.github.oop1.processor.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class DeviceFilterByProcessParamTest {

    private Device[] passedDevices;
    private DeviceParamName paramName;
    private String paramValue;
    private Device[] expectedDevices;

    public DeviceFilterByProcessParamTest(Device[] devices, DeviceParamName paramName, String paramValue, Device[] expectedDevices) {
        this.passedDevices = devices;
        this.paramName = paramName;
        this.paramValue = paramValue;
        this.expectedDevices = expectedDevices;
    }

    @Test
    public void filterByProcessorParam() {
        assertArrayEquals(expectedDevices, new DeviceFilter().filterByProcessorParam(passedDevices, paramName, paramValue));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        Device[] devices = prepareDevices();
        return Arrays.asList(new Object[][] {
                {
                    devices,
                    DeviceParamName.ARCHITECTURE,
                    ProcessorArchitectureType.X86.name(),
                    new Device[] {devices[5], devices[6], devices[7], devices[8], devices[9]}
                },
                {
                        devices,
                        DeviceParamName.FREQUENCY,
                        "3.3",
                        new Device[] {devices[3], devices[7]}
                },
                {
                        devices,
                        DeviceParamName.CACHE,
                        ProcessorCacheType.L1.name(),
                        new Device[] {devices[0], devices[3], devices[5], devices[8]}
                },
                {
                        devices,
                        DeviceParamName.BIT_CAPACITY,
                        ProcessorBitCapacity.BIT_16.name(),
                        new Device[] {devices[0], devices[3], devices[5], devices[8]}
                },
        });
    }

    private static Device[] prepareDevices() {
        ProcessorArm processorArmOne = new ProcessorArm(3.5, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmTwo = new ProcessorArm(2.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorArm processorArmThree = new ProcessorArm(3.4, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorArm processorArmFour = new ProcessorArm(3.3, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorArm processorArmFive = new ProcessorArm(3.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        ProcessorX86 processorX86One = new ProcessorX86(3.1, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Two = new ProcessorX86(2.7, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);
        ProcessorX86 processorX86Three = new ProcessorX86(3.3, ProcessorCacheType.L3, ProcessorBitCapacity.BIT_64);
        ProcessorX86 processorX86Four = new ProcessorX86(3.7, ProcessorCacheType.L1, ProcessorBitCapacity.BIT_16);
        ProcessorX86 processorX86Five = new ProcessorX86(3.8, ProcessorCacheType.L2, ProcessorBitCapacity.BIT_32);

        Device[] devices = new Device[10];
        devices[0] = new Device(processorArmOne, new Memory());
        devices[1] = new Device(processorArmTwo, new Memory());
        devices[2] = new Device(processorArmThree, new Memory());
        devices[3] = new Device(processorArmFour, new Memory());
        devices[4] = new Device(processorArmFive, new Memory());
        devices[5] = new Device(processorX86One, new Memory());
        devices[6] = new Device(processorX86Two, new Memory());
        devices[7] = new Device(processorX86Three, new Memory());
        devices[8] = new Device(processorX86Four, new Memory());
        devices[9] = new Device(processorX86Five, new Memory());

        devices[0].save(new String[] {"1"});
        devices[1].save(new String[] {"1", "2"});
        devices[2].save(new String[] {"1", "2", "3"});
        devices[3].save(new String[] {"1", "2", "3", "4"});
        devices[4].save(new String[] {"1", "2", "3", "4", "5"});
        devices[5].save(new String[] {"1", "2", "3", "4", "5", "6"});
        devices[6].save(new String[] {"1", "2", "3", "4", "5", "6", "7"});
        devices[7].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8"});
        devices[8].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9"});
        devices[9].save(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});

        return devices;
    }
}