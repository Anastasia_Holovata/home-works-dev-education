package com.github.server;

import java.util.HashMap;
import java.util.Map;

public class SimpleHttpResponse {

    private String protocol;
    private int statusCode;
    private String statusText;
    private Map<String, String> headers = new HashMap<>();
    private String body;

    public SimpleHttpResponse() {

    }

    public SimpleHttpResponse(
            String protocol,
            int statusCode,
            String statusText,
            Map<String, String> headers,
            String body) {
        this.protocol = protocol;
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.headers = headers;
        this.body = body;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void addHeader(String key, String value) {
        headers.put(key, value);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder
                .append(this.protocol)
                .append(" ")
                .append(this.statusCode)
                .append(" ")
                .append(this.statusText)
                .append("\r\n");

        for (String header : this.headers.keySet()) {
            builder
                    .append(header)
                    .append(":")
                    .append(this.headers.get(header))
                    .append("\r\n");
        }

        builder
                .append("\r\n")
                .append(this.body)
                .append("\r\n\r\n");

        return builder.toString();
    }
}
