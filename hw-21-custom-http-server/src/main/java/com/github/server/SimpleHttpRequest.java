package com.github.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class SimpleHttpRequest {

    private static final int INDEX_OF_METHOD_VALUE = 0;
    private static final int INDEX_OF_PROTOCOL_VALUE = 1;

    private static final String METHOD_POST = "POST";
    private static final String METHOD_PUT = "PUT";

    private static final String HEADER_CONTENT_LENGTH = "Content-Length";

    private final String method;
    private final String protocol;
    private final Map<String, String> headers = new HashMap<>();
    private Optional<String> body = Optional.empty();

    public SimpleHttpRequest(BufferedReader reader) throws IOException {
        List<String> requestDataLines = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null && (!line.isEmpty())) {
            System.out.println(line);
            requestDataLines.add(line);
        }

        if (requestDataLines.size() == 0) {
            throw new RuntimeException("");
        }

        String[] methodAndProtocolDataAsStringArray = requestDataLines.get(0).split("/");

        if (methodAndProtocolDataAsStringArray.length > 1) {
            method = methodAndProtocolDataAsStringArray[INDEX_OF_METHOD_VALUE].trim();
            protocol = methodAndProtocolDataAsStringArray[INDEX_OF_PROTOCOL_VALUE].trim();
        } else {
            throw new RuntimeException("");
        }

        requestDataLines.remove(0);

        requestDataLines.forEach(requestLineDataAsString -> {
            String[] headerDataPair = requestLineDataAsString.split(":");

            if (headerDataPair.length == 2) {
                headers.put(headerDataPair[0].trim(), headerDataPair[1].trim());
            }
        });

        if (method.equals(METHOD_POST) || method.equals(METHOD_PUT)) {
            if (headers.containsKey(HEADER_CONTENT_LENGTH)) {
                int contentLength = Integer.parseInt(headers.get(HEADER_CONTENT_LENGTH));
                char[] bodyAsCharArray = new char[contentLength];
                reader.read(bodyAsCharArray, 0, contentLength);
                body = Optional.of(new String(bodyAsCharArray));
            }
        }
    }

    public String getMethod() {
        return method;
    }

    public String getProtocol() {
        return protocol;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Optional<String> getBody() {
        return body;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder
                .append(this.method)
                .append(" / ")
                .append(this.protocol)
                .append("\r\n");

        for (String header : this.headers.keySet()) {
            builder
                    .append(header)
                    .append(":")
                    .append(this.headers.get(header))
                    .append("\r\n");
        }

        builder
                .append("\r\n")
                .append(this.body)
                .append("\r\n\r\n");

        return builder.toString();
    }
}
