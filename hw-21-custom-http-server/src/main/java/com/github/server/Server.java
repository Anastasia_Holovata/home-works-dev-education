package com.github.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public void start(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);

        while (true) {
            Socket socket = serverSocket.accept();

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            SimpleHttpRequest request = new SimpleHttpRequest(reader);
            SimpleHttpResponse response = RequestHandler.handle(request, new SimpleHttpResponse());

            writer.write(response.toString());

            writer.close();
            reader.close();
            socket.close();
        }
    }
}
