package com.github.server;

public class Application {

    public static void main(String[] args) {
        try {
            new Server().start(4321);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
