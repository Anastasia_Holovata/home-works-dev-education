package com.github.websocket;

import com.github.handler.DataHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebSocket {
    private static final List<Thread> threads = new ArrayList<>();

    private static final List<Thread> syncList = Collections.synchronizedList(threads);

    private static final LinkedBlockingQueue<String> messageQueue = new LinkedBlockingQueue<>();

    private static volatile boolean isRunning = true;

    private static ServerSocket server;

    private static Thread threadHandler;

    private static int pingTimeout;

    public void connect(int port, int pingTimeout) throws IOException {
        server = new ServerSocket(port);
        this.pingTimeout = pingTimeout;
        threadHandler = new ThreadHandler();
        threadHandler.start();
    }

    public void connect(int port, int backlog, InetAddress bindAddress, int pingTimeout) throws IOException {
        server = new ServerSocket(port, backlog, bindAddress);
        this.pingTimeout = pingTimeout;
        threadHandler = new ThreadHandler();
        threadHandler.start();
    }

    public boolean sendMessage(String message) throws IOException {
        DataHandler dh = new DataHandler();
        byte[] byteMsg = message.getBytes(StandardCharsets.UTF_8);
        byte[] framedMsg = dh.generateFrame(byteMsg);

        if (syncList.isEmpty()) {
            return false;
        }
        for (Thread thread : syncList) {
            ClientConnection con = (ClientConnection) thread;
            con.sendMessage(framedMsg);
        }
        return true;
    }

    public String receiveMessage() {
        String message = null;
        try {
            message = messageQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return message;
    }

    public void close() throws IOException {
        isRunning = false;
        threadHandler.interrupt();
        server.close();
    }

    private void removeClient(ClientConnection client) {
        for (int i = 0; i < syncList.size(); i++) {
            if (syncList.get(i).equals(client)) {
                syncList.remove(i);
            }
        }
    }

    private class ClientConnection extends Thread {
        private final Socket client;

        private InputStream in;

        private OutputStream out;

        private final DataHandler handler;

        private volatile boolean isClosing = false;

        private boolean ping = false;

        private ClientConnection(Socket client, int pingTimeout) throws SocketException {
            this.client = client;
            client.setSoTimeout(pingTimeout);
            this.handler = new DataHandler();
        }

        private void setIsClosing() {
            this.isClosing = true;
        }

        public void run() {
            try {
                in = client.getInputStream();
                out = client.getOutputStream();
                if (!handshake(in, out)) {

                    throw new IOException("Error connecting to client");
                }
                while (!isClosing) {
                    try {
                        byte type = (byte) in.read();
                        int opcode = type & 0x0F;
                        switch (opcode) {
                            case 0x1:
                                byte[] message = readTextMessage();
                                String messageStr = new String(message, StandardCharsets.UTF_8);
                                messageQueue.add(messageStr);
                                break;
                            case 0x9:
                                readControlMessage();
                                sendMessage(handler.generateStatusFrame("PONG"));
                                break;
                            case 0xA:
                                readControlMessage();
                                ping = false;
                                break;
                            case 0x8:
                                readControlMessage();
                                isClosing = true;
                                removeClient(this);
                                break;
                            default:
                                throw new IllegalArgumentException("Unsupported message type.");
                        }

                    } catch (SocketTimeoutException ste) {
                        if (ping) {
                            isClosing = true;
                            System.out.println("ping: have already sent ping...");
                        } else {
                            byte[] msgBack = handler.generateStatusFrame("PING");
                            sendMessage(msgBack);
                            ping = true;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (client.isConnected()) {
                    try {
                        byte[] closingframe = handler.generateStatusFrame("CLOSE");
                        sendMessage(closingframe);
                        out.close();
                        in.close();
                        client.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private boolean handshake(InputStream in, OutputStream out) {
            try {
                String data = new Scanner(in, StandardCharsets.UTF_8).useDelimiter("\\r\\n\\r\\n").next();

                String get = "GET";
                String httpVersion = "HTTP/1.1";
                String upgrade = "Upgrade: websocket";
                String connection = "Connection: Upgrade";

                Pattern getPat = Pattern.compile(get);
                Pattern httpVersionPat = Pattern.compile(httpVersion);
                Pattern upgradePat = Pattern.compile(upgrade);
                Pattern connectionPat = Pattern.compile(connection);

                Matcher getMatch = getPat.matcher(data);
                Matcher httpVersionMatch = httpVersionPat.matcher(data);
                Matcher upgradeMatch = upgradePat.matcher(data);
                Matcher connectionMatch = connectionPat.matcher(data);

                if (!getMatch.find() || !httpVersionMatch.find() || !upgradeMatch.find() || !connectionMatch.find()) {
                    String response = handler.badRequestResponse();
                    byte[] responseByte = response.getBytes();
                    out.write(responseByte);
                    return false;
                }

                String keyName = "Sec-WebSocket-Key: ";
                Pattern keyPat = Pattern.compile(keyName);
                Matcher match = keyPat.matcher(data);
                boolean isMatch = match.find();

                if (!isMatch) {
                    String response = handler.badRequestResponse();
                    byte[] responseByte = response.getBytes();
                    out.write(responseByte);
                    return false;
                }
                String key = data.substring(match.end(), match.end() + 24);

                String response = handler.generateServerResponse(key);
                byte[] responseByte = response.getBytes();
                out.write(responseByte);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        private byte[] readControlMessage() throws IOException {
            byte lengthRead = (byte) in.read();
            if (((lengthRead >>> 7) & 0xFF) == 0) {
                throw new IllegalArgumentException("Unmasked message from client");
            }
            int length = (0x000000FF) & lengthRead - 128;
            byte[] input = new byte[4 + length];
            in.read(input, 0, input.length);
            return handler.unmaskData(input);
        }

        private byte[] readTextMessage() throws IOException {
            int length = (0x000000FF) & in.read() - 128;

            if (length == 126) {
                byte[] buffer = new byte[2];
                buffer[0] = (byte) in.read();
                buffer[1] = (byte) in.read();
                length = (buffer[0] & 0xFF) << 8 | (buffer[1] & 0xFF);

            } else if (length == 127) {
                throw new IllegalArgumentException("Message too large.");
            }

            byte[] input = new byte[4 + length];
            in.read(input);

            return handler.unmaskData(input);
        }

        private void sendMessage(byte[] message) throws IOException {
            out.write(message, 0, message.length);
            out.flush();
        }
    }

    private class ThreadHandler extends Thread {

        public void run() {
            while (isRunning) {
                Socket connection = null;

                try {
                    connection = server.accept();
                } catch (SocketException e) {
                    System.out.println("Server is closed");
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Thread client = null;

                try {
                    client = new ClientConnection(connection, pingTimeout);
                } catch (SocketException e) {
                    e.printStackTrace();
                }
                if (!(client == null)) {
                    syncList.add(client);
                    client.start();
                }
            }
            for (Thread thread : syncList) {
                ClientConnection client = (ClientConnection) thread;
                client.setIsClosing();
            }
        }
    }
}
