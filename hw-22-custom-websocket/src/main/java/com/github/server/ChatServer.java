package com.github.server;

import com.github.websocket.WebSocket;

import java.io.IOException;

public class ChatServer extends WebSocket {

    public static void main(String[] args) throws IOException {
        ChatServer server = new ChatServer();
        try {
            server.connect(3001, 5000);

            for (int i = 0; i < 5; i++) {
                server.displayChat();
            }
            server.disconnect();
        } catch (IOException e) {
            server.close();
        }
    }

    public void sendToAll(String message) throws IOException {
        sendMessage(message);
    }

    public void displayChat() throws IOException {
        String message = receiveMessage();
        sendMessage(message);
    }

    public void disconnect() throws IOException {
        sendToAll("The server is disconnected.");
        close();
    }
}
