package com.github.server;

import com.github.websocket.WebSocket;

import java.io.IOException;

public class EchoServer extends WebSocket {

    public static void main(String[] args) throws IOException {
        EchoServer server = new EchoServer();
        server.connect(3001, 5000);

        server.echoMessage();
    }

    public void echoMessage() throws IOException {
        while (true) {
            String message = receiveMessage();
            sendMessage("You said: " + message);
            message = message.toUpperCase();
            sendMessage("Echo: " + message);
        }
    }
}
