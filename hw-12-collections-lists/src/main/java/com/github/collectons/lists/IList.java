package com.github.collectons.lists;

public interface IList {

    void init(int[] init);

    void clear();

    int size();

    int[] toArray();

    void addStart(int value);

    void addEnd(int value);

    void addByPos(int index, int value);

    int removeStart();

    int removeEnd();

    int removeByPos(int index);

    int max();

    int min();

    int maxPos();

    int minPos();

    int[] sort();

    int get(int pos);

    int[] halfReverse();

    int[] reverse();

    void set(int index, int value);
}
