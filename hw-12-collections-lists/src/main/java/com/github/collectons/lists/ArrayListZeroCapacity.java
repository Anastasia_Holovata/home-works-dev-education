package com.github.collectons.lists;

import com.github.collectons.lists.exception.ListEmptyException;

public class ArrayListZeroCapacity implements IList {

    private int[] array = new int[0];

    public void init(int[] init) {
        if (init == null) {
            this.array = new int[0];
        } else {
            this.array = new int[init.length];
            for (int i = 0; i < init.length; i++) {
                this.array[i] = init[i];
            }
        }
    }

    public void clear() {
        this.array = new int[0];
    }

    public int size() {
        return this.array.length;
    }

    public int[] toArray() {
        int size = size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            str.append(this.array[i]).append(", ");
        }
        return str.toString();
    }

    public void addStart(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        int[] newArray = new int[size + 1];
        for (int i = 0; i < size; i++) {
            newArray[i + 1] = this.array[i];
        }
        newArray[0] = val;
        this.array = newArray;
    }

    public void addEnd(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        int[] newArray = new int[size + 1];
        for (int i = 0; i < size; i++) {
            newArray[i] = this.array[i];
        }
        newArray[size] = val;
        this.array = newArray;
    }

    public void addByPos(int pos, int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        if (pos < 0 || pos > size) {
            throw new ListEmptyException();
        }

        int[] newArray = new int[size + 1];
        if (pos == 0) {
            addStart(val);
        } else if (pos == size) {
            addEnd(val);
        } else {
            for (int i = 0; i < size + 1; i++) {
                if (i < pos)
                    newArray[i] = this.array[i];
                else if (i == pos)
                    newArray[i] = val;
                else
                    newArray[i] = this.array[i - 1];
            }
        }
        this.array = newArray;
    }

    public int removeStart() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size - 1];

        for (int i = 0; i < size - 1; i++) {
            newArray[i] = this.array[i + 1];
        }
        int tmp = this.array[0];
        this.array = newArray;

        return tmp;
    }

    public int removeEnd() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size - 1];

        for (int i = 0; i < size - 1; i++) {
            newArray[i] = this.array[i];
        }
        int tmp = this.array[size - 1];
        this.array = newArray;

        return tmp;
    }

    public int removeByPos(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size - 1];
        int tmp = this.array[pos];


        if (pos == 0) {
            removeStart();
        } else if (pos == size - 1) {
            removeEnd();
        } else {
            for (int i = 0; i < size - 1; i++) {
                if (i < pos) {
                    newArray[i] = this.array[i];
                } else if (i >= pos) {
                    newArray[i] = this.array[i + 1];
                }
            }
            this.array = newArray;
        }
        return tmp;
    }

    public int max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];

        for (int i = 1; i < size; i++) {
            if (this.array[i] > max)
                max = this.array[i];
        }
        return max;
    }

    public int min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];

        for (int i = 1; i < size; i++) {
            if (this.array[i] < min)
                min = this.array[i];
        }
        return min;
    }

    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int maxPosition = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i] > this.array[maxPosition])
                maxPosition = i;
        }
        return maxPosition;
    }

    public int minPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int minPosition = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i] < this.array[minPosition])
                minPosition = i;
        }
        return minPosition;
    }

    public int[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        int temp;
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (this.array[j] < this.array[j - 1]) {
                    temp = this.array[j];
                    this.array[j] = this.array[j - 1];
                    this.array[j - 1] = temp;
                }
            }
        }
        return this.array;
    }

    public int get(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new ListEmptyException();
        }

        return this.array[pos];
    }

    public int[] halfReverse() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size];

        double doubleHalfPartIndex = ((double) size) / 2;
        int halfPartIndex = (int) doubleHalfPartIndex;

        boolean isNotEqualsPart = doubleHalfPartIndex % 2 != 0;

        for (int i = 0, y = isNotEqualsPart ? halfPartIndex + 1 : halfPartIndex; i < halfPartIndex; i++, y++) {
            newArray[i] = array[y];
        }

        for (int i = 0, y = halfPartIndex; y < size; i++, y++) {
            newArray[y] = array[i];
        }

        this.array = newArray;

        return newArray;
    }

    public int[] reverse() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        for (int i = 0; i < size / 2; i++) {
            int temp = this.array[i];
            this.array[i] = this.array[size - 1 - i];
            this.array[size - 1 - i] = temp;
        }
        return this.array;
    }

    public void set(int pos, int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos >= 0 || pos < size) {
            this.array[pos] = val;
        } else {
            throw new ListEmptyException();
        }
    }
}
