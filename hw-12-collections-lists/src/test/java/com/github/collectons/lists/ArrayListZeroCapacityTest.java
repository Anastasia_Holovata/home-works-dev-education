package com.github.collectons.lists;

public class ArrayListZeroCapacityTest extends AbstractListTest {

    @Override
    IList initList(int[] array) {
        IList list = new ArrayListZeroCapacity();
        list.init(array);
        return list;
    }
}