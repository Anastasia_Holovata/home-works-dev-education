package com.github.collectons.lists;

public class LinkedListOnlyNextTest extends AbstractListTest {

    @Override
    IList initList(int[] array) {
        IList list = new LinkedListOnlyNext();
        list.init(array);
        return list;
    }
}