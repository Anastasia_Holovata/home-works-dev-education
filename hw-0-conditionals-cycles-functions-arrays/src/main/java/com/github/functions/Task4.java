package com.github.functions;

/**
 * Задача №4
 * Найти расстояние между двумя точками в двумерном декартовом пространстве.
 */
public class Task4 {

    public static void main(String[] args) {
        Point pointOne = new Point(-2, 4);
        Point pointTwo = new Point(8, 5);

        System.out.println("Distance between " +
                "point [x:" + pointOne.getX() + "; y:" + pointOne.getY() + "] "
                + "and point [x:" + pointTwo.getX() + "; y:" + pointTwo.getY() + "] = "
                + calculateDistanceBetweenTwoPoints(pointOne, pointTwo));
    }

    public static double calculateDistanceBetweenTwoPoints(Point pointOne, Point pointTwo) {
        return Math.sqrt(Math.pow((pointTwo.getX() - pointOne.getX()), 2) + Math.pow((pointTwo.getY() - pointOne.getY()), 2));
    }

    public static class Point {

        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
