package com.github.functions;

import java.util.*;

/**
 * Задача №3
 * Вводим строку, которая содержит число, написанное прописью (0-999).
 * Получить само число
 */
public class Task3 {

    private static Map<String, Integer> digits;
    private static Map<String, Integer> tens;
    private static Map<String, Integer> divisibleByTen;

    static {
        digits = new HashMap<>();
        digits.put("zero", 0);
        digits.put("one", 1);
        digits.put("two", 2);
        digits.put("three", 3);
        digits.put("four", 4);
        digits.put("five", 5);
        digits.put("six", 6);
        digits.put("seven", 7);
        digits.put("eight", 8);
        digits.put("nine", 9);

        tens = new HashMap<>();
        tens.put("ten", 10);
        tens.put("eleven", 11);
        tens.put("twelve", 12);
        tens.put("thirteen", 13);
        tens.put("fourteen", 14);
        tens.put("fifteen", 15);
        tens.put("sixteen", 16);
        tens.put("seventeen", 17);
        tens.put("eighteen", 18);
        tens.put("nineteen", 19);

        divisibleByTen = new HashMap<>();
        divisibleByTen.put("twenty", 2);
        divisibleByTen.put("thirty", 3);
        divisibleByTen.put("forty", 4);
        divisibleByTen.put("fifty", 5);
        divisibleByTen.put("sixty", 6);
        divisibleByTen.put("seventy", 7);
        divisibleByTen.put("eighty", 8);
        divisibleByTen.put("ninety", 9);
    }

    public static void main(String[] args) {
        System.out.println("zero" + " = " + parseStringNumberToInt("zero"));
        System.out.println("seven" + " = " + parseStringNumberToInt("seven"));
        System.out.println("eleven" + " = " + parseStringNumberToInt("eleven"));
        System.out.println("sixty" + " = " + parseStringNumberToInt("sixty"));
        System.out.println("sixty eight" + " = " + parseStringNumberToInt("sixty eight"));
        System.out.println("one hundred" + " = " + parseStringNumberToInt("one hundred"));
        System.out.println("two hundred five" + " = " + parseStringNumberToInt("two hundred five"));
        System.out.println("seven hundred sixty eight" + " = " + parseStringNumberToInt("seven hundred sixty eight"));
        System.out.println("nine hundred twenty" + " = " + parseStringNumberToInt("nine hundred twenty"));
    }

    public static int parseStringNumberToInt(String stringNumber) {
        List<String> stringNumbersList = Arrays.asList(stringNumber.split(" "));
        Collections.reverse(stringNumbersList);

        List<String> result = new ArrayList<>();
        for (int i = 0; i < stringNumbersList.size(); i++) {
            if (i == 0) {
                if (stringNumbersList.get(i).equals("hundred")) {
                    result.add(digits.get(stringNumbersList.get(i + 1)) + "00");
                    break;
                } else if (stringNumbersList.size() == 1 && tens.containsKey(stringNumbersList.get(i))) {
                    result.add(String.valueOf(tens.get(stringNumbersList.get(i))));
                } else {
                    if (stringNumbersList.size() <= 1 || !tens.containsKey(stringNumbersList.get(i))) {
                        if (divisibleByTen.containsKey(stringNumbersList.get(i))) {
                            result.add(divisibleByTen.get(stringNumbersList.get(i)) + "0");
                        } else {
                            result.add(String.valueOf(digits.get(stringNumbersList.get(i))));
                        }
                    }
                }
            } else if (i == 1) {
                if (stringNumbersList.get(i).equals("hundred")) {
                    if (tens.containsKey(stringNumbersList.get(0))) {
                        result.add(String.valueOf(digits.get(stringNumbersList.get(i + 1))));
                        result.add(String.valueOf(tens.get(stringNumbersList.get(0))));
                    } else if (divisibleByTen.containsKey(stringNumbersList.get(0))) {
                        result.add(String.valueOf(digits.get(stringNumbersList.get(i + 1))));
                    } else {
                        result.add(digits.get(stringNumbersList.get(i + 1)) + "0");
                    }
                } else if (tens.containsKey(stringNumbersList.get(i))) {
                    result.add(String.valueOf(tens.get(stringNumbersList.get(i))));
                } else {
                    result.add(String.valueOf(divisibleByTen.get(stringNumbersList.get(i))));
                }
            } else if (i == 2 && stringNumbersList.get(i).equals("hundred")) {
                result.add(String.valueOf(digits.get(stringNumbersList.get(i + 1))));
            }
        }

        Collections.reverse(result);
        String myReducedString = String.join("", result);
        return Integer.parseInt(myReducedString);
    }
}

