package com.github.functions;

/**
 * Задача №1
 * Получить строковое название дня недели по номеру дня.
 */
public class Task1 {

    public static void main(String[] args) {
        System.out.println("The days of week that corresponds to inputted number:");
        System.out.println("Number 1 - " + getStringTheDayOfWeek(1));
        System.out.println("Number 3 - " + getStringTheDayOfWeek(3));
        System.out.println("Number 6 - " + getStringTheDayOfWeek(6));
    }

    public static String getStringTheDayOfWeek(int number) {
        switch (number) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "Not found";
        }
    }
}

