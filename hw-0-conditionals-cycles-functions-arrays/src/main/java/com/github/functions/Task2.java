package com.github.functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Задача №2
 * Вводим число (0-999), получаем строку с прописью числа.
 */
public class Task2 {

    private static String[] digits = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    private static String[] tens =
            {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private static String[] divisibleByTen = {null, null, "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private static String HUNDRED = "hundred";

    public static void main(String[] args) {
        System.out.println(0 + " = " + parseNumberToLine(0));
        System.out.println(7 + " = " + parseNumberToLine(7));
        System.out.println(10 + " = " + parseNumberToLine(10));
        System.out.println(17 + " = " + parseNumberToLine(17));
        System.out.println(20 + " = " + parseNumberToLine(20));
        System.out.println(68 + " = " + parseNumberToLine(68));
        System.out.println(100 + " = " + parseNumberToLine(100));
        System.out.println(105 + " = " + parseNumberToLine(105));
        System.out.println(110 + " = " + parseNumberToLine(110));
        System.out.println(113 + " = " + parseNumberToLine(113));
        System.out.println(120 + " = " + parseNumberToLine(120));
        System.out.println(168 + " = " + parseNumberToLine(168));
        System.out.println(300 + " = " + parseNumberToLine(300));
        System.out.println(309 + " = " + parseNumberToLine(309));
        System.out.println(310 + " = " + parseNumberToLine(310));
        System.out.println(900 + " = " + parseNumberToLine(900));
        System.out.println(999 + " = " + parseNumberToLine(999));
    }

    public static String parseNumberToLine(int number) {
        String stringNumber = String.valueOf(number);
        char[] numberAsCharArray = stringNumber.toCharArray();

        List<Integer> listOfNumberInts = new ArrayList<>();
        for (char c : numberAsCharArray) {
            listOfNumberInts.add(Character.getNumericValue(c));
        }

        Collections.reverse(listOfNumberInts);

        List<String> listOfNumberNames = new ArrayList<>();

        for (int i = 0; i < listOfNumberInts.size(); i++) {
            if (listOfNumberInts.get(i) == 0 && i != 0) {
                continue;
            }
            if (i == 0) {
                if (listOfNumberInts.get(0) != 0 || listOfNumberInts.size() <= 1) {
                    if (listOfNumberInts.size() <= 1 || listOfNumberInts.get(1) != 1) {
                        listOfNumberNames.add(parseLevel(listOfNumberInts.get(i), digits, null));
                    }
                }
            } else if (i == 1) {
                if (listOfNumberInts.get(1) == 1) {
                    listOfNumberNames.add(parseLevel(listOfNumberInts.get(0), tens, null));
                } else {
                    listOfNumberNames.add(parseLevel(listOfNumberInts.get(i), divisibleByTen, null));
                }
            } else if (i == 2) {
                listOfNumberNames.add(parseLevel(listOfNumberInts.get(i), digits, HUNDRED));
            }
        }

        Collections.reverse(listOfNumberNames);
        return getNumberAsStringLine(listOfNumberNames);
    }

    private static String getNumberAsStringLine(List<String> listOfNumberNames) {
        StringBuilder res = new StringBuilder();
        for (String listOfNumberName : listOfNumberNames) {
            res.append(listOfNumberName).append(" ");
        }
        return res.toString().trim();
    }

    private static String parseLevel(int element, String[] digits, String addition) {
        String res = "";
        res += digits[element];

        if (addition != null) {
            res += " " + addition;
        }

        return res;
    }
}
