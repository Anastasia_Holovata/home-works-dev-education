package com.github.cycles;

/**
 * Задача №6
 * Вывести число, которое является зеркальным отображением последовательности
 * цифр заданного числа, например, задано число 123, вывести 321
 */
public class Task6 {

    public static void main(String[] args) {
        System.out.println("The source number was " + 123 + "; The reversed one equals " + reverseNumber(123));
        System.out.println("The source number was " + 6789 + "; The reversed one equals " + reverseNumber(6789));
        System.out.println("The source number was " + 2021 + "; The reversed one equals " + reverseNumber(2021));
    }

    public static int reverseNumber(int number) {
        String numberAsString = String.valueOf(number);
        char[] numberAsCharArray = numberAsString.toCharArray();

        StringBuilder resultAsString = new StringBuilder();
        for (int i = numberAsCharArray.length - 1; i >= 0; i--) {
            resultAsString.append(numberAsCharArray[i]);
        }

        return Integer.parseInt(resultAsString.toString());
    }
}
