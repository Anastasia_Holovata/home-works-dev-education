package com.github.cycles;

/**
 * Задача №1
 * Найти сумму четных чисел и их количество в диапазоне от 1 до 99
 */
public class Task1 {

    public static void main(String[] args) {
        System.out.println("The sum of array [1..99] equals " + getSumEvenNumbers(1, 99));
    }

    public static int getSumEvenNumbers(int startNumber, int endNumber) {
        int sum = 0;
        for (int i = startNumber; i < endNumber; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }
}
