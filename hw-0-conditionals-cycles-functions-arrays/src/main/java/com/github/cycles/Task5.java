package com.github.cycles;

/**
 * Задача №5
 * Посчитать сумму цифр заданного числа
 */
public class Task5 {

    public static void main(String[] args) {
        System.out.println("The sum of " + 467 + " = " + findSumOfNumbers(467));
        System.out.println("The sum of " + 15555 + " = " + findSumOfNumbers(15555));
    }

    public static int findSumOfNumbers(int number) {
        String stringNumber = String.valueOf(number);
        char[] charArray = stringNumber.toCharArray();
        int sumOfNumbers = 0;

        for (int i = 0; i < charArray.length; i++) {
            sumOfNumbers += Character.getNumericValue(charArray[i]);
        }
        return sumOfNumbers;
    }
}
