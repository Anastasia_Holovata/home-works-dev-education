package com.github.cycles;

/**
 * Задача №3
 * Найти корень натурального числа с точностью до целого (рассмотреть вариант
 * последовательного подбора и метод бинарного поиска)
 */
public class Task3 {

    public static void main(String[] args) {
        System.out.println("Sqrt (by sequence method) for number " + 1 + " = " + getRootBySeq(1));
        System.out.println("Sqrt (by binary search method) for number " + 1 + " = " + getRootByBinarySearch(1));

        System.out.println("Sqrt (by sequence method) for number " + 14 + " = " + getRootBySeq(14));
        System.out.println("Sqrt (by binary search method) for number " + 14 + " = " + getRootByBinarySearch(14));
    }

    public static int getRootBySeq(int number) {
        for (int i = 1; ; i++) {
            int q = i * i;
            if (number == q) {
                return i;
            }
            if (number < q) {
                return i - 1;
            }
        }
    }

    public static int getRootByBinarySearch(int number) {
        int min = 1;
        int max = number;
        int prev = 0;

        while (true) {
            int mid = (min + max) / 2;
            if (prev == mid) {
                return mid;
            }

            int q = mid * mid;

            if (number == q) {
                return mid;
            }
            if (number < q) {
                max = mid;
            } else {
                min = mid;
            }

            prev = mid;
        }
    }
}