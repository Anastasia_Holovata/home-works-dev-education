package com.github.cycles;

/**
 * Задача №2
 * Проверить простое ли число?
 */
public class Task2 {

    public static void main(String[] args) {
        int primeNumber1 = 2;
        int primeNumber2 = 3;
        int primeNumber3 = 5;
        int primeNumber4 = 137;

        int notPrimeNumber1 = 0;
        int notPrimeNumber2 = 1;
        int notPrimeNumber3 = 4;
        int notPrimeNumber4 = 96;

        System.out.println("Is " + primeNumber1 + " prime number = " + isSimpleNumber(primeNumber1));
        System.out.println("Is " + primeNumber2 + " prime number = " + isSimpleNumber(primeNumber2));
        System.out.println("Is " + primeNumber3 + " prime number = " + isSimpleNumber(primeNumber3));
        System.out.println("Is " + primeNumber4 + " prime number = " + isSimpleNumber(primeNumber4));

        System.out.println("Is " + notPrimeNumber1 + " prime number = " + isSimpleNumber(notPrimeNumber1));
        System.out.println("Is " + notPrimeNumber2 + " prime number = " + isSimpleNumber(notPrimeNumber2));
        System.out.println("Is " + notPrimeNumber3 + " prime number = " + isSimpleNumber(notPrimeNumber3));
        System.out.println("Is " + notPrimeNumber4 + " prime number = " + isSimpleNumber(notPrimeNumber4));
    }

    public static boolean isSimpleNumber(int number) {
        if (number == 0 || number == 1) {
            return false;
        } else {
            boolean result = true;
            for (int i = 2; i < number; i++) {
                if (number % i == 0) {
                    result = false;
                    break;
                }
                result = true;
            }
            return result;
        }
    }
}
