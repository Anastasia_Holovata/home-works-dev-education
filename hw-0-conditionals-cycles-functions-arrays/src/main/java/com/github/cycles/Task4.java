package com.github.cycles;

import java.util.ArrayList;
import java.util.List;

/**
 * Вычислить факториал числа n. n! = 1*2*…*n-1*n;!
 */
public class Task4 {

    public static void main(String[] args) {
        System.out.println("Factorial of number " + 1 + " = " + calculateNFactorial(1));
        System.out.println("Factorial of number " + 3 + " = " + calculateNFactorial(3));
        System.out.println("Factorial of number " + 5 + " = " + calculateNFactorial(5));
        System.out.println("Factorial of number " + 7 + " = " + calculateNFactorial(7));
    }

    public static int calculateNFactorial(int number) {
        List<Integer> factorialNumbers = new ArrayList<>();
        for (int i = 1; i < number + 1; i++) {
            factorialNumbers.add(i);
        }
        int multiply = 1;
        for (int i = 1; i < factorialNumbers.size(); i++) {
            multiply *= factorialNumbers.get(i);
        }
        return multiply;
    }
}
