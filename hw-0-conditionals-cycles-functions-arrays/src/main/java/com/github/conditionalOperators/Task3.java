package com.github.conditionalOperators;

/**
 * Задача №3
 * Найти суммы только положительных из трех чисел
 */
public class Task3 {

    public static void main(String[] args) {
        System.out.println(calculate(2, 3, 5));
        System.out.println(calculate(2, 3, -5));
        System.out.println(calculate(2, -3, 5));
        System.out.println(calculate(-2, 3, 5));
        System.out.println(calculate(2, -3, -5));
        System.out.println(calculate(-2, 3, -5));
        System.out.println(calculate(-2, -3, 5));
        System.out.println(calculate(-2, -3, -5));
    }

    public static int calculate(int a, int b, int c) {
        if (a >= 0 && b >= 0 && c >= 0) {
            return a + b + c;
        } else if (a >= 0 && b >= 0) {
            return a + b;
        } else if (a >= 0 && c >= 0) {
            return a + c;
        } else if (b >= 0 && c >= 0) {
            return b + c;
        } else if (a >= 0) {
            return a;
        } else if (b >= 0) {
            return b;
        } else if (c >= 0) {
            return c;
        } else {
            return 0;
        }
    }
}
