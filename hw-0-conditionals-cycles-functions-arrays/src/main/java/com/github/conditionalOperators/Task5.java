package com.github.conditionalOperators;

/**
 * Задача №5
 * Написать программу определения оценки студента по его рейтингу
 */
public class Task5 {

    public static void main(String[] args) {
        System.out.println(studentMark(152));
        System.out.println(studentMark(99));
        System.out.println(studentMark(75));
        System.out.println(studentMark(63));
        System.out.println(studentMark(47));
        System.out.println(studentMark(39));
        System.out.println(studentMark(0));
    }

    public static String studentMark(int mark) {
        if (mark >= 90 && mark <= 100) {
            return "A";
        } else if (mark >= 75 && mark <= 89) {
            return "B";
        } else if (mark >= 60 && mark <= 74) {
            return "C";
        } else if (mark >= 40 && mark <= 59) {
            return "D";
        } else if (mark >= 20 && mark <= 39) {
            return "E";
        } else if (mark >= 0 && mark <= 19) {
            return "F";
        } else {
            return "You entered the mark wrong!";
        }
    }
}

