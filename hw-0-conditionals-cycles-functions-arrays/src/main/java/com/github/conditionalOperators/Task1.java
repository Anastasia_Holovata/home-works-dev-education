package com.github.conditionalOperators;

/**
 * Задача №1
 * Если а – четное посчитать а*б, иначе а+б
 */
public class Task1 {

    public static void main(String[] args) {
        int aEven = 10;
        int aOdd = 9;
        int b = 5;

        System.out.println(function(aEven, b));
        System.out.println(function(aOdd, b));
    }

    public static int function(int a, int b) {
        if (a % 2 == 0) {
            return a * b;
        } else {
            return a + b;
        }
    }
}
