package com.github.conditionalOperators;

/**
 * Задача №2
 * Определить какой четверти принадлежит точка с координатами (х,у)
 */
public class Task2 {

    public static void main(String[] args) {
        System.out.println(findQuarter(4, 2));
        System.out.println(findQuarter(-5, 7));
        System.out.println(findQuarter(-12, -4));
        System.out.println(findQuarter(10, -5));
        System.out.println(findQuarter(0, 0));
    }

    public static String findQuarter(int x, int y) {
        if (x > 0 && y > 0) {
            return "First quarter";
        } else if (x < 0 && y > 0) {
            return "Second quarter";
        } else if (x < 0 && y < 0) {
            return "Third quarter";
        } else if (x > 0 && y < 0) {
            return "Fourth quarter";
        } else {
            return "The coordinates don't belong to a quarter";
        }
    }
}
