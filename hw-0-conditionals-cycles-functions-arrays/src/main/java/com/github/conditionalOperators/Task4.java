package com.github.conditionalOperators;

/**
 * Задача №4
 * Посчитать выражение макс(а*б*с, а+б+с)+3
 */
public class Task4 {

    public static void main(String[] args) {
        System.out.println(calculate(1, 1, 1));
        System.out.println(calculate(3, 3, 3));
        System.out.println(calculate(-4, -2, 1));
        System.out.println(calculate(-5, 3, 1));
    }

    public static int calculate(int a, int b, int c) {
        return getMax(a * b * c, a + b + c) + 3;
    }

    public static int getMax(int a, int b) {
        if (a >= b) {
            return a;
        } else {
            return b;
        }
    }
}
