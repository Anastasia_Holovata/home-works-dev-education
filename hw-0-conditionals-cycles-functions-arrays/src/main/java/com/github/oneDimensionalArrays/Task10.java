package com.github.oneDimensionalArrays;

import java.util.Arrays;

/**
 * Задача №10
 * *Отсортировать массив (Quick, Merge, Shell, Heap)
 */
public class Task10 {

    private static int[] array = {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89};

    public static void main(String[] args) {
        System.out.println("Array before sort: " + Arrays.toString(array));

        int[] copyOfArrayForQuickSort = Arrays.copyOf(array, array.length);
        sortingByQuickSort(copyOfArrayForQuickSort);
        System.out.println("Array after Quick sort: " + Arrays.toString(copyOfArrayForQuickSort));

        int[] copyOfArrayForMergeSort = Arrays.copyOf(array, array.length);
        sortingByMergeSort(copyOfArrayForMergeSort);
        System.out.println("Array after Merge sort: " + Arrays.toString(copyOfArrayForMergeSort));

        int[] copyOfArrayForShellSort = Arrays.copyOf(array, array.length);
        sortingByShellSort(copyOfArrayForShellSort);
        System.out.println("Array after Shell sort: " + Arrays.toString(copyOfArrayForShellSort));

        int[] copyOfArrayForHeapSort = Arrays.copyOf(array, array.length);
        sortingByHeapSort(copyOfArrayForHeapSort);
        System.out.println("Array after Heap sort:  " + Arrays.toString(copyOfArrayForHeapSort));
    }

    public static void sortingByQuickSort(int[] array) {
        sortingByQuickSort(array, 0, array.length - 1);
    }

    private static void sortingByQuickSort(int[] array, int low, int high) {
        int middle = low + (high - low) / 2;
        int basisElement = array[middle];

        int i = low, j = high;
        while (i <= j) {
            while (array[i] < basisElement) {
                i++;
            }

            while (array[j] > basisElement) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (low < j) {
            sortingByQuickSort(array, low, j);
        }

        if (high > i) {
            sortingByQuickSort(array, i, high);
        }
    }

    public static void sortingByMergeSort(int[] array) {
        sortingByMergeSort(array, array.length);
    }

    private static void sortingByMergeSort(int[] array, int number) {
        if (number < 2) {
            return;
        }
        int mid = number / 2;
        int[] l = new int[mid];
        int[] r = new int[number - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = array[i];
        }
        for (int i = mid; i < number; i++) {
            r[i - mid] = array[i];
        }
        sortingByMergeSort(l, mid);
        sortingByMergeSort(r, number - mid);

        merge(array, l, r, mid, number - mid);
    }

    private static void merge(int[] array, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                array[k++] = l[i++];
            } else {
                array[k++] = r[j++];
            }
        }
        while (i < left) {
            array[k++] = l[i++];
        }
        while (j < right) {
            array[k++] = r[j++];
        }
    }

    public static void sortingByShellSort(int[] array) {
        sortingByShellSort(array, array.length);
    }

    private static void sortingByShellSort(int[] array, int num) {
        int i, j, k, tmp;
        for (i = num / 2; i > 0; i = i / 2) {
            for (j = i; j < num; j++) {
                for (k = j - i; k >= 0; k = k - i) {
                    if (array[k + i] >= array[k])
                        break;
                    else {
                        tmp = array[k];
                        array[k] = array[k + i];
                        array[k + i] = tmp;
                    }
                }
            }
        }
    }

    public static void sortingByHeapSort(int[] array) {
        int n = array.length;

        for (int i = n / 2 - 1; i >= 0; i--) {
            heap(array, n, i);
        }

        for (int i = n - 1; i >= 0; i--) {
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            heap(array, i, 0);
        }
    }

    private static void heap(int[] arr, int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < n && arr[l] > arr[largest]) {
            largest = l;
        }

        if (r < n && arr[r] > arr[largest]) {
            largest = r;
        }

        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            heap(arr, n, largest);
        }
    }
}