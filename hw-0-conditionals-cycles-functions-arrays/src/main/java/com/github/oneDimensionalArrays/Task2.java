package com.github.oneDimensionalArrays;

/**
 * Задача №2
 * Найти максимальный элемент массива
 */
public class Task2 {

    private static double[] array = {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11};

    public static void main(String[] args) {
        System.out.println("The maximum element of array is " + findMax(array));
    }

    public static double findMax(double[] array) {
        double max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        return max;
    }
}