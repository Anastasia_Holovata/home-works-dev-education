package com.github.oneDimensionalArrays;

import java.util.Arrays;

/**
 * Задача №8
 * Поменять местами первую и вторую половину массива,
 * например, для массива 1 2 3 4, результат 3 4 1 2
 */
public class Task8 {

    private static int[] array = {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89, -112};

    public static void main(String[] args) {
        System.out.println("The swapped array of given array is " + Arrays.toString(swapElementsOfArray(array)));
    }

    public static int[] swapElementsOfArray(int[] array) {
        int[] swappedArray = new int[array.length];
        int index = 0;
        for (int i = (array.length) / 2; i < array.length; i++) {
            swappedArray[index] = array[i];
            index++;
        }
        for (int i = 0; i < (array.length) / 2; i++) {
            swappedArray[index] = array[i];
            index++;
        }
        return swappedArray;
    }
}
