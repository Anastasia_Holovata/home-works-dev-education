package com.github.oneDimensionalArrays;

/**
 * Задача №7
 * Посчитать количество нечетных элементов массива
 */
public class Task7 {

    private static int[] array = {11, 123, 0, 56, 7, 45, 8, 23, 35, 4, 89};

    public static void main(String[] args) {
        System.out.println("The amount of odd elements in given array equals " + amountOfOddElementsInArray(array));
    }

    public static int amountOfOddElementsInArray(int[] array) {
        int counterOddElements = 0;
        for (int value : array) {
            if (value % 2 == 1) {
                counterOddElements++;
            }
        }
        return counterOddElements;
    }
}