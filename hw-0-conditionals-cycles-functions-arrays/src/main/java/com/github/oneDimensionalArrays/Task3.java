package com.github.oneDimensionalArrays;

/**
 * Задача №3
 * Найти индекс минимального элемента массива
 */
public class Task3 {

    private static double[] array = {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11};

    public static void main(String[] args) {
        System.out.println("The minimum index of element in given array equals " + minIndexOfElementInArray(array));
    }

    public static int minIndexOfElementInArray(double[] array) {
        int minIndex = 0;
        double minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (minValue > array[i]) {
                minIndex = i;
                minValue = array[i];
            }
        }
        return minIndex;
    }
}