package com.github.oneDimensionalArrays;

/**
 * Задача №5
 * Посчитать сумму элементов массива с нечетными индексами
 */
public class Task5 {

    private static int[] array = {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89};

    public static void main(String[] args) {
        System.out.println("The sum of elements with odd indexes in given array equals " + findSumElementsWithOddIndexes(array));
    }

    public static int findSumElementsWithOddIndexes(int[] array) {
        int sumElementsWithOddIndexes = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 1) {
                sumElementsWithOddIndexes += array[i];
            }
        }
        return sumElementsWithOddIndexes;
    }
}