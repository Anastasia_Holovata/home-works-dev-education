package com.github.oneDimensionalArrays;

/**
 * Задача №1
 * Найти минимальный элемент массива
 */
public class Task1 {

    private static double[] array = {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11};

    public static void main(String[] args) {
        System.out.println("The minimum element of array is " + findMin(array));
    }

    public static double findMin(double[] array) {
        double min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        return min;
    }
}