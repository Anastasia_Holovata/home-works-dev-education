package com.github.oneDimensionalArrays;

import java.util.Arrays;

/**
 * Задача №6
 * Сделать реверс массива (массив в обратном направлении)
 */
public class Task6 {

    private static int[] array = {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89};

    public static void main(String[] args) {
        System.out.println(
                "The array before reverse " + Arrays.toString(array) +
                        "\n The array after reverse " + Arrays.toString(reverseArray(array)));
    }

    public static int[] reverseArray(int[] array) {
        int[] reversedArray = new int[array.length];
        for (int i = 0, j = array.length - 1; j >= 0; j--, i++) {
            reversedArray[i] = array[j];
        }
        return reversedArray;
    }
}
