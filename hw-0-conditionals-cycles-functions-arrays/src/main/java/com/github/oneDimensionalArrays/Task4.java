package com.github.oneDimensionalArrays;

/**
 * Задача №4
 * Найти индекс максимального элемента массива
 */
public class Task4 {

    private static double[] array = {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11};

    public static void main(String[] args) {
        System.out.println("The maximum index of element in given array equals " + maxIndexOfElementInArray(array));
    }

    public static int maxIndexOfElementInArray(double[] array) {
        int maxIndex = 0;
        double maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (maxValue < array[i]) {
                maxIndex = i;
                maxValue = array[i];
            }
        }
        return maxIndex;
    }
}