package com.github.oneDimensionalArrays;

import java.util.Arrays;

/**
 * Задача №9
 * Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert))
 */
public class Task9 {

    private static int[] array = {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89};

    public static void main(String[] args) {
        System.out.println("Array before sort: " + Arrays.toString(array));

        System.out.println("Array after Bubble sort: " + Arrays.toString(sortingByBubbleSort(array)));
        System.out.println("Array after Insert sort: " + Arrays.toString(sortingByInsertSort(array)));
        System.out.println("Array after Select sort: " + Arrays.toString(sortingBySelectSort(array)));
    }

    public static int[] sortingByBubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

    public static int[] sortingBySelectSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[index]) {
                    index = j;
                }
            }
            int smallerNumber = array[index];
            array[index] = array[i];
            array[i] = smallerNumber;
        }
        return array;
    }

    public static int[] sortingByInsertSort(int[] array) {
        for (int k = 1; k < array.length; k++) {
            int temp = array[k];
            int j = k - 1;
            while (j >= 0 && temp <= array[j]) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = temp;
        }
        return array;
    }
}
