package com.github.collections.lists;

import com.github.collections.lists.exceptions.ListEmptyException;

public class LinkedListNextAndPrev implements IList {

    private Node start;
    private Node end;

    @Override
    public void init(int[] init) {
        if (init != null) {
            for (int i = 0; i < init.length; i++) {
                addEnd(init[i]);
            }
        }
    }

    @Override
    public void clear() {
        this.start = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = this.start;

        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return count;
    }

    @Override
    public int[] toArray() {
        int size = size();
        if (size == 0) {
            return new int[0];
        }
        int count = 0;
        int[] array = new int[size];
        Node tmp = this.start;
        while (tmp != null) {
            array[count++] = tmp.value;
            tmp = tmp.next;

        }
        return array;
    }

    @Override
    public void addStart(int val) {
        Node tmp = new Node(val);
        tmp.next = this.start;
        tmp.prev = null;
        this.start = tmp;
    }

    @Override
    public void addEnd(int val) {
        if (this.start == null) {
            this.start = new Node(val);
        } else {
            Node tmp = this.start;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            Node newNode = new Node(val);
            newNode.prev = tmp;

            tmp.next = newNode;
            end = newNode;
        }
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (pos < 0 || pos > size) {
            throw new IllegalArgumentException();
        }
        if (pos == 0) {
            addStart(val);
        } else if (pos == size) {
            addEnd(val);
        } else {
            Node tmp = this.start;
            Node newNode = new Node(val);
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            newNode.next = tmp.next;
            newNode.prev = tmp;
            tmp.next = newNode;
        }
    }

    @Override
    public int removeStart() {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        } else {
            Node newRoot = tmp.next;
            int oldRootValue = tmp.value;
            start = newRoot;
            return oldRootValue;
        }
    }

    @Override
    public int removeEnd() {
        Node currentNode = start;
        if (currentNode == null) {
            throw new ListEmptyException();
        }
        Node prev = null;
        while (currentNode.next != null) {
            prev = currentNode;
            currentNode = currentNode.next;
        }
        int oldEndNodeValue = currentNode.value;

        if (prev == null) {
            start = null;
        } else {
            prev.next = null;
            end = prev;
        }

        return oldEndNodeValue;
    }

    @Override
    public int removeByPos(int pos) {
        Node currentNode = start;
        if (currentNode == null) {
            throw new ListEmptyException();
        }

        if (pos == 0) {
            return removeStart();
        }

        int currentPosition = 0;
        Node prev;

        while (currentNode.next != null) {
            prev = currentNode;
            currentNode = currentNode.next;
            currentPosition++;
            if (currentPosition == pos) {
                int oldNodeValue = currentNode.value;
                Node next = currentNode.next;
                next.prev = prev;
                prev.next = next;
                return oldNodeValue;
            }
        }

        throw new ListEmptyException();
    }

    @Override
    public int max() {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        int max = Integer.MIN_VALUE;

        while (currentNode != null) {
            if (max < currentNode.value) {
                max = currentNode.value;
            }
            currentNode = currentNode.next;
        }

        return max;
    }

    @Override
    public int min() {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        int min = Integer.MAX_VALUE;

        while (currentNode != null) {
            if (min > currentNode.value) {
                min = currentNode.value;
            }
            currentNode = currentNode.next;
        }

        return min;
    }

    @Override
    public int maxPos() {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        int max = Integer.MIN_VALUE;
        int positionOfMaxElement = 0;
        int currentPosition = 0;

        while (currentNode != null) {
            if (max < currentNode.value) {
                max = currentNode.value;
                positionOfMaxElement = currentPosition;
            }
            currentPosition++;
            currentNode = currentNode.next;
        }

        return positionOfMaxElement;
    }

    @Override
    public int minPos() {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        int min = Integer.MAX_VALUE;
        int positionOfMinElement = 0;
        int currentPosition = 0;

        while (currentNode != null) {
            if (min > currentNode.value) {
                min = currentNode.value;
                positionOfMinElement = currentPosition;
            }
            currentPosition++;
            currentNode = currentNode.next;
        }

        return positionOfMinElement;
    }

    @Override
    public int[] sort() {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        Node current = start;
        while (current != null) {

            Node second = current.next;
            while (second != null) {

                if (current.value > second.value) {
                    int tmp = current.value;
                    current.value = second.value;
                    second.value = tmp;
                }
                second = second.next;
            }
            current = current.next;
        }

        return toArray();
    }

    @Override
    public int get(int pos) {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        if (pos == 0) {
            return start.value;
        }

        int currentPosition = 0;

        while (currentNode.next != null) {
            currentPosition++;
            currentNode = currentNode.next;

            if (currentPosition == pos) {
                return currentNode.value;
            }
        }

        throw new ListEmptyException();
    }

    @Override
    public int[] halfReverse() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size];

        double doubleHalfPartIndex = ((double) size) / 2;
        int halfPartIndex = (int) doubleHalfPartIndex;

        boolean isNotEqualsPart = doubleHalfPartIndex % 2 != 0;

        for (int i = 0, y = isNotEqualsPart ? halfPartIndex + 1 : halfPartIndex; i < halfPartIndex; i++, y++) {
            newArray[i] = get(y);
        }

        for (int i = 0, y = halfPartIndex; y < size; i++, y++) {
            newArray[y] = get(i);
        }

        clear();
        init(newArray);

        return toArray();
    }

    @Override
    public int[] reverse() {
        Node current = start;

        if (current == null) {
            throw new ListEmptyException();
        }

        Node prev = null;
        Node next;

        int[] values = new int[size()];
        int currentPosition = 0;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        start = prev;

        return toArray();
    }

    @Override
    public void set(int pos, int val) {
        Node currentNode = start;

        if (currentNode == null) {
            throw new ListEmptyException();
        }

        if (pos == 0) {
            start.value = val;
            return;
        }

        int currentPosition = 0;

        while (currentNode.next != null) {
            currentPosition++;
            currentNode = currentNode.next;

            if (currentPosition == pos) {
                currentNode.value = val;
                return;
            }
        }

        throw new ListEmptyException();
    }

    private static class Node {
        int value;
        Node next;
        Node prev;

        public Node(int value) {
            this.value = value;
        }
    }
}
