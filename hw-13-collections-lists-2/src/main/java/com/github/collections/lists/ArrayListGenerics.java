package com.github.collections.lists;

import com.github.collections.lists.exceptions.ListEmptyException;

import java.lang.reflect.Array;

public class ArrayListGenerics<T extends Comparable<T>> implements IListGen<T> {

    private T[] array;
    private Class<T> clazz;

    @Override
    public void init(T[] init, Class<T> clazz) {
        this.clazz = clazz;
        if (init == null) {
            this.array = createNewArray(0);
        } else {
            this.array = createNewArray(init.length);
            for (int i = 0; i < init.length; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.array = createNewArray(0);
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public T[] toArray() {
        int size = size();
        T[] result = createNewArray(size);
        for (int i = 0; i < size; i++) {
            result[i] = array[i];
        }
        return result;
    }

    @Override
    public void addStart(T val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        T[] newArray = createNewArray(size + 1);
        for (int i = 0; i < size; i++) {
            newArray[i + 1] = this.array[i];
        }
        newArray[0] = val;
        this.array = newArray;
    }

    @Override
    public void addStart(T val, Comparable<?> com) {
        // TODO: NEED MORE INFORMATION FOR THIS FUNCTIONALITY
    }

    @Override
    public void addEnd(T val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] tmp = createNewArray(size + 1);
        for (int i = 0; i < size; i++) {
            tmp[i] = this.array[i];
        }
        tmp[size] = val;
        this.array = tmp;
    }

    @Override
    public void addByPos(int pos, T val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        if (pos < 0 || pos > size) {
            throw new ListEmptyException();
        }

        T[] tmp = createNewArray(size + 1);

        if (pos == 0) {
            addStart(val);
        } else if (pos == size) {
            addEnd(val);
        } else {
            for (int i = 0; i < size + 1; i++) {
                if (i < pos)
                    tmp[i] = this.array[i];
                else if (i == pos)
                    tmp[i] = val;
                else
                    tmp[i] = this.array[i - 1];
            }
        }
        this.array = tmp;
    }

    @Override
    public T removeStart() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] newArray = createNewArray(size - 1);

        for (int i = 0; i < size - 1; i++) {
            newArray[i] = this.array[i + 1];
        }
        T tmp = this.array[0];
        this.array = newArray;

        return tmp;
    }

    @Override
    public T removeEnd() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] newArray = createNewArray(size - 1);

        for (int i = 0; i < size - 1; i++) {
            newArray[i] = this.array[i];
        }
        T tmp = this.array[size - 1];
        this.array = newArray;

        return (T) tmp;
    }

    @Override
    public T removeByPos(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] newArray = createNewArray(size - 1);
        T tmp = this.array[pos];


        if (pos == 0) {
            removeStart();
        } else if (pos == size - 1) {
            removeEnd();
        } else {
            for (int i = 0; i < size - 1; i++) {
                if (i < pos) {
                    newArray[i] = this.array[i];
                } else if (i >= pos) {
                    newArray[i] = this.array[i + 1];
                }
            }
            this.array = newArray;
        }
        return tmp;
    }

    @Override
    public T max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T max = this.array[0];

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(max) > 0) {
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public T min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T min = (T) this.array[0];

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(min) < 0) {
                min = (T) this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int maxPosition = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(this.array[maxPosition]) > 0)
                maxPosition = i;
        }
        return maxPosition;
    }

    @Override
    public int minPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int minPosition = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(this.array[minPosition]) < 0)
                minPosition = i;
        }
        return minPosition;
    }

    @Override
    public T[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }

        T temp;
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (this.array[j].compareTo(this.array[j - 1]) < 0) {
                    temp = this.array[j];
                    this.array[j] = this.array[j - 1];
                    this.array[j - 1] = temp;
                }
            }
        }
        return this.array;
    }

    @Override
    public T get(int pos) {
        if (pos < 0 || pos >= size()) {
            throw new ListEmptyException();
        }

        return (T) this.array[pos];
    }

    @Override
    public T[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] newArray = createNewArray(size);

        double doubleHalfPartIndex = ((double) size) / 2;
        int halfPartIndex = (int) doubleHalfPartIndex;

        boolean isNotEqualsPart = doubleHalfPartIndex % 2 != 0;

        for (int i = 0, y = isNotEqualsPart ? halfPartIndex + 1 : halfPartIndex; i < halfPartIndex; i++, y++) {
            newArray[i] = array[y];
        }

        for (int i = 0, y = halfPartIndex; y < size; i++, y++) {
            newArray[y] = array[i];
        }

        this.array = newArray;

        return this.array;
    }

    @Override
    public T[] revers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        for (int i = 0; i < size / 2; i++) {
            T temp = this.array[i];
            this.array[i] = this.array[size - 1 - i];
            this.array[size - 1 - i] = temp;
        }
        return this.array;
    }

    @Override
    public void set(int pos, T val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos >= 0 || pos < size) {
            this.array[pos] = val;
        } else {
            throw new ListEmptyException();
        }
    }

    private <T> T[] createNewArray(int size) {
        @SuppressWarnings("unchecked")
        T[] arr = (T[]) Array.newInstance(clazz, size);

        return arr;
    }
}
