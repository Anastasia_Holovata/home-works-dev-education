package com.github.collections.lists;

public interface IListGen<T extends Comparable<T>> {

    void init(T[] init, Class<T> clazz);

    void clear();

    int size();

    T[] toArray();

    void addStart(T val);

    void addStart(T val, Comparable<?> com);

    void addEnd(T val);

    void addByPos(int pos, T val);

    T removeStart();

    T removeEnd();

    T removeByPos(int pos);

    <T extends Comparable<T>> T max();

    T min();

    int maxPos();

    int minPos();

    T[] sort();

    T get(int pos);

    T[] halfRevers();

    T[] revers();

    void set(int pos, T val);


}