package com.github.collections.lists;

import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.Assert.*;

public class ArrayListGenericsTest {

    @Test
    public void clearTen() {
        IListGen<Integer> list = initList(getArrayElements(10));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearFive() {
        IListGen<Integer> list = initList(getArrayElements(5));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearTwo() {
        IListGen<Integer> list = initList(getArrayElements(2));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearOne() {
        IListGen<Integer> list = initList(getArrayElements(1));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearZero() {
        IListGen<Integer> list = initList(new Integer[0]);
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void sizeTen() {
        IListGen<Integer> list = initList(getArrayElements(10));
        assertEquals(10, list.size());
    }

    @Test
    public void sizeFive() {
        IListGen<Integer> list = initList(getArrayElements(5));
        assertEquals(5, list.size());
    }

    @Test
    public void sizeTwo() {
        IListGen<Integer> list = initList(getArrayElements(2));
        assertEquals(2, list.size());
    }

    @Test
    public void sizeOne() {
        IListGen<Integer> list = initList(getArrayElements(1));
        assertEquals(1, list.size());
    }

    @Test
    public void sizeZero() {
        IListGen<Integer> list = initList(new Integer[0]);
        assertEquals(0, list.size());
    }

    @Test
    public void addStartTen() {
        IListGen<Integer> list = initList(getArrayElements(10));
        assertEquals(10, list.size());
        list.addStart(999);
        assertEquals(11, list.size());
        assertEquals(Integer.valueOf(999), list.get(0));
    }

    @Test
    public void addStartFive() {
        IListGen<Integer> list = initList(getArrayElements(5));
        assertEquals(5, list.size());
        list.addStart(444);
        assertEquals(6, list.size());
        assertEquals(Integer.valueOf(444), list.get(0));
    }

    @Test
    public void addStartTwo() {
        IListGen<Integer> list = initList(getArrayElements(2));
        assertEquals(2, list.size());
        list.addStart(111);
        assertEquals(3, list.size());
        assertEquals(Integer.valueOf(111), list.get(0));
    }

    @Test
    public void addStartOne() {
        IListGen<Integer> list = initList(getArrayElements(1));
        assertEquals(1, list.size());
        list.addStart(111);
        assertEquals(2, list.size());
        assertEquals(Integer.valueOf(111), list.get(0));
    }

    @Test
    public void toArrayTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayFive() {
        Integer[] expectedArray = getArrayElements(5);
        IListGen<Integer> list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toEmptyArray() {
        Integer[] expectedArray = new Integer[]{};
        IListGen<Integer> list = initList(expectedArray);
        assertArrayEquals(expectedArray, new Integer[]{});
    }

    @Test
    public void addEndTen() {
        IListGen<Integer> list = initList(getArrayElements(10));
        list.addEnd(999);
        assertEquals(11, list.size());
        assertEquals(Integer.valueOf(999), list.get(10));
    }

    @Test
    public void addEndSix() {
        IListGen<Integer> list = initList(getArrayElements(6));
        list.addEnd(999);
        assertEquals(7, list.size());
        assertEquals(Integer.valueOf(999), list.get(6));
    }

    @Test
    public void addEndThree() {
        IListGen<Integer> list = initList(getArrayElements(3));
        list.addEnd(999);
        assertEquals(4, list.size());
        assertEquals(Integer.valueOf(999), list.get(3));
    }

    @Test
    public void addEndTwo() {
        IListGen<Integer> list = initList(getArrayElements(2));
        list.addEnd(999);
        assertEquals(3, list.size());
        assertEquals(Integer.valueOf(999), list.get(2));
    }

    @Test
    public void addEndOne() {
        IListGen<Integer> list = initList(getArrayElements(1));
        list.addEnd(999);
        assertEquals(2, list.size());
        assertEquals(Integer.valueOf(999), list.get(1));
    }

    @Test
    public void addByPosTen() {
        IListGen<Integer> list = initList(getArrayElements(10));
        list.addByPos(5, 999);
        assertEquals(11, list.size());
        assertEquals(Integer.valueOf(999), list.get(5));
    }

    @Test
    public void addByPosSix() {
        IListGen<Integer> list = initList(getArrayElements(6));
        list.addByPos(2, 999);
        assertEquals(7, list.size());
        assertEquals(Integer.valueOf(999), list.get(2));
    }

    @Test
    public void addByPosThree() {
        IListGen<Integer> list = initList(getArrayElements(3));
        list.addByPos(2, 999);
        assertEquals(4, list.size());
        assertEquals(Integer.valueOf(999), list.get(2));
    }

    @Test
    public void addByPosTwo() {
        IListGen<Integer> list = initList(getArrayElements(2));
        list.addByPos(1, 999);
        assertEquals(3, list.size());
        assertEquals(Integer.valueOf(999), list.get(1));
    }

    @Test
    public void removeStartTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeStart();
        assertEquals(9, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeStart();
        assertEquals(5, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeStart();
        assertEquals(2, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeStart();
        assertEquals(1, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeStart();
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void removeEndTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeEnd();
        assertEquals(9, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeEnd();
        assertEquals(5, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeEnd();
        assertEquals(2, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeEnd();
        assertEquals(1, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeEnd();
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void removeByPosTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeByPos(5);
        assertEquals(9, list.size());
        assertEquals(expectedArray[6], list.get(5));
    }

    @Test
    public void removeByPosSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeByPos(3);
        assertEquals(5, list.size());
        assertEquals(expectedArray[4], list.get(3));
    }

    @Test
    public void removeByPosThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeByPos(1);
        assertEquals(2, list.size());
        assertEquals(expectedArray[2], list.get(1));
    }

    @Test
    public void removeByPosTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeByPos(0);
        assertEquals(1, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeByPosOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeByPos(0);
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void maxTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        Integer max = list.max();
        assertEquals(Arrays.stream(expectedArray).max(Comparator.naturalOrder()).get(), max);
    }

    @Test
    public void maxSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        Integer max = list.max();
        assertEquals(Arrays.stream(expectedArray).max(Comparator.naturalOrder()).get(), max);
    }

    @Test
    public void maxThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        Integer max = list.max();
        assertEquals(Arrays.stream(expectedArray).max(Comparator.naturalOrder()).get(), max);
    }

    @Test
    public void maxTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        Integer max = list.max();
        assertEquals(Arrays.stream(expectedArray).max(Comparator.naturalOrder()).get(), max);
    }

    @Test
    public void maxOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        Integer max = list.max();
        assertEquals(Arrays.stream(expectedArray).max(Comparator.naturalOrder()).get(), max);
    }

    @Test
    public void minTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        Integer min = list.min();
        assertEquals(Arrays.stream(expectedArray).min(Comparator.naturalOrder()).get(), min);
    }

    @Test
    public void minSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        Integer min = list.min();
        assertEquals(Arrays.stream(expectedArray).min(Comparator.naturalOrder()).get(), min);
    }

    @Test
    public void minThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        Integer min = list.min();
        assertEquals(Arrays.stream(expectedArray).min(Comparator.naturalOrder()).get(), min);
    }

    @Test
    public void minTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        Integer min = list.min();
        assertEquals(Arrays.stream(expectedArray).min(Comparator.naturalOrder()).get(), min);
    }

    @Test
    public void minOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        Integer min = list.min();
        assertEquals(Arrays.stream(expectedArray).min(Comparator.naturalOrder()).get(), min);
    }

    @Test
    public void maxPosTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(9, maxPos);
    }

    @Test
    public void maxPosSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(5, maxPos);
    }

    @Test
    public void maxPosThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(2, maxPos);
    }

    @Test
    public void maxPosTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(1, maxPos);
    }

    @Test
    public void maxPosOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(0, maxPos);
    }

    @Test
    public void minPosTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void sortMany() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200, 0, 500, 700, 400, 600, 1000, 800});
        Integer[] expectedArray = {0, 100, 200, 300, 400, 500, 600, 700, 800, 1000};
        Integer[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortArrayWithSameValues() {
        IListGen<Integer> list = initList(new Integer[]{300, 300, 300, 300, 300, 300});
        Integer[] expectedArray = {300, 300, 300, 300, 300, 300};
        Integer[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortArrayWithThreeValues() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200});
        Integer[] expectedArray = {100, 200, 300};
        Integer[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortOneElementArray() {
        IListGen<Integer> list = initList(new Integer[]{300});
        Integer[] expectedArray = {300};
        Integer[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void getElementOfTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        Integer actualValue = list.get(5);
        assertEquals(expectedArray[5], actualValue);
    }

    @Test
    public void getElementOfSix() {
        Integer[] expectedArray = getArrayElements(6);
        IListGen<Integer> list = initList(expectedArray);
        Integer actualValue = list.get(3);
        assertEquals(expectedArray[3], actualValue);
    }

    @Test
    public void getElementOfThree() {
        Integer[] expectedArray = getArrayElements(3);
        IListGen<Integer> list = initList(expectedArray);
        Integer actualValue = list.get(2);
        assertEquals(expectedArray[2], actualValue);
    }

    @Test
    public void getElementOfTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        Integer actualValue = list.get(1);
        assertEquals(expectedArray[1], actualValue);
    }

    @Test
    public void getElementOfOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        Integer actualValue = list.get(0);
        assertEquals(expectedArray[0], actualValue);
    }

    @Test
    public void halfReverseOddCount() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200, 0, 500});
        Integer[] expectedArray = {0, 500, 300, 100, 200};
        Integer[] actualArray = list.halfRevers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseEvenCount() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200, 500});
        Integer[] expectedArray = {200, 500, 300, 100};
        Integer[] actualArray = list.halfRevers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseSameElements() {
        IListGen<Integer> list = initList(new Integer[]{300, 300, 300, 300, 300});
        Integer[] expectedArray = {300, 300, 300, 300, 300};
        Integer[] actualArray = list.halfRevers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseOneElement() {
        IListGen<Integer> list = initList(new Integer[]{500});
        Integer[] expectedArray = {500};
        Integer[] actualArray = list.halfRevers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseEmptyArray() {
        Integer[] expectedArray = {};
        assertArrayEquals(expectedArray, new Integer[]{});
    }

    @Test
    public void reverseMany() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200, 0, 500, 400, 600, 800});
        Integer[] expectedArray = {800, 600, 400, 500, 0, 200, 100, 300};
        Integer[] actualArray = list.revers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseFive() {
        IListGen<Integer> list = initList(new Integer[]{300, 100, 200, 0, 500});
        Integer[] expectedArray = {500, 0, 200, 100, 300};
        Integer[] actualArray = list.revers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseTwo() {
        IListGen<Integer> list = initList(new Integer[]{200, 0});
        Integer[] expectedArray = {0, 200};
        Integer[] actualArray = list.revers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseOne() {
        IListGen<Integer> list = initList(new Integer[]{500});
        Integer[] expectedArray = {500};
        Integer[] actualArray = list.revers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseZero() {
        Integer[] expectedArray = {};
        assertArrayEquals(expectedArray, new Integer[]{});
    }

    @Test
    public void reverseEqualElements() {
        IListGen<Integer> list = initList(new Integer[]{100, 100, 100, 100, 100});
        Integer[] expectedArray = {100, 100, 100, 100, 100};
        Integer[] actualArray = list.revers();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void setTen() {
        Integer[] expectedArray = getArrayElements(10);
        IListGen<Integer> list = initList(expectedArray);
        list.set(5, 999);
        assertEquals(Integer.valueOf(999), list.get(5));
    }

    @Test
    public void setFive() {
        Integer[] expectedArray = getArrayElements(5);
        IListGen<Integer> list = initList(expectedArray);
        list.set(4, 999);
        assertEquals(Integer.valueOf(999), list.get(4));
    }

    @Test
    public void setTwo() {
        Integer[] expectedArray = getArrayElements(2);
        IListGen<Integer> list = initList(expectedArray);
        list.set(1, 999);
        assertEquals(Integer.valueOf(999), list.get(1));
    }

    @Test
    public void setOne() {
        Integer[] expectedArray = getArrayElements(1);
        IListGen<Integer> list = initList(expectedArray);
        list.set(0, 999);
        assertEquals(Integer.valueOf(999), list.get(0));
    }

    @Test
    public void setZero() {
        Integer[] expectedArray = new Integer[]{};
        IListGen<Integer> list = initList(expectedArray);
        assertEquals(0, 0);
    }

    protected Integer[] getArrayElements(int elements) {
        Integer[] array = new Integer[elements];
        for (int i = 0; i < elements; i++) {
            array[i] = 100 * i;
        }
        return array;
    }

    IListGen<Integer> initList(Integer[] array) {
        IListGen<Integer> list = new ArrayListGenerics<>();
        list.init(array, Integer.class);
        return list;
    }
}