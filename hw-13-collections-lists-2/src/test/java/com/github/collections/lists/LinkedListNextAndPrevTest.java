package com.github.collections.lists;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class LinkedListNextAndPrevTest {

    @Test
    public void clearTen() {
        IList list = initList(getArrayElements(10));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearFive() {
        IList list = initList(getArrayElements(5));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearTwo() {
        IList list = initList(getArrayElements(2));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearOne() {
        IList list = initList(getArrayElements(1));
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void clearZero() {
        IList list = initList(new int[0]);
        list.clear();
        assertEquals(0, list.size());
    }

    @Test
    public void sizeTen() {
        IList list = initList(getArrayElements(10));
        assertEquals(10, list.size());
    }

    @Test
    public void sizeFive() {
        IList list = initList(getArrayElements(5));
        assertEquals(5, list.size());
    }

    @Test
    public void sizeTwo() {
        IList list = initList(getArrayElements(2));
        assertEquals(2, list.size());
    }

    @Test
    public void sizeOne() {
        IList list = initList(getArrayElements(1));
        assertEquals(1, list.size());
    }

    @Test
    public void sizeZero() {
        IList list = initList(new int[0]);
        assertEquals(0, list.size());
    }

    @Test
    public void addStartTen() {
        IList list = initList(getArrayElements(10));
        assertEquals(10, list.size());
        list.addStart(999);
        assertEquals(11, list.size());
        assertEquals(999, list.get(0));
    }

    @Test
    public void addStartFive() {
        IList list = initList(getArrayElements(5));
        assertEquals(5, list.size());
        list.addStart(444);
        assertEquals(6, list.size());
        assertEquals(444, list.get(0));
    }

    @Test
    public void addStartTwo() {
        IList list = initList(getArrayElements(2));
        assertEquals(2, list.size());
        list.addStart(111);
        assertEquals(3, list.size());
        assertEquals(111, list.get(0));
    }

    @Test
    public void addStartOne() {
        IList list = initList(getArrayElements(1));
        assertEquals(1, list.size());
        list.addStart(111);
        assertEquals(2, list.size());
        assertEquals(111, list.get(0));
    }

    @Test
    public void toArrayTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayFive() {
        int[] expectedArray = getArrayElements(5);
        IList list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toArrayOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        assertArrayEquals(expectedArray, list.toArray());
    }

    @Test
    public void toEmptyArray() {
        int[] expectedArray = new int[]{};
        IList list = initList(expectedArray);
        assertArrayEquals(expectedArray, new int[]{});
    }

    @Test
    public void addEndTen() {
        IList list = initList(getArrayElements(10));
        list.addEnd(999);
        assertEquals(11, list.size());
        assertEquals(999, list.get(10));
    }

    @Test
    public void addEndSix() {
        IList list = initList(getArrayElements(6));
        list.addEnd(999);
        assertEquals(7, list.size());
        assertEquals(999, list.get(6));
    }

    @Test
    public void addEndThree() {
        IList list = initList(getArrayElements(3));
        list.addEnd(999);
        assertEquals(4, list.size());
        assertEquals(999, list.get(3));
    }

    @Test
    public void addEndTwo() {
        IList list = initList(getArrayElements(2));
        list.addEnd(999);
        assertEquals(3, list.size());
        assertEquals(999, list.get(2));
    }

    @Test
    public void addEndOne() {
        IList list = initList(getArrayElements(1));
        list.addEnd(999);
        assertEquals(2, list.size());
        assertEquals(999, list.get(1));
    }

    @Test
    public void addByPosTen() {
        IList list = initList(getArrayElements(10));
        list.addByPos(5, 999);
        assertEquals(11, list.size());
        assertEquals(999, list.get(5));
    }

    @Test
    public void addByPosSix() {
        IList list = initList(getArrayElements(6));
        list.addByPos(2, 999);
        assertEquals(7, list.size());
        assertEquals(999, list.get(2));
    }

    @Test
    public void addByPosThree() {
        IList list = initList(getArrayElements(3));
        list.addByPos(2, 999);
        assertEquals(4, list.size());
        assertEquals(999, list.get(2));
    }

    @Test
    public void addByPosTwo() {
        IList list = initList(getArrayElements(2));
        list.addByPos(1, 999);
        assertEquals(3, list.size());
        assertEquals(999, list.get(1));
    }

    @Test
    public void addByPosOne() {
        IList list = initList(getArrayElements(1));
        list.addByPos(1, 0);
        assertEquals(2, list.size());
        assertEquals(0, list.get(1));
    }

    @Test
    public void removeStartTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeStart();
        assertEquals(9, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeStart();
        assertEquals(5, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeStart();
        assertEquals(2, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeStart();
        assertEquals(1, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeStartOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeStart();
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void removeEndTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeEnd();
        assertEquals(9, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeEnd();
        assertEquals(5, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeEnd();
        assertEquals(2, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeEnd();
        assertEquals(1, list.size());
        assertEquals(expectedArray[expectedArray.length - 2], list.get(expectedArray.length - 2));
    }

    @Test
    public void removeEndOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeEnd();
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void removeByPosTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        assertEquals(10, list.size());
        list.removeByPos(5);
        assertEquals(9, list.size());
        assertEquals(expectedArray[6], list.get(5));
    }

    @Test
    public void removeByPosSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        assertEquals(6, list.size());
        list.removeByPos(3);
        assertEquals(5, list.size());
        assertEquals(expectedArray[4], list.get(3));
    }

    @Test
    public void removeByPosThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        assertEquals(3, list.size());
        list.removeByPos(1);
        assertEquals(2, list.size());
        assertEquals(expectedArray[2], list.get(1));
    }

    @Test
    public void removeByPosTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        assertEquals(2, list.size());
        list.removeByPos(0);
        assertEquals(1, list.size());
        assertEquals(expectedArray[1], list.get(0));
    }

    @Test
    public void removeByPosOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        assertEquals(1, list.size());
        list.removeByPos(0);
        assertEquals(0, list.size());
        assertEquals(0, 0);
    }

    @Test
    public void maxTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        int max = list.max();
        assertEquals(Arrays.stream(expectedArray).max().getAsInt(), max);
    }

    @Test
    public void maxSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        int max = list.max();
        assertEquals(Arrays.stream(expectedArray).max().getAsInt(), max);
    }

    @Test
    public void maxThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        int max = list.max();
        assertEquals(Arrays.stream(expectedArray).max().getAsInt(), max);
    }

    @Test
    public void maxTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        int max = list.max();
        assertEquals(Arrays.stream(expectedArray).max().getAsInt(), max);
    }

    @Test
    public void maxOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        int max = list.max();
        assertEquals(Arrays.stream(expectedArray).max().getAsInt(), max);
    }

    @Test
    public void minTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        int min = list.min();
        assertEquals(Arrays.stream(expectedArray).min().getAsInt(), min);
    }

    @Test
    public void minSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        int min = list.min();
        assertEquals(Arrays.stream(expectedArray).min().getAsInt(), min);
    }

    @Test
    public void minThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        int min = list.min();
        assertEquals(Arrays.stream(expectedArray).min().getAsInt(), min);
    }

    @Test
    public void minTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        int min = list.min();
        assertEquals(Arrays.stream(expectedArray).min().getAsInt(), min);
    }

    @Test
    public void minOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        int min = list.min();
        assertEquals(Arrays.stream(expectedArray).min().getAsInt(), min);
    }

    @Test
    public void maxPosTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(9, maxPos);
    }

    @Test
    public void maxPosSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(5, maxPos);
    }

    @Test
    public void maxPosThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(2, maxPos);
    }

    @Test
    public void maxPosTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(1, maxPos);
    }

    @Test
    public void maxPosOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        int maxPos = list.maxPos();
        assertEquals(0, maxPos);
    }

    @Test
    public void minPosTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void minPosOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        int minPos = list.minPos();
        assertEquals(0, minPos);
    }

    @Test
    public void sortMany() {
        IList list = initList(new int[]{300, 100, 200, 0, 500, 700, 400, 600, 1000, 800});
        int[] expectedArray = {0, 100, 200, 300, 400, 500, 600, 700, 800, 1000};
        int[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortArrayWithSameValues() {
        IList list = initList(new int[]{300, 300, 300, 300, 300, 300});
        int[] expectedArray = {300, 300, 300, 300, 300, 300};
        int[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortArrayWithThreeValues() {
        IList list = initList(new int[]{300, 100, 200});
        int[] expectedArray = {100, 200, 300};
        int[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortOneElementArray() {
        IList list = initList(new int[]{300});
        int[] expectedArray = {300};
        int[] actualArray = list.sort();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void sortEmptyArray() {
        int[] expectedArray = {};
        assertArrayEquals(expectedArray, new int[]{});
    }

    @Test
    public void getElementOfTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        int actualValue = list.get(5);
        assertEquals(expectedArray[5], actualValue);
    }

    @Test
    public void getElementOfSix() {
        int[] expectedArray = getArrayElements(6);
        IList list = initList(expectedArray);
        int actualValue = list.get(3);
        assertEquals(expectedArray[3], actualValue);
    }

    @Test
    public void getElementOfThree() {
        int[] expectedArray = getArrayElements(3);
        IList list = initList(expectedArray);
        int actualValue = list.get(2);
        assertEquals(expectedArray[2], actualValue);
    }

    @Test
    public void getElementOfTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        int actualValue = list.get(1);
        assertEquals(expectedArray[1], actualValue);
    }

    @Test
    public void getElementOfOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        int actualValue = list.get(0);
        assertEquals(expectedArray[0], actualValue);
    }

    @Test
    public void halfReverseOddCount() {
        IList list = initList(new int[]{300, 100, 200, 0, 500});
        int[] expectedArray = {0, 500, 300, 100, 200};
        int[] actualArray = list.halfReverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseEvenCount() {
        IList list = initList(new int[]{300, 100, 200, 500});
        int[] expectedArray = {200, 500, 300, 100};
        int[] actualArray = list.halfReverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseSameElements() {
        IList list = initList(new int[]{300, 300, 300, 300, 300});
        int[] expectedArray = {300, 300, 300, 300, 300};
        int[] actualArray = list.halfReverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseOneElement() {
        IList list = initList(new int[]{500});
        int[] expectedArray = {500};
        int[] actualArray = list.halfReverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void halfReverseEmptyArray() {
        int[] expectedArray = {};
        assertArrayEquals(expectedArray, new int[]{});
    }

    @Test
    public void reverseMany() {
        IList list = initList(new int[]{300, 100, 200, 0, 500, 400, 600, 800});
        int[] expectedArray = {800, 600, 400, 500, 0, 200, 100, 300};
        int[] actualArray = list.reverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseFive() {
        IList list = initList(new int[]{300, 100, 200, 0, 500});
        int[] expectedArray = {500, 0, 200, 100, 300};
        int[] actualArray = list.reverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseTwo() {
        IList list = initList(new int[]{200, 0});
        int[] expectedArray = {0, 200};
        int[] actualArray = list.reverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseOne() {
        IList list = initList(new int[]{500});
        int[] expectedArray = {500};
        int[] actualArray = list.reverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void reverseZero() {
        int[] expectedArray = {};
        assertArrayEquals(expectedArray, new int[]{});
    }

    @Test
    public void reverseEqualElements() {
        IList list = initList(new int[]{100, 100, 100, 100, 100});
        int[] expectedArray = {100, 100, 100, 100, 100};
        int[] actualArray = list.reverse();
        assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void setTen() {
        int[] expectedArray = getArrayElements(10);
        IList list = initList(expectedArray);
        list.set(5, 999);
        assertEquals(999, list.get(5));
    }

    @Test
    public void setFive() {
        int[] expectedArray = getArrayElements(5);
        IList list = initList(expectedArray);
        list.set(4, 999);
        assertEquals(999, list.get(4));
    }

    @Test
    public void setTwo() {
        int[] expectedArray = getArrayElements(2);
        IList list = initList(expectedArray);
        list.set(1, 999);
        assertEquals(999, list.get(1));
    }

    @Test
    public void setOne() {
        int[] expectedArray = getArrayElements(1);
        IList list = initList(expectedArray);
        list.set(0, 999);
        assertEquals(999, list.get(0));
    }

    @Test
    public void setZero() {
        int[] expectedArray = new int[]{};
        IList list = initList(expectedArray);
        assertEquals(0, 0);
    }

    protected int[] getArrayElements(int elements) {
        int[] array = new int[elements];
        for (int i = 0; i < elements; i++) {
            array[i] = 100 * i;
        }
        return array;
    }


    IList initList(int[] array) {
        IList list = new LinkedListNextAndPrev();
        list.init(array);
        return list;
    }
}