package com.github.log4j;

import org.apache.log4j.Logger;

import java.util.Random;

public class Application {

    private final static Logger log = Logger.getLogger(Application.class);
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        try {
            generate();
            log.info("Приложение успешно запущено");
        } catch (DevEducationException exception) {
            log.error(exception.getMessage());
            throw exception;
        }
    }

    private static void generate() throws DevEducationException {
        int generatedValue = generateRandomIntValue(0, 10);

        if (generatedValue <= 5) {
            throw new DevEducationException("Сгенерированное число – " + generatedValue);
        }
    }

    private static int generateRandomIntValue(int min, int max) {
        return RANDOM.nextInt(max - min + 1) + min;
    }
}
