package com.github.log4j;

public class DevEducationException extends RuntimeException {

    DevEducationException(String message) {
        super(message);
    }
}
