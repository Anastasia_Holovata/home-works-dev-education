package com.github.desktopApplication;

import com.github.desktopApplication.converterTypes.*;

import static org.junit.Assert.assertEquals;

public class ConvertersTest {

    private static final double DELTA = 1e-15;

    @org.junit.Test
    public void convertTime() {
        assertEquals(3600.0, Converters.convertTime(1, TimeType.HOUR, TimeType.SECOND), DELTA);
        assertEquals(0.5, Converters.convertTime(30, TimeType.SECOND, TimeType.MINUTE), DELTA);
    }

    @org.junit.Test
    public void convertWeight() {
        assertEquals(25000, Converters.convertWeight(5, WeightType.KILOGRAM, WeightType.CARAT),
                DELTA);
        assertEquals(0.001, Converters.convertWeight(5, WeightType.CARAT, WeightType.KILOGRAM),
                DELTA);
    }

    @org.junit.Test
    public void convertVolume() {
        assertEquals(7000, Converters.convertVolume(7, VolumeType.LITRE, VolumeType.CUBIC_METRE),
                DELTA);
        assertEquals(2.6417200000000003,
                Converters.convertVolume(10, VolumeType.GALLON, VolumeType.LITRE), DELTA);
    }

    @org.junit.Test
    public void convertLength() {
        assertEquals(4000, Converters.convertLength(4, LengthType.KILOMETER, LengthType.METER),
                DELTA);
        assertEquals(6.43736, Converters.convertLength(4, LengthType.MILE, LengthType.KILOMETER),
                DELTA);
    }

    @org.junit.Test
    public void convertTemperature() {
        assertEquals(33.8,
                Converters.convertTemperature(1, TemperatureType.CELSIUS, TemperatureType.FAHRENHEIT),
                DELTA);
        assertEquals(-272.15,
                Converters.convertTemperature(1, TemperatureType.KELVIN, TemperatureType.CELSIUS),
                DELTA);
    }
}