package com.github.desktopApplication.windows;

import com.github.desktopApplication.converterTypes.ConverterType;
import com.github.desktopApplication.panels.ConverterPanel;

import javax.swing.*;
import java.awt.*;

public class ConverterWindow extends JFrame {

    private ConverterType converterType;

    public ConverterWindow(ConverterType converterType) throws HeadlessException {
        this.converterType = converterType;

        setTitle("Converter by type: " + converterType.name());
        setVisible(true);
        setBounds(200, 200, 400, 450);
        add(new ConverterPanel(converterType));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
