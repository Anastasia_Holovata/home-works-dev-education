package com.github.desktopApplication.windows;

import com.github.desktopApplication.panels.HomePanel;

import javax.swing.*;

public class HomeConverterWindow extends JFrame {

    public HomeConverterWindow() {
        setTitle("Converter");
        setVisible(true);
        setBounds(150, 150, 300, 450);
        add(new HomePanel());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
