package com.github.desktopApplication.panels;

import com.github.desktopApplication.converterTypes.ConverterType;
import com.github.desktopApplication.windows.ConverterWindow;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePanel extends JPanel implements ActionListener {

    private final static Color FRAME_COLOR = new Color(221, 226, 205);

    public HomePanel() {
        setPanelParams();
        initComponents();
    }

    private void initComponents() {
        createButton("Time converter", ConverterType.TIME);
        createButton("Weight converter", ConverterType.WEIGHT);
        createButton("Volume converter", ConverterType.VOLUME);
        createButton("Length converter", ConverterType.LENGTH);
        createButton("Temperature converter", ConverterType.TEMPERATURE);
    }

    private void createButton(String text, ConverterType converterType) {
        JButton button = new JButton(text);
        button.setBounds(28, 10 + converterType.ordinal() * 65, 230, 60);
        button.setBackground(FRAME_COLOR);
        button.setBorder(new BevelBorder(BevelBorder.RAISED));
        button.addActionListener(this);
        button.setActionCommand(converterType.name());
        add(button);
    }

    private void setPanelParams() {
        setBackground(FRAME_COLOR);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(null);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actionCommand = actionEvent.getActionCommand();

        for (ConverterType converterType : ConverterType.values()) {
            if (converterType.name().equals(actionCommand)) {
                new ConverterWindow(ConverterType.valueOf(actionCommand));
            }
        }
    }
}
