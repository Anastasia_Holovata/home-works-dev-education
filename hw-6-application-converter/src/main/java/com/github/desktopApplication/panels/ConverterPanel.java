package com.github.desktopApplication.panels;

import com.github.desktopApplication.Converters;
import com.github.desktopApplication.converterTypes.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class ConverterPanel extends JPanel {

    private final static Color FRAME_COLOR = new Color(221, 226, 205);

    private ConverterType converterType;

    public ConverterPanel(ConverterType converterType) {
        this.converterType = converterType;
        setPanelParams();
        initComponents();
    }

    private void setPanelParams() {
        setBackground(FRAME_COLOR);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(null);
    }

    private void initComponents() {
        JLabel labelFrom = new JLabel("From type: ");
        labelFrom.setBounds(10, 80, 100, 50);
        add(labelFrom);

        JLabel labelTo = new JLabel("To type: ");
        labelTo.setBounds(10, 150, 100, 50);
        add(labelTo);

        JLabel labelParam = new JLabel("Value: ");
        labelParam.setBounds(10, 10, 100, 50);
        add(labelParam);

        JLabel labelResult = new JLabel("Result: ");
        labelResult.setBounds(10, 350, 100, 50);
        add(labelResult);

        TextField param = new TextField();
        param.setBounds(150, 10, 200, 50);
        add(param);

        JComboBox comboBoxFrom = new JComboBox<>(getComboBoxValues());
        comboBoxFrom.setVisible(true);
        comboBoxFrom.setBounds(150, 80, 200, 50);
        comboBoxFrom.setBackground(FRAME_COLOR);
        comboBoxFrom.setBorder(new BevelBorder(BevelBorder.RAISED));
        add(comboBoxFrom);

        JComboBox comboBoxTo = new JComboBox<>(getComboBoxValues());
        comboBoxTo.setVisible(true);
        comboBoxTo.setBounds(150, 150, 200, 50);
        comboBoxTo.setBackground(FRAME_COLOR);
        comboBoxTo.setBorder(new BevelBorder(BevelBorder.RAISED));
        add(comboBoxTo);

        JButton calculateButton = new JButton("Convert");
        calculateButton.setBounds(10, 280, 350, 50);
        calculateButton.setBackground(FRAME_COLOR);
        calculateButton.setBorder(new BevelBorder(BevelBorder.RAISED));
        add(calculateButton);

        TextField result = new TextField();
        result.setBounds(150, 350, 200, 50);
        add(result);

        calculateButton.addActionListener(actionEvent -> {
            double r = 0;
            switch (converterType) {
                case TIME:
                    r = Converters.convertTime(
                            Double.parseDouble(param.getText()),
                            TimeType.valueOf((String) comboBoxFrom.getSelectedItem()),
                            TimeType.valueOf((String) comboBoxTo.getSelectedItem())
                    ); break;
                case WEIGHT:
                    r = Converters.convertWeight(
                            Double.parseDouble(param.getText()),
                            WeightType.valueOf((String) comboBoxFrom.getSelectedItem()),
                            WeightType.valueOf((String) comboBoxTo.getSelectedItem())
                    ); break;
                case VOLUME:
                    r = Converters.convertVolume(
                            Double.parseDouble(param.getText()),
                            VolumeType.valueOf((String) comboBoxFrom.getSelectedItem()),
                            VolumeType.valueOf((String) comboBoxTo.getSelectedItem())
                    ); break;
                case LENGTH:
                    r = Converters.convertLength(
                            Double.parseDouble(param.getText()),
                            LengthType.valueOf((String) comboBoxFrom.getSelectedItem()),
                            LengthType.valueOf((String) comboBoxTo.getSelectedItem())
                    ); break;
                case TEMPERATURE:
                    r = Converters.convertTemperature(
                            Double.parseDouble(param.getText()),
                            TemperatureType.valueOf((String) comboBoxFrom.getSelectedItem()),
                            TemperatureType.valueOf((String) comboBoxTo.getSelectedItem())
                    ); break;
            }
            result.setText(String.valueOf(r));
        });
    }

    private String[] getComboBoxValues() {
        switch (converterType) {
            case TIME: return getEnumValuesAsStringArray(TimeType.values());
            case WEIGHT: return getEnumValuesAsStringArray(WeightType.values());
            case VOLUME: return getEnumValuesAsStringArray(VolumeType.values());
            case LENGTH: return getEnumValuesAsStringArray(LengthType.values());
            case TEMPERATURE: return getEnumValuesAsStringArray(TemperatureType.values());
            default: return new String[] {};
        }
    }

    private String[] getEnumValuesAsStringArray(Enum[] enumValues) {
        String[] r = new String[enumValues.length];

        for (int i = 0; i < r.length; i++) {
            r[i] = enumValues[i].name();
        }
        return r;
    }
}
