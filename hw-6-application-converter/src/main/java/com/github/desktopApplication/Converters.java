package com.github.desktopApplication;

import com.github.desktopApplication.converterTypes.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Конвертация производится в обе стороны, например секунды в минуты и минуты в секунды.
 * <p>
 * Категории конвертации:
 * - Length
 * - Temperature
 * - Weight
 * - Time
 * - Volume
 */
public class Converters {

    private static final Map<TimeType, Double> timeFactor = new HashMap<>();
    private static final Map<LengthType, Double> lengthFactor = new HashMap<>();
    private static final Map<VolumeType, Double> volumeFactor = new HashMap<>();
    private static final Map<WeightType, Double> weightFactor = new HashMap<>();

    static {
        timeFactor.put(TimeType.SECOND, 1.0);
        timeFactor.put(TimeType.MINUTE, 60.0);
        timeFactor.put(TimeType.HOUR, 3600.0);
        timeFactor.put(TimeType.DAY, 86400.0);
        timeFactor.put(TimeType.WEEK, 604800.0);
        timeFactor.put(TimeType.MONTH, 2592000.0);
        timeFactor.put(TimeType.ASTRONOMICAL_MONTH, 2360592.0);

        volumeFactor.put(VolumeType.LITRE, 1.0);
        volumeFactor.put(VolumeType.CUBIC_METRE, 0.001);
        volumeFactor.put(VolumeType.GALLON, 0.264172);
        volumeFactor.put(VolumeType.PINT, 2.11338);
        volumeFactor.put(VolumeType.QUART, 1.05669);
        volumeFactor.put(VolumeType.BARREL, 0.00852168);
        volumeFactor.put(VolumeType.CUBIC_FOOT, 0.0353147);
        volumeFactor.put(VolumeType.CUBIC_INCH, 61.0237);

        weightFactor.put(WeightType.KILOGRAM, 1.0);
        weightFactor.put(WeightType.GRAM, 0.001);
        weightFactor.put(WeightType.CARAT, 0.0002);
        weightFactor.put(WeightType.ENGLISH_POUND, 0.45359237);
        weightFactor.put(WeightType.POUND, 0.45359237);
        weightFactor.put(WeightType.STONE, 6.3502949712);
        weightFactor.put(WeightType.RUSSIAN_POUND, 0.40951717924);

        lengthFactor.put(LengthType.METER, 1.0);
        lengthFactor.put(LengthType.KILOMETER, 1000.0);
        lengthFactor.put(LengthType.MILE, 1609.34);
        lengthFactor.put(LengthType.NAUTICAL_MILE, 1852.0);
        lengthFactor.put(LengthType.CABLE, 185.3184);
        lengthFactor.put(LengthType.LEAGUE, 5556.0);
        lengthFactor.put(LengthType.FEET, 0.3048);
        lengthFactor.put(LengthType.YARD, 0.9144);
    }

    public static double convertTime(double timeValue, TimeType fromType, TimeType toType) {
        return timeValue * timeFactor.get(fromType) / timeFactor.get(toType);
    }

    public static double convertWeight(double weightValue, WeightType fromType, WeightType toType) {
        return weightValue * weightFactor.get(fromType) / weightFactor.get(toType);
    }

    public static double convertVolume(double weightValue, VolumeType fromType, VolumeType toType) {
        return weightValue * volumeFactor.get(fromType) / volumeFactor.get(toType);
    }

    public static double convertLength(double lengthValue, LengthType fromType, LengthType toType) {
        return lengthValue * lengthFactor.get(fromType) / lengthFactor.get(toType);
    }

    public static double convertTemperature(double temperatureValue, TemperatureType fromType, TemperatureType toType) {
        return temperatureFromCelsius(temperatureToCelsius(temperatureValue, fromType), toType);
    }

    private static double temperatureFromCelsius(double celsius, TemperatureType temperatureType) {
        switch (temperatureType) {
            case CELSIUS:
                return celsius;
            case KELVIN:
                return celsius + 273.15;
            case FAHRENHEIT:
                return (celsius * 1.8) + 32;
            case REAUMUR:
                return celsius * 0.8;
            case ROMER:
                return (celsius * 0.525) + 7.5;
            case RANKINE:
                return (celsius + 273.15) * 1.8;
            case NEWTON:
                return celsius * 0.33;
            case DELISLE:
                return (100 - celsius) * 1.5;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static double temperatureToCelsius(double value, TemperatureType temperatureType) {
        switch (temperatureType) {
            case CELSIUS:
                return value;
            case KELVIN:
                return value - 273.15;
            case FAHRENHEIT:
                return (value - 32) * 5 / 9;
            case REAUMUR:
                return value * 1.25;
            case ROMER:
                return (value - 7.5) * 40 / 21;
            case RANKINE:
                return (value - 491.67) * 5 / 9;
            case NEWTON:
                return value * 100 / 33;
            case DELISLE:
                return (100 - value) * 2 / 3;
            default:
                throw new IllegalArgumentException();
        }
    }
}
