package com.github.desktopApplication.converterTypes;

public enum WeightType {

    KILOGRAM("Kilogram"),
    GRAM("Gram"),
    CARAT("Carat"),
    ENGLISH_POUND("English pound"),
    POUND("Pound"),
    STONE("Stone"),
    RUSSIAN_POUND("Russian pound");

    private String typeName;

    WeightType(String typeName) {
        this.typeName = typeName;
    }
}
