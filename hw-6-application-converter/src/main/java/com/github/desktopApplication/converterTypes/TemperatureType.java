package com.github.desktopApplication.converterTypes;

public enum TemperatureType {

    CELSIUS("Celsius"),
    KELVIN("Kelvin"),
    FAHRENHEIT("Fahrenheit"),
    REAUMUR("Reaumur"),
    ROMER("Romer"),
    RANKINE("Rankine"),
    NEWTON("Newton"),
    DELISLE("Delisle");

    private String typeName;

    TemperatureType(String typeName) {
        this.typeName = typeName;
    }
}
