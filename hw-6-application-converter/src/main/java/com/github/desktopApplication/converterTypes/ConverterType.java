package com.github.desktopApplication.converterTypes;

public enum ConverterType {
    TIME, WEIGHT, VOLUME, LENGTH, TEMPERATURE;
}
