package com.github.desktopApplication.converterTypes;

public enum TimeType {

    SECOND("Second"),
    MINUTE("Minute"),
    HOUR("Hour"),
    DAY("Day"),
    WEEK("Week"),
    MONTH("Month"),
    ASTRONOMICAL_MONTH("Astronomical month");

    private String timeTypeName;

    TimeType(String timeTypeName) {
        this.timeTypeName = timeTypeName;
    }
}
