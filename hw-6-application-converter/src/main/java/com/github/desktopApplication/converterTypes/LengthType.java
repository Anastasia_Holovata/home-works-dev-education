package com.github.desktopApplication.converterTypes;

public enum LengthType {

    METER("Meter"),
    KILOMETER("Kilometer"),
    MILE("Mile"),
    NAUTICAL_MILE("Nautical mile"),
    CABLE("Cable"),
    LEAGUE("League"),
    FEET("Feet"),
    YARD("Yard");

    private String typeName;

    LengthType(String typeName) {
        this.typeName = typeName;
    }
}
