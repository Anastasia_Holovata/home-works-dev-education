package com.github.desktopApplication.converterTypes;

public enum VolumeType {

    LITRE("Litre"),
    CUBIC_METRE("Cubic metre"),
    GALLON("Gallon"),
    PINT("Pint"),
    QUART("Quart"),
    BARREL("Barrel"),
    CUBIC_FOOT("Cubic foot"),
    CUBIC_INCH("Cubic inch");

    private String typeName;

    VolumeType(String typeName) {
        this.typeName = typeName;
    }
}
