package com.github.guessNumber;

public enum GuessMessage {

    GUESSED("guessed"), NO_GUESSED("no guessed"), WARMER("warmer"), COLDER("colder");

    private final String message;

    GuessMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
