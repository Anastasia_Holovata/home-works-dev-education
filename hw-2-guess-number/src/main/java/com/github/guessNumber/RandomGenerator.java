package com.github.guessNumber;

import java.util.Random;

public class RandomGenerator {

    private static final Random RANDOM = new Random();

    public static int generate(int min, int max) {
        return RANDOM.nextInt(max - min + 1) + min;
    }
}
