package com.github.guessNumber;

public class ParamsValidator {

    public static void validateMinMaxRandom(int minRandomValue, int maxRandomValue) {
        if (minRandomValue < 1) {
            throw new IllegalArgumentException("You entered too small value! Increase the minimum!");
        }
        if (maxRandomValue > 200) {
            throw new IllegalArgumentException("You entered too big value! Decrease the maximum!");
        }
        if (minRandomValue > maxRandomValue) {
            throw new IllegalArgumentException("Your min value is bigger than max value!");
        }
    }

    public static void validateCountOfAttempts(int countOfAttempts) {
        if (countOfAttempts < 1) {
            throw new IllegalArgumentException("Count of attempts can't be less than 1!");
        } else if (countOfAttempts > 15) {
            throw new IllegalArgumentException("Count of attempts can't be more than 15!");
        }
    }
}
