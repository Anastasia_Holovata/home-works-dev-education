package com.github.guessNumber;

import java.util.Scanner;

public class GuessNumber {

    private static final Scanner SCANNER = new Scanner(System.in);

    private final int minRandomValue;
    private final int maxRandomValue;
    private int countOfAttempts;

    public GuessNumber(int minRandomValue, int maxRandomValue, int countOfAttempts) {
        ParamsValidator.validateCountOfAttempts(countOfAttempts);
        ParamsValidator.validateMinMaxRandom(minRandomValue, maxRandomValue);

        this.minRandomValue = minRandomValue;
        this.maxRandomValue = maxRandomValue;
        this.countOfAttempts = countOfAttempts;
    }

    public void start() {
        MakerHiddenNumber makerHiddenNumber = new MakerHiddenNumber(minRandomValue, maxRandomValue);

        System.out.println("Hi, I've made a number from " + minRandomValue + " to " + maxRandomValue +
                " of your diapason!\nTry to guess the number in " + countOfAttempts + " attempts");

        for (int i = 0; i < countOfAttempts; i++) {
            System.out.print("Enter number: ");
            String userInput = SCANNER.nextLine();

            if ("exit".equalsIgnoreCase(userInput)) {
                System.exit(0);
            }

            int userChoice = Integer.parseInt(userInput);

            GuessMessage answer = makerHiddenNumber.guess(userChoice);

            if (GuessMessage.GUESSED.equals(answer)) {
                System.out.println(GuessMessage.GUESSED.getMessage());
                return;
            } else if (GuessMessage.NO_GUESSED.equals(answer)) {
                System.out.println("You are wrong. You have " + (countOfAttempts - i - 1) + " attempts!");
            } else {
                System.out.println("You are wrong. But you are " + answer.getMessage() +
                        ". You have " + (countOfAttempts - i - 1) + " attempts!");
            }
        }
    }

}
