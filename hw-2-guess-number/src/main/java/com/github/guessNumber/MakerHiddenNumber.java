package com.github.guessNumber;

public class MakerHiddenNumber {

    private final int hiddenNumber;
    private boolean isFirstTry = true;
    private int previousMod = Integer.MAX_VALUE;

    public MakerHiddenNumber(int minRandomValue, int maxRandomValue) {
        ParamsValidator.validateMinMaxRandom(minRandomValue, maxRandomValue);
        this.hiddenNumber = RandomGenerator.generate(minRandomValue, maxRandomValue);
    }

    public GuessMessage guess(int guessedValue) {
        if (guessedValue == hiddenNumber) {
            return GuessMessage.GUESSED;
        } else {
            if (isFirstTry) {
                isFirstTry = false;
                previousMod = Math.abs(hiddenNumber - guessedValue);
                return GuessMessage.NO_GUESSED;
            } else {
                int currentMod = Math.abs(hiddenNumber - guessedValue);

                GuessMessage answer = currentMod < previousMod ? GuessMessage.WARMER : GuessMessage.COLDER;
                previousMod = currentMod;
                return answer;
            }
        }
    }
}
