package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Дискриминант ax^2+bx+c
 */
public class Task1 {

    private static int A = 0;
    private static int B = 0;
    private static int C = 0;

    static int discriminant = 0;

    public static void main(String[] args) {

        System.out.println("Find the discriminant of square equation: ");
        inputNumbers();

        System.out.println(getDiscriminant());
        messageAboutDiscriminant(discriminant);
    }

    private static int getIntValueFromInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static void inputNumbers() {
        System.out.print("Enter A: ");
        A = getIntValueFromInput();

        System.out.print("Enter B: ");
        B = getIntValueFromInput();

        System.out.print("Enter C: ");
        C = getIntValueFromInput();

        System.out.println("We got the equation: " + A + "x^2 + " + B + "x + " + C);
    }

    private static int getDiscriminant() {
        discriminant = (int) (Math.pow(B, 2) - 4 * A * C);
        return discriminant;
    }

    private static void messageAboutDiscriminant(int discriminant) {
        if (discriminant > 0) {
            System.out.println("Discriminant is bigger than 0. The equation has 2 roots");
        } else if (discriminant == 0) {
            System.out.println("Discriminant equals 0. The equation has 1 root");
        } else {
            System.out.println("Discriminant is less than 0, The equation hasn't got any roots");
        }
    }
}
