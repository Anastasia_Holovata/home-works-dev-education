package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Даны 5 чисел (тип int). Вывести вначале наименьшее, а затем наибольшее из данных чисел.
 */
public class Task3 {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int[] array;


    public static void main(String[] args) {
        inputNumbers();
        //numbersToArray();
        // TODO: Correct realization of sorting the array
        minAndMaxValue(array);
    }
    private static int getIntValueFromInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static void inputNumbers() {
        System.out.print("Enter A: ");
        a = getIntValueFromInput();

        System.out.print("Enter B: ");
        b = getIntValueFromInput();

        System.out.print("Enter C: ");
        c = getIntValueFromInput();

        System.out.print("Enter D: ");
        d = getIntValueFromInput();

        System.out.print("Enter E: ");
        e = getIntValueFromInput();

        System.out.println("We got numbers: " + a + ", "+ b + ", " + c + ", " + d + ", " + e + ".");
    }

    private static void numbersToArray() {
        array = new int[] {a, b, c, d, e};
        System.out.println(Arrays.toString(array));
    }

    private static void minAndMaxValue(int[] array) {
        numbersToArray();
        Arrays.sort(array);
        System.out.println("The minimum value equals " + array[0]);
        Arrays.sort(array, array.length-1, 0);
        System.out.println("The maximum value equals " + (array.length-1));
    }
}
