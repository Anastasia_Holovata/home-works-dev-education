package com.github.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleHttpRequest {

    private static final int INDEX_OF_METHOD_VALUE = 0;
    private static final int INDEX_OF_PROTOCOL_VALUE = 1;

    private static final String METHOD_POST = "POST";
    private static final String METHOD_PUT = "PUT";

    private static final String HEADER_CONTENT_LENGTH = "Content-Length";

    private String method;
    private String protocol;
    private final Map<String, String> headers = new HashMap<>();
    private Optional<String> body = Optional.empty();


    public SimpleHttpRequest(SocketChannel channel) throws IOException {
        parse(readRequest(channel));
    }

    private String readRequest(SocketChannel channel) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        ByteBuffer byteBuffer = ByteBuffer.allocate(2048);
        for(int bytes = 2048; bytes == 2048; ){
            bytes = channel.read(byteBuffer);
            if(bytes == -1){
                throw new IOException();
            }
            byteBuffer.flip();
            stringBuilder.append(new String(byteBuffer.array()));
        }
        byteBuffer.clear();
        return stringBuilder.toString();
    }


    private void parse(String requestAsString) {
        List<String> requestDataLines = Stream.of(requestAsString.split("\n"))
                .map (String::new)
                .collect(Collectors.toList());

        if (requestDataLines.size() == 0) {
            throw new RuntimeException("");
        }

        String[] methodAndProtocolDataAsStringArray = requestDataLines.get(0).split("/");

        if (methodAndProtocolDataAsStringArray.length > 1) {
            method = methodAndProtocolDataAsStringArray[INDEX_OF_METHOD_VALUE].trim();
            protocol = methodAndProtocolDataAsStringArray[INDEX_OF_PROTOCOL_VALUE].trim();
        } else {
            throw new RuntimeException("");
        }

        requestDataLines.remove(0);

        requestDataLines.forEach(requestLineDataAsString -> {
            String[] headerDataPair = requestLineDataAsString.split(":");

            if (headerDataPair.length == 2) {
                headers.put(headerDataPair[0].trim(), headerDataPair[1].trim());
            }
        });
    }

    public String getMethod() {
        return method;
    }

    public String getProtocol() {
        return protocol;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Optional<String> getBody() {
        return body;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder
                .append(this.method)
                .append(" / ")
                .append(this.protocol)
                .append("\r\n");

        for (String header : this.headers.keySet()) {
            builder
                    .append(header)
                    .append(":")
                    .append(this.headers.get(header))
                    .append("\r\n");
        }

        builder
                .append("\r\n")
                .append(this.body)
                .append("\r\n\r\n");

        return builder.toString();
    }
}
