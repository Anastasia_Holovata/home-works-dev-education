package com.github.nio;

public class RequestHandler {

    public static SimpleHttpResponse handle(SimpleHttpRequest request, SimpleHttpResponse response) {
        System.out.println("Processing request: \n" + request);

        switch (request.getMethod()) {
            case "GET": return get(request, response);
            case "POST": return post(request, response);
            case "PUT": return put(request, response);
            case "DELETE": return delete(request, response);
            default: throw new RuntimeException("");
        }
    }

    private static SimpleHttpResponse get(SimpleHttpRequest request, SimpleHttpResponse response) {
        return setBodyToResponse(response, getBodyAsHtml(request));
    }

    private static SimpleHttpResponse post(SimpleHttpRequest request, SimpleHttpResponse response) {
        return setBodyToResponse(response, getBodyAsHtml(request));
    }

    private static SimpleHttpResponse put(SimpleHttpRequest request, SimpleHttpResponse response) {
        return setBodyToResponse(response, getBodyAsHtml(request));
    }

    private static SimpleHttpResponse delete(SimpleHttpRequest request, SimpleHttpResponse response) {
        return setBodyToResponse(response, getBodyAsHtml(request));
    }

    private static SimpleHttpResponse setBodyToResponse(SimpleHttpResponse response, String body) {
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Type", "text/plain");
        response.addHeader("Content-Length", "" + body.length());
        response.setBody(body);

        return response;
    }

    private static String getBodyAsHtml(SimpleHttpRequest request) {
        return "<p>[" + request.getMethod() + "] Hello dude from my stupid socket server</p>";
    }

}
