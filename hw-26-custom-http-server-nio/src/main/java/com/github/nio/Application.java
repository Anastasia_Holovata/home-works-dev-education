package com.github.nio;

public class Application {

    public static void main(String[] args) {
        try {
            new Server().start(8081);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
