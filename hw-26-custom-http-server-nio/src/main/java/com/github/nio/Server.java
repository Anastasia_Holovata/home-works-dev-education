package com.github.nio;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class Server {

    public void start(int port) throws IOException {
        ServerSocketChannel serverSocket;
        serverSocket = ServerSocketChannel.open();
        serverSocket.socket().bind(new InetSocketAddress(port));
        System.out.println("Server started. Port: " + port);
        SocketChannel channel;

        while (true) {
            channel = serverSocket.accept();
            SimpleHttpRequest request = new SimpleHttpRequest(channel);
            SimpleHttpResponse response = new SimpleHttpResponse();
            RequestHandler.handle(request, response);
            channel.write(ByteBuffer.wrap(response.toString().getBytes(StandardCharsets.UTF_8)));
            channel.finishConnect();
        }
    }
}
