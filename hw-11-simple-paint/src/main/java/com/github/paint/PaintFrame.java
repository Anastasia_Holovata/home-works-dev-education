package com.github.paint;

import com.github.paint.panels.ColorPanel;
import com.github.paint.panels.FilePanel;
import com.github.paint.panels.PaintPanel;
import com.github.paint.panels.StrokePanel;

import javax.swing.*;
import java.awt.*;

public class PaintFrame extends JFrame {

    public PaintFrame(
            PaintPanel paintPanel,
            ColorPanel colorPanel,
            StrokePanel strokeChooserPanel,
            FilePanel FIlePanel) throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setBounds(10, 10, 1050, 660);
        add(paintPanel);
        add(colorPanel);
        add(strokeChooserPanel);
        add(FIlePanel);
        setVisible(Boolean.TRUE);
    }
}
