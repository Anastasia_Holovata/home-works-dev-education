package com.github.paint.panels;

import com.github.paint.commands.FileCommands;

import javax.swing.*;

public class FilePanel extends JPanel {

    public FilePanel(FileCommands fileCommands) {
        setLayout(null);
        setBounds(820, 10, 200, 50);

        JButton btnSave = new JButton("Save");
        JButton btnOpen = new JButton("Open");

        btnSave.addActionListener(fileCommands.actionSave());
        btnOpen.addActionListener(fileCommands.actionOpen());

        btnSave.setBounds(20, 10, 70, 30);
        btnOpen.setBounds(100, 10, 70, 30);

        add(btnSave);
        add(btnOpen);

        setVisible(Boolean.TRUE);
    }

}

