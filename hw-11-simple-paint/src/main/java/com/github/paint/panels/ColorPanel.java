package com.github.paint.panels;

import com.github.paint.models.Context;

import javax.swing.*;
import java.awt.*;

public class ColorPanel extends JColorChooser {

    public ColorPanel() {
        setLayout(null);
        setBounds(820, 80, 200, 50);

        JButton btnChooseColor = new JButton("Color");
        btnChooseColor.setBounds(10, 10, 180, 30);

        btnChooseColor.addActionListener(e -> {
            Context.setColor(JColorChooser.showDialog(this, "Select a color", Context.getColor()));
        });

        add(btnChooseColor);
        setVisible(Boolean.TRUE);
    }

    @Override
    public void setColor(Color color) { Context.setColor(color); }
}
