package com.github.paint.panels;

import com.github.paint.models.Context;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class StrokePanel extends JPanel {

    public StrokePanel() {
        setLayout(null);
        setBounds(820, 150, 200, 50);

        JSlider slider = new JSlider(1, 30, 1);
        slider.setBounds(10, 10, 180, 30);

        slider.addChangeListener(e -> Context.setStroke(((JSlider)e.getSource()).getValue()));

        add(slider);
        setVisible(Boolean.TRUE);
    }
}
