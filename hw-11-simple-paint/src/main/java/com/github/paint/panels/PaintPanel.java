package com.github.paint.panels;

import com.github.paint.models.Context;
import com.github.paint.models.VectorData;
import com.github.paint.models.VectorNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class PaintPanel extends JPanel implements MouseListener, MouseMotionListener {

    private int x;
    private int y;

    public PaintPanel() {
        setBounds(10, 10, 800, 600);
        addMouseListener(this);
        addMouseMotionListener(this);
        setVisible(Boolean.TRUE);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Point p = e.getPoint();
        this.x = p.x;
        this.y = p.y;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Point p = e.getPoint();

        int newX = p.x;
        int newY = p.y;

        VectorData vectorData = new VectorData(this.x, this.y, newX, newY, Context.getStroke(), Context.getColor().getRGB());
        Context.getLines().add(new VectorNode(vectorData));

        this.x = newX;
        this.y = newY;

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (VectorNode line : Context.getLines()) {
            line.paint(g);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // DO NOTHING
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        // DO NOTHING
    }
}
