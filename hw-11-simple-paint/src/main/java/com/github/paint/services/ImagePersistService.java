package com.github.paint.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.github.paint.models.Context;
import com.github.paint.models.VectorData;
import com.github.paint.models.VectorNode;
import com.github.paint.panels.PaintPanel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ImagePersistService {

    private static ObjectMapper jsonObjectMapper = new ObjectMapper();
    private static ObjectMapper xmlObjectMapper = new XmlMapper();
    private static ObjectMapper yamlObjectMapper = new ObjectMapper(new YAMLFactory());
    private static ObjectMapper csvObjectMapper = new CsvMapper()
            .enable(CsvParser.Feature.SKIP_EMPTY_LINES)
            .enable(CsvParser.Feature.TRIM_SPACES)
            .enable(CsvParser.Feature.WRAP_AS_ARRAY)
            .enable(CsvParser.Feature.INSERT_NULLS_FOR_MISSING_COLUMNS);

    static {
        csvObjectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    public enum Format {
        JSON, XML, YAML, CSV;
    }

    public void save(File file, List<VectorNode> lines) {
        ObjectMapper objectMapper = getObjectMapper(getFormat(file.getName()));

        try {
            List<VectorData> collect = lines.stream().map(VectorNode::getData).collect(Collectors.toList());

            if (objectMapper instanceof CsvMapper) {
                CsvSchema schema = getSchema();
                objectMapper.writer(schema).writeValue(file, collect);
            } else {
                objectMapper.writeValue(file, collect);
            }

        } catch (IOException e) {
            throw new RuntimeException("");
        }
    }

    private CsvSchema getSchema() {
        return CsvSchema.builder()
                .addColumn("firstX")
                .addColumn("firstY")
                .addColumn("lastX")
                .addColumn("lastY")
                .addColumn("color")
                .addColumn("width")
                .build();
    }

    public void read(File file) {
        ObjectMapper objectMapper = getObjectMapper(getFormat(file.getName()));

        try {
            TypeReference<List<VectorData>> type = new TypeReference<>() {
            };

            List<VectorNode> data;
            if (objectMapper instanceof CsvMapper) {
                CsvSchema schema = ((CsvMapper) objectMapper).schemaFor(VectorData.class)
                        .withSkipFirstDataRow(true)
                        .sortedBy("firstX", "firstY", "lastX", "lastY", "color", "width")
                        .withColumnSeparator(',').withoutQuoteChar();

                MappingIterator<VectorData> iterator = objectMapper
                        .readerFor(VectorData.class)
                        .with(schema).readValues(file);

                data = iterator.readAll().stream().map(VectorNode::new).collect(Collectors.toList());
            } else {
                data = objectMapper.readValue(file, type).stream().map(VectorNode::new).collect(Collectors.toList());
            }

            Context.setLines(data);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static Format getFormat(String fileName) {
        int lastIndexOf = fileName.lastIndexOf(".");
        if (lastIndexOf == -1) {
            throw new RuntimeException("Unknown file extension!");
        }
        return Format.valueOf(fileName.substring(lastIndexOf + 1).toUpperCase());
    }

    private static ObjectMapper getObjectMapper(Format format) {
        switch (format) {
            case JSON: return jsonObjectMapper;
            case XML: return xmlObjectMapper;
            case YAML: return yamlObjectMapper;
            case CSV: return csvObjectMapper;
            default: throw new RuntimeException("Unknown file extension!");
        }
    }
}
