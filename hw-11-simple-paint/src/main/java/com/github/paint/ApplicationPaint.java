package com.github.paint;

import com.github.paint.commands.FileCommands;
import com.github.paint.panels.ColorPanel;
import com.github.paint.panels.FilePanel;
import com.github.paint.panels.PaintPanel;
import com.github.paint.panels.StrokePanel;
import com.github.paint.services.ImagePersistService;

import java.awt.*;

public class ApplicationPaint {

    public static void main(String[] args) {
        PaintPanel paintPanel = new PaintPanel();
        ColorPanel colorPanel = new ColorPanel();
        StrokePanel strokePanel = new StrokePanel();
        ImagePersistService imagePersistService = new ImagePersistService();
        FileCommands fileCommands = new FileCommands(paintPanel, imagePersistService);
        FilePanel filePanel = new FilePanel(fileCommands);

        PaintFrame paintFrame = new PaintFrame(
                paintPanel,
                colorPanel,
                strokePanel,
                filePanel);

        paintFrame.getContentPane().setBackground(new Color(146, 151, 131, 163));
        colorPanel.setBackground(new Color(221, 226, 205));
        strokePanel.setBackground(new Color(221, 226, 205));
        filePanel.setBackground(new Color(221, 226, 205));
    }
}
