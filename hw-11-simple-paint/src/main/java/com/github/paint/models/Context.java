package com.github.paint.models;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Context {

    private static List<VectorNode> lines = new ArrayList<>();
    private static int stroke = 1;
    private static Color color = Color.RED;

    public static List<VectorNode> getLines() {
        return lines;
    }

    public static int getStroke() {
        return stroke;
    }

    public static Color getColor() {
        return color;
    }

    public static void setLines(List<VectorNode> lines) {
        Context.lines = lines;
    }

    public static void setStroke(int stroke) {
        Context.stroke = stroke;
    }

    public static void setColor(Color color) {
        Context.color = color;
    }
}
