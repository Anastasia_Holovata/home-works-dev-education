package com.github.paint.models;

import java.awt.*;
import java.io.Serializable;
import java.util.Locale;

public class VectorNode extends Component {

    private VectorData data;

    public VectorNode(VectorData vectorData) {
        this.data = vectorData;
        setLocale(Locale.ENGLISH);
    }

    public VectorData getData() {
        return data;
    }

    public void setData(VectorData data) {
        this.data = data;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(data.getWidth()));
        g2d.setColor(new Color(data.getColor()));
        g2d.drawLine(data.getFirstX(), data.getFirstY(), data.getLastX(), data.getLastY());
    }
}