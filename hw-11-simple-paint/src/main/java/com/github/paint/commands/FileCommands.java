package com.github.paint.commands;

import com.github.paint.models.Context;
import com.github.paint.panels.PaintPanel;
import com.github.paint.services.ImagePersistService;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.io.File;

public class FileCommands {

    private PaintPanel paintPanel;
    private ImagePersistService imagePersistService;

    public FileCommands(PaintPanel paintPanel, ImagePersistService imagePersistService) {
        this.paintPanel = paintPanel;
        this.imagePersistService = imagePersistService;
    }

    public ActionListener actionOpen() {
        return e -> {
            imagePersistService.read(getFileFromDialog());
            paintPanel.repaint();
        };
    }

    public ActionListener actionSave() {
        return e -> imagePersistService.save(getFileFromDialog(), Context.getLines());
    }

    private File getFileFromDialog() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(
                new FileNameExtensionFilter("Allowed formats", "json", "csv", "xml", "yaml"));
        int ret = fileChooser.showDialog(null, "Select file");
        if (ret == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        } else {
            throw new RuntimeException("Selected incorrect file!");
        }
    }

}
