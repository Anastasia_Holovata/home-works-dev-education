package com.github.jwtToken;

import io.jsonwebtoken.Claims;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TokenProviderTest {

    @org.junit.jupiter.api.Test
    void createAndDecodeToken() {
        UUID jwtId = UUID.randomUUID();
        String jwtIssuer = "Test JWT";
        String jwtSubject = "Test subject";
        int jwtTimeToLive = 123456;

        String token = TokenProvider.generateToken(jwtId, jwtIssuer, jwtSubject, jwtTimeToLive);

        Claims claims = TokenProvider.decodeToken(token);

        assertEquals(jwtId.toString(), claims.getId());
        assertEquals(jwtIssuer, claims.getIssuer());
        assertEquals(jwtSubject, claims.getSubject());
    }

    @org.junit.jupiter.api.Test
    void isValidToken() {
        String validToken = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJhZmI0OTc4YS1jMWM4LTQ2MTUtOGUzYi1jZmIwMDI5Mjg1MzEiLCJpYXQiOjE2MjIzMTY1MzUsInN1YiI6IkFuZHJldyIsImlzcyI6IkpXVCBEZW1vIiwiZXhwIjoxNjIyMzE3MzM1fQ.eiaXBH5h5PUxkvAM1feLjZNywL0s5F7V10bskTiUIOQ";
        String notValidToken = "ayJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJhZmI0OTc4YS1jMWM4LTQ2MTUtOGUzYi1jZmIwMDI5Mjg1MzEiLCJpYXQiOjE2MjIzMTY1MzUsInN1YiI6IkFuZHJldyIsImlzcyI6IkpXVCBEZW1vIiwiZXhwIjoxNjIyMzE3MzM1fQ.eiaXBH5h5PUxkvAM1feLjZNywL0s5F7V10bskTiUIOQ";

        assertTrue(TokenProvider.isValidToken(validToken));
        assertFalse(TokenProvider.isValidToken(notValidToken));
    }
}