package com.github.functions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task2Test {

    private String expectedStringNumber;
    private int intNumber;

    public Task2Test(String expectedStringNumber, int intNumber) {
        this.expectedStringNumber = expectedStringNumber;
        this.intNumber = intNumber;
    }

    @Test
    public void parseNumberToLine() {
        assertEquals(expectedStringNumber, Task2.parseNumberToLine(intNumber));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { "zero", 0 },
                { "seven", 7 },
                { "ten", 10 },
                { "eleven", 11 },
                { "seventeen", 17 },
                { "twenty", 20 },
                { "sixty eight", 68 },
                { "one hundred", 100 },
                { "one hundred five", 105 },
                { "one hundred ten", 110 },
                { "one hundred thirteen", 113 },
                { "one hundred twenty", 120 },
                { "one hundred sixty eight", 168 },
                { "three hundred", 300 },
                { "three hundred nine", 309 },
                { "three hundred ten", 310 },
                { "nine hundred", 900 },
                { "nine hundred ninety nine", 999 }
        });
    }
}
