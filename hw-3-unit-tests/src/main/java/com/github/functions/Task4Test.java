package com.github.functions;

import org.junit.Test;

import static com.github.functions.Task4.Point;
import static org.junit.Assert.assertEquals;

public class Task4Test {

    private static final double DELTA = 1e-15;

    @Test
    public void calculateDistanceBetweenTwoPoints() {
        Point pointOne = new Point(-2, 4);
        Point pointTwo = new Point(8, 5);

        double expected = 10.04987562112089;
        double actual = Task4.calculateDistanceBetweenTwoPoints(pointOne, pointTwo);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void calculateDistanceBetweenTwoSamePoints() {
        Point pointOne = new Point(2, 2);
        Point pointTwo = new Point(2, 2);

        double expected = 0.0;
        double actual = Task4.calculateDistanceBetweenTwoPoints(pointOne, pointTwo);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void calculateDistanceBetweenTwoDiagonalOppositePoints() {
        Point pointOne = new Point(1, 1);
        Point pointTwo = new Point(-1, -1);

        double expected = 2.8284271247461903;
        double actual = Task4.calculateDistanceBetweenTwoPoints(pointOne, pointTwo);
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void calculateDistanceBetweenTwoMirroredPoints() {
        Point pointOne = new Point(5, 2);
        Point pointTwo = new Point(5, -2);

        double expected = 4.0;
        double actual = Task4.calculateDistanceBetweenTwoPoints(pointOne, pointTwo);
        assertEquals(expected, actual, DELTA);
    }
}