package com.github.functions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task3Test {

    private int expectedIntNumber;
    private String stringNumber;

    public Task3Test(int expectedIntNumber, String stringNumber) {
        this.expectedIntNumber = expectedIntNumber;
        this.stringNumber = stringNumber;
    }

    @Test
    public void parseStringNumberToInt() {
        assertEquals(expectedIntNumber, Task3.parseStringNumberToInt(stringNumber));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 0, "zero" },
                { 7, "seven" },
                { 11, "eleven" },
                { 60, "sixty" },
                { 68, "sixty eight" },
                { 100, "one hundred" },
                { 205, "two hundred five" },
                { 768, "seven hundred sixty eight" },
                { 920, "nine hundred twenty" }
        });
    }
}