package com.github.functions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task1Test {

    private String dayAsString;
    private int dayAsInt;

    public Task1Test(String dayAsString, int dayAsInt) {
        this.dayAsString = dayAsString;
        this.dayAsInt = dayAsInt;
    }

    @Test
    public void getStringTheDayOfWeek() {
        assertEquals(dayAsString, Task1.getStringTheDayOfWeek(dayAsInt));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { "Monday", 1 },
                { "Wednesday", 3 },
                { "Saturday", 6 },
                { "Not found", 8 },
                { "Thursday", 04 }
        });
    }
}