package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task4Test {

    private int expectedNFactorial;
    private int number;

    public Task4Test(int expectedNFactorial, int number) {
        this.expectedNFactorial = expectedNFactorial;
        this.number = number;
    }
    @Test
    public void calculateNFactorial() {
        assertEquals(expectedNFactorial, Task4.calculateNFactorial(number));
    }
    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 1, 0 },
                { 1, 1 },
                { 6, 3 },
                { 24, 4 },
                { 120, 5 },
                { 5040, 7 }
        });
    }
}