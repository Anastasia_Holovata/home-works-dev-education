package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task5Test {

    private int expectedSumOfNumbers;
    private int number;

    public Task5Test(int expectedSumOfNumbers, int number) {
        this.expectedSumOfNumbers = expectedSumOfNumbers;
        this.number = number;
    }

    @Test
    public void findSumOfNumbers() {
        assertEquals(expectedSumOfNumbers, Task5.findSumOfNumbers(number));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 2, 2 },
                { 6, 15 },
                { 8, 224 },
                { 17, 467 },
                { 21, 15555 }
        });
    }
}