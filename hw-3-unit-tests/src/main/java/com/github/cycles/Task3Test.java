package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task3Test {

    private int expectedRoot;
    private int number;

    public Task3Test(int expectedRoot, int number) {
        this.expectedRoot = expectedRoot;
        this.number = number;
    }

    @Test
    public void getRootBySeq() {
        assertEquals(expectedRoot, Task3.getRootBySeq(number));
        assertEquals(3, Task3.getRootBySeq(14));
        assertEquals(5, Task3.getRootBySeq(27));
        assertEquals(6, Task3.getRootBySeq(38));
        assertEquals(8, Task3.getRootBySeq(64));
    }

    @Test
    public void getRootByBinarySearch() {
        assertEquals(expectedRoot, Task3.getRootByBinarySearch(number));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 1, 1 },
                { 3, 14 },
                { 5, 27 },
                { 6, 38 },
                { 8, 64 }
        });
    }
}