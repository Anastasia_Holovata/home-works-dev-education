package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class Task2Test {

    private boolean expected;
    private int simpleNumber;

    public Task2Test(boolean expected, int simpleNumber) {
        this.expected = expected;
        this.simpleNumber = simpleNumber;
    }

    @Test
    public void isSimpleNumber() {
        assertEquals(expected, Task2.isSimpleNumber(simpleNumber));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { true, 2 },
                { true, 3 },
                { true, 5 },
                { true, 137 },
                { true, 997 },
                { false, 0 },
                { false, 1 },
                { false, 4 },
                { false, 96 },
                { false, 144 },
        });
    }
}