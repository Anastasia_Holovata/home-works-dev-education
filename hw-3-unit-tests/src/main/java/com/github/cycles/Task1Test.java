package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task1Test {

    private int expected;
    private int firstParam;
    private int secondParam;

    public Task1Test(int expected, int firstParam, int secondParam) {
        this.expected = expected;
        this.firstParam = firstParam;
        this.secondParam = secondParam;
    }

    @Test
    public void getSumEvenNumbers() {
        assertEquals(expected, Task1.getSumEvenNumbers(firstParam, secondParam));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 20, 1, 10 },
                { 600, 1, 50 },
                { 2450, 1, 99 },
                { 2450, 1, 100 },
                { 0, 0, 0 }
        });
    }
}