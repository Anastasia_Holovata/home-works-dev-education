package com.github.cycles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task6Test {

    private int expectedReversedNumber;
    private int number;

    public Task6Test(int expectedReversedNumber, int number) {
        this.expectedReversedNumber = expectedReversedNumber;
        this.number = number;
    }

    @Test
    public void reverseNumber() {
        assertEquals(expectedReversedNumber, Task6.reverseNumber(number));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 1, 1 },
                { 21, 12 },
                { 321, 123 },
                { 1202, 2021 },
                { 00001, 10000 },
                { 54321, 12345 }
        });
    }
}