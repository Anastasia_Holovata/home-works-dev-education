package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task2Test {

    private static final double DELTA = 1e-15;
    private double expectedDoubleMaxNumber;
    private double[] array;

    public Task2Test(double expectedDoubleMaxNumber, double[] array) {
        this.expectedDoubleMaxNumber = expectedDoubleMaxNumber;
        this.array = array;
    }

    @Test
    public void findMax() {
        assertEquals(expectedDoubleMaxNumber, Task2.findMax(array), DELTA);
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 38.0, new double[] {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11} },
                { 12.65, new double[] {9.8, 3.04, 1.0, 12.65, 0.04, 7.0, 5.01, 11.11} },
                { 11.11, new double[] {9.8, 3.04, 1.0, 0.04, 7.0, 5.01, 11.11} },
                { 9.8, new double[] {9.8, 3.04, 1.0, 0.04, 7.0, 5.01} },
                { 7.0, new double[] {3.04, 1.0, 0.04, 7.0, 5.01} }
        });
    }
}