package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task1Test {

    private static final double DELTA = 1e-15;
    private double expectedDoubleMinNumber;
    private double[] array;

    public Task1Test(double expectedDoubleMinNumber, double[] array) {
        this.expectedDoubleMinNumber = expectedDoubleMinNumber;
        this.array = array;
    }

    @Test
    public void findMin() {
        assertEquals(expectedDoubleMinNumber, Task1.findMin(array), DELTA);
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][]{
                {0.04, new double[] {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11}},
                {1.0, new double[] {9.9, 3.04, 1.0, 12.65, 38.0, 7.0, 5.01, 11.11}},
                {3.04, new double[] {9.9, 3.04, 12.65, 38.0, 7.0, 5.01, 11.11}},
                {5.01, new double[] {9.9, 12.65, 38.0, 7.0, 5.01, 11.11}},
                {7.0, new double[] {9.9, 12.65, 38.0, 7.0, 11.11}}
        });
    }
}