package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task5Test {

    private int expectedSumOfElementsWithOddIndexes;
    private int[] array;

    public Task5Test(int expectedSumOfElementsWithOddIndexes, int[] array) {
        this.expectedSumOfElementsWithOddIndexes = expectedSumOfElementsWithOddIndexes;
        this.array = array;
    }

    @Test
    public void findSumElementsWithOddIndexes() {
        assertEquals(expectedSumOfElementsWithOddIndexes, Task5.findSumElementsWithOddIndexes(array));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 205, new int[] {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89} },
                { 57, new int[] {1, 12, 123, 32, 3, 0, -3, -35, 35, 48, 189} },
                { 5, new int[] {157, -123, 200, 56, 34, 45, -18, 23, 305, 4, 89} },
                { 118, new int[] {123, 89, 56, 45, 11, 7, 4, 0, -8, -23} },
                { 221, new int[] {-23, -8, 0, 4, 7, 11, 23, 35, 45, 56, 89, 123} }
        });
    }
}