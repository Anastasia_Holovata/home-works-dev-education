package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class Task9Test {

    private int[] arrayAfterSorting;
    private int[] arrayBeforeSorting;

    public Task9Test(int[] arrayAfterSorting, int[] arrayBeforeSorting) {
        this.arrayAfterSorting = arrayAfterSorting;
        this.arrayBeforeSorting = arrayBeforeSorting;
    }

    @Test
    public void sortingByBubbleSort() {
        assertArrayEquals(
                arrayAfterSorting,
                Task9.sortingByBubbleSort(Arrays.copyOf(arrayBeforeSorting, arrayBeforeSorting.length)));
    }

    @Test
    public void sortingBySelectSort() {
        assertArrayEquals(
                arrayAfterSorting,
                Task9.sortingBySelectSort(Arrays.copyOf(arrayBeforeSorting, arrayBeforeSorting.length)));
    }

    @Test
    public void sortingByInsertSort() {
        assertArrayEquals(
                arrayAfterSorting,
                Task9.sortingByInsertSort(Arrays.copyOf(arrayBeforeSorting, arrayBeforeSorting.length)));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new int[][][] {
                {new int[] {-112, -23, -8, 0, 4, 7, 11, 35, 45, 56, 89, 123},
                        new int[] {-8, -23, 35, 4, 89, -112, 11, 123, 0, 56, 7, 45} },
                { new int[] {-35, -3, 0, 1, 3, 12, 32, 35, 48, 123, 189},
                        new int[] {0, -3, -35, 35, 48, 189, 1, 12, 123, 32, 3} },
                { new int[] {-123, -18, 4, 23, 34, 45, 56, 89, 157, 200, 305},
                        new int[] {157, -123, 200, 56, 34, 45, -18, 23, 305, 4, 89} }
        });
    }
}