package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task8Test {

    private int[] expectedSwappedArray;
    private int[] array;

    public Task8Test(int[] expectedSwappedArray, int[] array) {
        this.expectedSwappedArray = expectedSwappedArray;
        this.array = array;
    }

    @Test
    public void swapElementsOfArray() {
        assertEquals(Arrays.toString(expectedSwappedArray), Arrays.toString(Task8.swapElementsOfArray(array)));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new int[][][] {
                {new int[] {-8, -23, 35, 4, 89, -112, 11, 123, 0, 56, 7, 45},
                        new int[] {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89, -112} },
                { new int[] {0, -3, -35, 35, 48, 189, 1, 12, 123, 32, 3},
                        new int[] {1, 12, 123, 32, 3, 0, -3, -35, 35, 48, 189} },
                { new int[] {45, -18, 23, 305, 4, 89, 157, -123, 200, 56, 34},
                        new int[] {157, -123, 200, 56, 34, 45, -18, 23, 305, 4, 89} },
                { new int[] {7, 4, 0, -8, -23, 123, 89, 56, 45, 11},
                        new int[] {123, 89, 56, 45, 11, 7, 4, 0, -8, -23} },
                { new int[] {23, 35, 45, 56, 89, 123, -23, -8, 0, 4, 7, 11},
                        new int[] {-23, -8, 0, 4, 7, 11, 23, 35, 45, 56, 89, 123} }
        });
    }
}