package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task3Test {

    private static final double DELTA = 1e-15;
    private int expectedDoubleMinIndex;
    private double[] array;

    public Task3Test(int expectedDoubleMinIndex, double[] array) {
        this.expectedDoubleMinIndex = expectedDoubleMinIndex;
        this.array = array;
    }

    @Test
    public void minIndexOfElementInArray() {
        assertEquals(expectedDoubleMinIndex, Task3.minIndexOfElementInArray(array), DELTA);
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 5, new double[] {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11} },
                { 2, new double[] {9.9, 3.04, 1.0, 12.65, 38.0, 7.0, 5.01, 11.11} },
                { 1, new double[] {9.9, 3.04, 12.65, 38.0, 7.0, 5.01, 11.11} },
                { 4, new double[] {9.9, 12.65, 38.0, 7.0, 5.01, 11.11} },
                { 3, new double[] {9.9, 12.65, 38.0, 7.0, 11.11} }
        });
    }
}