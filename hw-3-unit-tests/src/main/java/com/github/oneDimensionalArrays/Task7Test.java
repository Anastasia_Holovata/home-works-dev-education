package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task7Test {

    private int expectedAmountOfOddElements;
    private int[] array;

    public Task7Test(int expectedAmountOfOddElements, int[] array) {
        this.expectedAmountOfOddElements = expectedAmountOfOddElements;
        this.array = array;
    }

    @Test
    public void amountOfOddElementsInArray() {
        assertEquals(expectedAmountOfOddElements, Task7.amountOfOddElementsInArray(array));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 7, new int[] {11, 123, 0, 56, 7, 45, 8, 23, 35, 4, 89} },
                { 6, new int[] {1, 12, 123, 32, 3, 0, 3, 35, 45, 48, 188} },
                { 5, new int[] {157, 123, 200, 56, 34, 45, 18, 23, 300, 4, 89} },
                { 4, new int[] {124, 89, 56, 45, 10, 7, 4, 0, 8, 23} },
                { 10, new int[] {23, 9, 0, 4, 7, 11, 23, 35, 45, 57, 89, 123} }
        });
    }
}