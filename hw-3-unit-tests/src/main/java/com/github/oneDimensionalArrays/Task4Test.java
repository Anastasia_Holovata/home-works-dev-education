package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task4Test {

    private static final double DELTA = 1e-15;
    private int expectedDoubleMaxIndex;
    private double[] array;

    public Task4Test(int expectedDoubleMaxIndex, double[] array) {
        this.expectedDoubleMaxIndex = expectedDoubleMaxIndex;
        this.array = array;
    }

    @Test
    public void maxIndexOfElementInArray() {
        assertEquals(expectedDoubleMaxIndex, Task4.maxIndexOfElementInArray(array), DELTA);
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 4, new double[] {9.8, 3.04, 1.0, 12.65, 38.0, 0.04, 7.0, 5.01, 11.11} },
                { 3, new double[] {9.8, 3.04, 1.0, 12.65, 0.04, 7.0, 5.01, 11.11} },
                { 6, new double[] {9.8, 3.04, 1.0, 0.04, 7.0, 5.01, 11.11} },
                { 0, new double[] {9.8, 3.04, 1.0, 0.04, 7.0, 5.01} },
                { 3, new double[] {3.04, 1.0, 0.04, 7.0, 5.01} }
        });
    }
}