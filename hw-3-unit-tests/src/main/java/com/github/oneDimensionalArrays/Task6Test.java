package com.github.oneDimensionalArrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task6Test {

    private int[] expectedReversedArray;
    private int[] array;

    public Task6Test(int[] expectedReversedArray, int[] array) {
        this.expectedReversedArray = expectedReversedArray;
        this.array = array;
    }

    @Test
    public void reverseArray() {
        assertArrayEquals(expectedReversedArray, Task6.reverseArray(array));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new int[][][] {
                { new int[] {89, 4, 35, -23, -8, 45, 7, 56, 0, 123, 11}, new int[] {11, 123, 0, 56, 7, 45, -8, -23, 35, 4, 89} },
                { new int[] {189, 48, 35, -35, -3, 0, 3, 32, 123, 12, 1}, new int[] {1, 12, 123, 32, 3, 0, -3, -35, 35, 48, 189} },
                { new int[] {89, 4, 305, 23, -18, 45, 34, 56, 200, -123, 157}, new int[] {157, -123, 200, 56, 34, 45, -18, 23, 305, 4, 89} },
                { new int[] {-23, -8, 0, 4, 7, 11, 45, 56, 89, 123}, new int[] {123, 89, 56, 45, 11, 7, 4, 0, -8, -23} },
                { new int[] {123, 89, 56, 45, 35, 23, 11, 7, 4, 0, -8, -23}, new int[] {-23, -8, 0, 4, 7, 11, 23, 35, 45, 56, 89, 123} }
        });
    }
}