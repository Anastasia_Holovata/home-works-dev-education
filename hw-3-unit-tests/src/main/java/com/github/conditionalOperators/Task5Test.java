package com.github.conditionalOperators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Task5Test {

    @Test
    public void studentMarkA() {
        String expected = "A";
        String actual = Task5.studentMark(99);

        assertEquals(expected, actual);
    }

    @Test
    public void studentMarkB() {
        assertEquals("B", Task5.studentMark(75));
    }

    @Test
    public void studentMarkC() {
        assertEquals("C", Task5.studentMark(63));
    }

    @Test
    public void studentMarkD() {
        assertEquals("D", Task5.studentMark(47));
    }

    @Test
    public void studentMarkE() {
        assertEquals("E", Task5.studentMark(39));
    }

    @Test
    public void studentMarkF() {
        assertEquals("F", Task5.studentMark(0));
    }

    @Test
    public void studentMarkWrong() {
        assertEquals("You entered the mark wrong!", Task5.studentMark(152));
    }
}