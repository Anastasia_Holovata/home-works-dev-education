package com.github.conditionalOperators;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class Task3Test {

    private int expected;
    private int firstParam;
    private int secondParam;
    private int thirdParam;

    public Task3Test(int expected, int firstParam, int secondParam, int thirdParam) {
        this.expected = expected;
        this.firstParam = firstParam;
        this.secondParam = secondParam;
        this.thirdParam = thirdParam;
    }

    @Test
    public void calculate() {
        assertEquals(expected, Task3.calculate(firstParam, secondParam, thirdParam));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][] {
                { 5, 2, 3, -5 },
                { 7, 2, -3, 5 },
                { 8, -2, 3, 5 },
                { 2, 2, -3, -5 },
                { 3, -2, 3, -5 },
                { 5, -2, -3, 5 },
                { 0, -2, -3, -5 }
        });
    }
}