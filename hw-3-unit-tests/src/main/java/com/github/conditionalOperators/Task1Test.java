package com.github.conditionalOperators;

import org.junit.Assert;
import org.junit.Test;

public class Task1Test {

    @Test
    public void checkFunctionIfaEven() {
        int aEven = 10;
        int b = 5;
        int expectedIfaEven = 50;

        int actualIfaEven = Task1.function(aEven, b);

        Assert.assertEquals(expectedIfaEven, actualIfaEven);
    }

    @Test
    public void checkFunctionIfaOdd() {
        int aOdd = 9;
        int b = 5;
        int expectedIfaOdd = 14;

        int actualIfaOdd = Task1.function(aOdd, b);

        Assert.assertEquals(expectedIfaOdd, actualIfaOdd);
    }
}