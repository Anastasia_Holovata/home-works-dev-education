package com.github.conditionalOperators;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task4Test {

    @Test
    public void calculate() {
        assertEquals(6, Task4.calculate(1, 1, 1));
        assertEquals(30, Task4.calculate(3, 3, 3));
        assertEquals(11, Task4.calculate(-4, -2, 1));
        assertEquals(2, Task4.calculate(-5, 3, 1));
        assertEquals(0, Task4.calculate(-1, -3, -1));
        assertEquals(3, Task4.calculate(0, -3, -1));
        assertEquals(8, Task4.calculate(5, 0, 0));
    }

    @Test
    public void checkGetMaxIfFirstGreater() {
        assertEquals(15, Task4.getMax(15, 6));
    }

    @Test
    public void checkGetMaxIfSecondGreater() {
        assertEquals(10, Task4.getMax(5, 10));
    }

    @Test
    public void checkGetMaxIfFirstAndSecondEquals() {
        assertEquals(4, Task4.getMax(4, 4));
    }
}