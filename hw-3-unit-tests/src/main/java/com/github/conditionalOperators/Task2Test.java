package com.github.conditionalOperators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void findFirstQuarter() {
        int x = 4;
        int y = 2;

        String expectedQuarterName = "First quarter";
        String actualQuarterName = Task2.findQuarter(x, y);

        assertEquals(expectedQuarterName, actualQuarterName);
    }

    @Test
    public void findSecondQuarter() {
        int x = -5;
        int y = 7;

        String expectedQuarterName = "Second quarter";
        String actualQuarterName = Task2.findQuarter(x, y);

        assertEquals(expectedQuarterName, actualQuarterName);
    }

    @Test
    public void findThirdQuarter() {
        int x = -12;
        int y = -4;

        String expectedQuarterName = "Third quarter";
        String actualQuarterName = Task2.findQuarter(x, y);

        assertEquals(expectedQuarterName, actualQuarterName);
    }

    @Test
    public void findFourthQuarter() {
        int x = 10;
        int y = -5;

        String expectedQuarterName = "Fourth quarter";
        String actualQuarterName = Task2.findQuarter(x, y);

        assertEquals(expectedQuarterName, actualQuarterName);
    }

    @Test
    public void findQuarterIfCoordinatesAreZeros() {
        int x = 0;
        int y = 0;

        String expectedQuarterName = "The coordinates don't belong to a quarter";
        String actualQuarterName = Task2.findQuarter(x, y);

        assertEquals(expectedQuarterName, actualQuarterName);
    }
}