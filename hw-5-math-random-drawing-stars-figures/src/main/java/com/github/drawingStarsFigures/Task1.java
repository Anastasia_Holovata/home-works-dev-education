package com.github.drawingStarsFigures;

public class Task1 {

    private static final char[][] TWO_D_ARRAY = new char[7][7];
    private static final int SQUARE_WIDTH = 7;

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        //System.out.println(task9());
    }

    public static char[][] task1() {
        System.out.println("===============Task1===============");
        for (int i = 0; i < TWO_D_ARRAY.length; i++) {
            for (int j = 0; j < TWO_D_ARRAY[i].length; j++) {
                TWO_D_ARRAY[i][j] = '*';
                System.out.print(TWO_D_ARRAY[i][j] + " ");
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task2() {
        System.out.println("\n===============Task2===============");
        for (int i = 0; i < TWO_D_ARRAY.length; i++) {
            for (int j = 0; j < TWO_D_ARRAY[i].length; j++) {
                if ((i == 0 || i == TWO_D_ARRAY.length-1) || (j == 0 || j == TWO_D_ARRAY[0].length-1)) {
                    TWO_D_ARRAY[i][j] = '*';
                    System.out.print(TWO_D_ARRAY[i][j] + "");
                }
                else {
                    System.out.print(" "); }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task3() {
        System.out.println("\n===============Task3===============");
        for (int i = 0; i < TWO_D_ARRAY.length; i++) {
            for (int j = SQUARE_WIDTH -1; j >= 0; j--) {
                if ((i == 0) || (j == SQUARE_WIDTH-1)) {
                    System.out.print("* ");
                } else if(j == i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task4() {
        System.out.println("===============Task4===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if ((i == SQUARE_WIDTH-1) || (j == 0)) {
                    System.out.print("* ");
                } else if(j == i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task5() {
        System.out.println("===============Task5===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if ((i == SQUARE_WIDTH-1) || (j == SQUARE_WIDTH-1)) {
                    System.out.print("* ");
                } else if(j == SQUARE_WIDTH - i -1) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task6() {
        System.out.println("===============Task6===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if ((i == 0) || (j == SQUARE_WIDTH-1)) {
                    System.out.print("* ");
                } else if(j == i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task7() {
        System.out.println("===============Task7===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if (j == i || j == SQUARE_WIDTH - i -1) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task8() {
        System.out.println("===============Task8===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if (i == 0) {
                    System.out.print("* ");
                } else if (i > SQUARE_WIDTH / 2) {
                    System.out.print("  ");
                } else if (j == i || j == SQUARE_WIDTH - i -1) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }

    public static char[][] task9() {
        System.out.println("===============Task9===============");
        for (int i = 0; i < SQUARE_WIDTH; i++) {
            for (int j = 0; j < SQUARE_WIDTH; j++) {
                if (i < SQUARE_WIDTH / 2) {
                    System.out.print("  ");
                } else if (i == SQUARE_WIDTH - 1 || i == j || j == SQUARE_WIDTH - i - 1) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        return TWO_D_ARRAY;
    }
}
