package com.github.math;

/**
 * Записать логическое выражение, принимающее значение 1, если точка лежит внутри заштрихованной области, иначе — 0.
 */
public class Task3 {

    public static Point[] polygons = new Point[]{
            new Point(0, 0),
            new Point(2, 2),
            new Point(0, -1),
            new Point(-2, -2)
    };

    public static void main(String[] args) {


        System.out.println(inside(new Point(0.0, -0.5), polygons));
        System.out.println(inside(new Point(0.0, -2), polygons));
    }

    public static int inside(Point p, Point[] polygons) {
        int intersections = 0;
        Point prev = polygons[polygons.length - 1];
        for (Point polygon : polygons) {
            if ((prev.y <= p.y && p.y < polygon.y) || (prev.y >= p.y && p.y > polygon.y)) {
                double dy = polygon.y - prev.y;
                double dx = polygon.x - prev.x;
                double x = (p.y - prev.y) / dy * dx + prev.x;
                if (x > p.x) {
                    intersections++;
                }
            }
            prev = polygon;
        }
        return intersections % 2 == 1 ? 1 : 0;
    }

    public static class Point {
        double x;
        double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}
