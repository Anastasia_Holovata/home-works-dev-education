package com.github.math;

/**
 * Стрельба из гаубицы. Дан угол возвышения ствола а и начальная скорость полёта снаряда. v км/ч.
 * Вычислить расстояние полёта снаряда. Реализовать решения для угла в градусах и в радианах.
 */
public class Task1 {

    public static void main(String[] args) {
        System.out.println("Calculated distance by degrees: " +
                calculateDistanceOfProjectiveFlightByDegree(45, 18));
        System.out.println("Calculated distance by degrees: " +
                calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(45), 18));
    }

    public static double calculateDistanceOfProjectiveFlightByDegree(double alphaDegree, int speed) {
        return (Math.pow(speed, 2) * Math.sin(Math.toRadians(2 * alphaDegree))) / 9.8;
    }

    public static double calculateDistanceOfProjectiveFlightByRadian(double alphaRadian, int speed) {
        return (Math.pow(speed, 2) / 9.8) * Math.sin(2 * alphaRadian);
    }
}
