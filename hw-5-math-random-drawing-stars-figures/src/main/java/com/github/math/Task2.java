package com.github.math;

/**
 *Скорость первого автомобиля v1 км/ч, второго — v2 км/ч, расстояние между ними s км.
 * Какое расстояние будет между ними через t ч, если автомобили движутся в разные стороны?
 */
public class Task2 {

    public static void main(String[] args) {
        System.out.println("The distance between 2 cars is " + getTheDistance(10, 15, 3, 30));
        System.out.println("The distance between 2 cars is " + getTheDistance(20, 30, 2, 30));
    }

    public static int getTheDistance(int velocity1, int velocity2, int timeOfRide, int initialDistance) {
        return  timeOfRide * (velocity1 + velocity2) + initialDistance;
    }
}
