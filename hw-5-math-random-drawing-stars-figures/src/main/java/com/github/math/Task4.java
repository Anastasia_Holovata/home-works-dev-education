package com.github.math;

import java.util.Scanner;

/**
 * Вычислить значение выражения.
 */
public class Task4 {

    public static void main(String[] args) {
        System.out.println("Input the x: ");
        int enteredValue = new Scanner(System.in).nextInt();
        System.out.println("The value of the expression with x = " + enteredValue + " is " + getTheValueOFExpression(enteredValue));
    }

    public static double getTheValueOFExpression(int x) {
        double numerator1 = 6 * Math.log(Math.sqrt(Math.exp(x+1) + 2 * Math.exp(x) * Math.cos(x)));
        double denominator1 = Math.log(x - Math.exp(x+1) * Math.sin(x));
        double numerator2 = Math.cos(x);
        double denominator2 = Math.exp(Math.sin(x));

        return  (numerator1 / denominator1) + Math.abs(numerator2 / denominator2);
    }
}
