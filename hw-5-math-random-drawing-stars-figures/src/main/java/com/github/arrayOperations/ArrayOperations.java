package com.github.arrayOperations;

import java.util.Arrays;
import java.util.Random;

public class ArrayOperations {

    private static final Random RANDOM = new Random();
    private static int[] array;

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
    }

    /**
     * Создайте массив из всех чётных чисел от 2 до 100 и выведите элементы массива на экран
     */
    public static void task1() {
        System.out.println("=================Task 1=================");
        array = new int[50];
        for (int i = 0, j = 2; j <= 100; i++, j += 2) {
            if (j % 2 == 0) {
                array[i] = j;
            }
        }
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    /**
     * Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в строку,
     */
    public static void task2() {
        System.out.println("\n\n=================Task 2=================");
        array = new int[50];
        for (int i = 0, j = 1; j <= 99; j += 2, i++) {
            if (j % 2 == 1) {
                array[i] = j;
            }
        }
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    /**
     * Создайте массив из 15 случайных целых чисел из отрезка [0;9]. Выведите массив на экран.
     */
    public static void task3() {
        System.out.println("\n\n=================Task 3=================");
        System.out.print(Arrays.toString(generateFromDefinedLimits(0, 9, 15)));
    }

    /**
     * Создайте массив из 8 случайных целых чисел из отрезка [1;10]. Выведите массив на экран в строку.
     */
    public static void task4() {
        System.out.println("\n\n=================Task 4=================");
        System.out.print(Arrays.toString(generateFromDefinedLimits(1, 10, 8)));
    }

    /**
     * Создайте массив из 4 случайных целых чисел из отрезка [10;99], выведите его на экран
     */
    public static void task5() {
        System.out.println("\n\n=================Task 5=================");
        System.out.print(Arrays.toString(generateFromDefinedLimits(10, 99, 4)));
    }

    /**
     * Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
     */
    public static void task6() {
        System.out.println("\n\n=================Task 6=================");
        array = new int[20];

        int element1 = 0;
        int element2 = 1;
        array[0] = element1;
        array[1] = element2;

        for (int i = 2; i < array.length; i++) {
            array[i] = element1 + element2;
            element1 = element2;
            element2 = array[i];
        }
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    /**
     * Создайте массив из 12 случайных целых чисел из отрезка [-15;15]. Определите какой элемент является в этом массиве максимальным вывести его.
     */
    public static void task7() {
        System.out.println("\n\n=================Task 7=================");
        array = generateFromDefinedLimits(-15, 15, 12);

        int maxValue = array[0];
        for (int value : array) {
            if (maxValue < value) {
                maxValue = value;
            }
        }
        System.out.print(Arrays.toString(array));
        System.out.println("\n" + maxValue);
    }

    private static int[] generateFromDefinedLimits(int min, int max, int size) {
        int[] generatedValues = new int[size];

        for (int i = 0; i < size; i++) {
            generatedValues[i] = RANDOM.nextInt(max - min + 1) + min;
        }

        return generatedValues;
    }
}
