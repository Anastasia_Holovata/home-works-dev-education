package com.github.random;

import java.util.Arrays;
import java.util.Random;

public class RandomClass {

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
    }

    /**
     * Вывести на консоль случайное число.
     */
    private static void task1() {
        System.out.println("===============Task 1===============");
        int number = RANDOM.nextInt();
        System.out.println(number);
    }

    /**
     * Вывести на консоль 10 случайных чисел.
     */
    private static void task2() {
        System.out.println("\n===============Task 2===============");
        for (int i = 0; i < 10; i++) {
            System.out.print(RANDOM.nextInt() + " ");
        }
    }

    /**
     * Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
     */
    private static void task3() {
        System.out.println("\n\n===============Task 3===============");
        System.out.print(Arrays.toString(generateFromDefinedLimits(0, 10, 10)));
    }

    /**
     * Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
     */
    private static void task4() {
        System.out.println("\n\n===============Task 4===============");
        System.out.print(Arrays.toString(generateFromDefinedLimits(20, 50, 10)));
    }

    /**
     * Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
     */
    private static void task5() {
        System.out.println("\n\n===============Task 5===============");
        System.out.print(Arrays.toString(generateFromDefinedLimits(-10, 10, 10)));
    }

    private static int[] generateFromDefinedLimits(int min, int max, int size) {
        int[] generatedValues = new int[size];

        for (int i = 0; i < size; i++) {
            generatedValues[i] = RANDOM.nextInt(max - min + 1) + min;
        }

        return generatedValues;
    }

    /**
     * Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.
     */
    private static void task6() {
        System.out.println("\n\n===============Task 6===============");
        System.out.print(Arrays.toString(
                generateFromDefinedLimits(-10, 35, RANDOM.nextInt(15 - 3 + 1) + 3)));
    }
}
