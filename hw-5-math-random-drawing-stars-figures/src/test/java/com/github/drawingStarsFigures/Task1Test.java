package com.github.drawingStarsFigures;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task1Test {

    @Test
    public void task1() {
        char[][] expectedFigure = {
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'}};
                assertArrayEquals(expectedFigure, Task1.task1());
    }

    @Test
    public void task2() {
        char[][] expectedFigure = {
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},
                {'*', '*', '*', '*', '*', '*', '*'},};
        assertArrayEquals(expectedFigure, Task1.task2());
    }

    @Test
    public void task3() {
    }

    @Test
    public void task4() {
    }

    @Test
    public void task5() {
    }

    @Test
    public void task6() {
    }

    @Test
    public void task7() {
    }

    @Test
    public void task8() {
    }

    @Test
    public void task9() {
    }
}