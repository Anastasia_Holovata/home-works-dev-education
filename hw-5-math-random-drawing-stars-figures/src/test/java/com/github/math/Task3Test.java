package com.github.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task3Test {

    @Test
    public void inside() {
        assertEquals(1, Task3.inside(new Task3.Point(0, 0), Task3.polygons));
        assertEquals(0, Task3.inside(new Task3.Point(-2, -2), Task3.polygons));
        assertEquals(1, Task3.inside(new Task3.Point(1.5, 1.5), Task3.polygons));
        assertEquals(0, Task3.inside(new Task3.Point(-2, -4), Task3.polygons));
        assertEquals(1, Task3.inside(new Task3.Point(-1, -1), Task3.polygons));
    }
}