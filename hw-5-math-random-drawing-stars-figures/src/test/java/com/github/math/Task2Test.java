package com.github.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task2Test {

    @Test
    public void getTheDistance() {
        assertEquals(105, Task2.getTheDistance(10, 15, 3, 30));
        assertEquals(80, Task2.getTheDistance(20, 20, 2, 0));
        assertEquals(30, Task2.getTheDistance(0, 0, 3, 30));
        assertEquals(40, Task2.getTheDistance(10, -5, 2, 30));
        assertEquals(30, Task2.getTheDistance(10, 15, 0, 30));
    }
}