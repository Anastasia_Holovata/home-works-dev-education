package com.github.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task4Test {

    private static final double DELTA = 1e-15;
    private double expected;
    private int intNumber;


    public Task4Test(double expected, int intNumber) {
        this.expected = expected;
        this.intNumber = intNumber;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][]{
                {1.0, 0},
                {Double.NaN, 1},
                {Double.NaN, 9},
                {4.343979255262756, 10},
                {4.453016476824533, 25},
                {Double.NaN, -1}
        });
    }

    @Test
    public void getTheValueOFExpression() {
        assertEquals(expected, Task4.getTheValueOFExpression(intNumber), DELTA);
    }
}