package com.github.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task1Test {

    private static final double DELTA = 1e-15;

    @Test
    public void calculateDistanceOfProjectiveFlightByDegree() {
        assertEquals(33.06122448979592, Task1.calculateDistanceOfProjectiveFlightByDegree(45, 18), DELTA);
        assertEquals(0.9183673469387754, Task1.calculateDistanceOfProjectiveFlightByDegree(45, -3), DELTA);
        assertEquals(4.4987025274800724E-14, Task1.calculateDistanceOfProjectiveFlightByDegree(90, 60), DELTA);
        assertEquals(0.0, Task1.calculateDistanceOfProjectiveFlightByDegree(0, 18), DELTA);
        assertEquals(-20.40816326530612, Task1.calculateDistanceOfProjectiveFlightByDegree(-15, 20), DELTA);
    }

    @Test
    public void calculateDistanceOfProjectiveFlightByRadian() {
        assertEquals(33.06122448979592, Task1.calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(45), 18), DELTA);
        assertEquals(0.9183673469387754, Task1.calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(45), -3), DELTA);
        assertEquals(4.4987025274800724E-14, Task1.calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(90), 60), DELTA);
        assertEquals(0.0, Task1.calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(0), 18), DELTA);
        assertEquals(-20.40816326530612, Task1.calculateDistanceOfProjectiveFlightByRadian(Math.toRadians(-15), 20), DELTA);

    }
}