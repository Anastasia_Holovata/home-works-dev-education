package com.github.servlets;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class Main {

    public static void main(String[] args) throws ServletException, LifecycleException {
        Tomcat tomcat = new Tomcat();
        String webPort = System.getenv("PORT");

        if(webPort == null || webPort.isEmpty()){
            webPort = "8082";
        }

        tomcat.setPort(Integer.parseInt(webPort));
        tomcat.addWebapp("", "C:\\Users\\Пользователь\\DevEducation\\Homeworks_Holovata\\hw-23-servlet-api\\web");

        tomcat.start();
        tomcat.getServer().await();
    }
}
