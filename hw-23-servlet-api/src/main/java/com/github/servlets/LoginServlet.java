package com.github.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.*;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        response.setContentType("text/html");

        if (isAdmin(username, password)) {
            response.setStatus(200);
            HttpSession session = request.getSession();
            session.setAttribute("username", "admin");
            session.setMaxInactiveInterval(30*60);
            Cookie userName = new Cookie("username", username);
            userName.setMaxAge(30*60);
            response.addCookie(userName);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/index");
            dispatcher.forward(request, response);
        } else {
            response.setStatus(403);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Your credentials are wrong!</font>");
            requestDispatcher.include(request, response);
        }
    }

    private boolean isAdmin(String username, String password) {
        return username.equalsIgnoreCase("admin")
               && password.equalsIgnoreCase("password");
    }
}
