<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Index page</title>
</head>
<body>

<%
  String userName = null;
  String sessionID = null;

  Cookie[] cookies = request.getCookies();

  if(cookies !=null){
    for(Cookie cookie : cookies){
      if(cookie.getName().equals("username")) userName = cookie.getValue();
      if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
    }
  }
%>

<h1>Hello, <%=userName %></h1>
<h2>Your login is successful!</h2>
<h3>Your JSESSIONID: <%=sessionID %></h3>

</body>
</html>