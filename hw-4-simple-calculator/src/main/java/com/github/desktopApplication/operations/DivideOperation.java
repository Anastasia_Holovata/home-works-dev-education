package com.github.desktopApplication.operations;

public class DivideOperation implements Operation {

    @Override
    public double calculate(double firstParam, double secondParam) {
        return firstParam / secondParam;
    }
}
