package com.github.desktopApplication.operations;

public interface Operation {

    double calculate(double firstParam, double secondParam);
}
