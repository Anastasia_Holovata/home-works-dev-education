package com.github.desktopApplication;

import javax.swing.*;

public class CalculatorWindow extends JFrame {

    public CalculatorWindow() {
        setTitle("Calculator");
        setVisible(true);
        setBounds(150, 150, 300, 400);
        add( new CalculatorDisplay());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
