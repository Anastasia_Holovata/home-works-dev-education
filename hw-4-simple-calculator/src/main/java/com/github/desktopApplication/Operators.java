package com.github.desktopApplication;

public enum Operators {
    PLUS("+"), MINUS("-"), MULTIPLY("*"), DIVIDE("/");

    private String operation;

    Operators(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public static Operators fromString(String text) {
        for (Operators operator : Operators.values()) {
            if (operator.getOperation().equalsIgnoreCase(text)) {
                return operator;
            }
        }
        return null;
    }
}
