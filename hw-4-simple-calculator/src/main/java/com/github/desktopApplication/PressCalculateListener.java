package com.github.desktopApplication;

import com.github.desktopApplication.operations.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PressCalculateListener implements ActionListener {

    private final TextField firstValueTextField;
    private final TextField secondValueTextField;
    private final TextField operatorTextField;
    private final TextField resultTextField;

    public PressCalculateListener(
            TextField firstValueTextField,
            TextField secondValueTextField,
            TextField operatorTextField,
            TextField resultTextField) {
        this.firstValueTextField = firstValueTextField;
        this.secondValueTextField = secondValueTextField;
        this.operatorTextField = operatorTextField;
        this.resultTextField = resultTextField;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        double firstParamValue = Double.parseDouble(firstValueTextField.getText());
        double secondParamValue = Double.parseDouble(secondValueTextField.getText());
        Operators operator = Operators.fromString(operatorTextField.getText());

        double resultValue = 0;

        switch (operator) {
            case PLUS: resultValue = new PlusOperation().calculate(firstParamValue, secondParamValue); break;
            case MINUS: resultValue = new MinusOperation().calculate(firstParamValue, secondParamValue); break;
            case MULTIPLY: resultValue = new MultiplyOperation().calculate(firstParamValue, secondParamValue); break;
            case DIVIDE: resultValue = new DivideOperation().calculate(firstParamValue, secondParamValue); break;
        }

        resultTextField.setText(String.valueOf(resultValue));
    }
}
