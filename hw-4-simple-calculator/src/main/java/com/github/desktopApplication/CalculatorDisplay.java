package com.github.desktopApplication;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.util.LinkedHashMap;
import java.util.Map;

@SuppressWarnings("ALL")
public class CalculatorDisplay extends JPanel {

    private final static Color FRAME_COLOR = new Color(221, 226, 205);

    public CalculatorDisplay() {
        initComponents();
    }

    private void initComponents() {
        Map<String, Component> allComponents = new LinkedHashMap<>();

        setPanelParams();

        createTextFields(allComponents);
        createLabels(allComponents);
        createButtons(allComponents);

        addComponentsToPanel(allComponents);
    }

    private void setPanelParams() {
        setBackground(FRAME_COLOR);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(null);
    }

    private void addComponentsToPanel(Map<String, Component> allComponents) {
        for (Component component : allComponents.values()) {
            add(component);
        }
    }

    private void createTextFields(Map<String, Component> allComponents) {
        TextField firstValueTextField = new TextField();
        firstValueTextField.setBounds(90, 30, 170, 45);
        allComponents.put("TEXT_FIELD_FIRST_PARAM", firstValueTextField);

        TextField secondValueTextField = new TextField();
        secondValueTextField.setBounds(90, 95, 170, 45);
        allComponents.put("TEXT_FIELD_SECOND_PARAM", secondValueTextField);

        TextField operatorTextField = new TextField();
        operatorTextField.setBounds(90, 160, 170, 45);
        allComponents.put("TEXT_FIELD_OPERATOR_PARAM", operatorTextField);

        TextField resultValueTextField = new TextField();
        resultValueTextField.setBounds(90, 300, 170, 45);
        allComponents.put("TEXT_FIELD_RESULT", resultValueTextField);
    }

    private void createLabels(Map<String, Component> allComponents) {
        JLabel firstLineLabel = new JLabel("Число 1");
        firstLineLabel.setBounds(30, 40, 195, 30);
        allComponents.put("LABEL_FIRST_PARAM", firstLineLabel);

        JLabel secondLineLabel = new JLabel("Число 2");
        secondLineLabel.setBounds(30, 105, 195, 30);
        allComponents.put("LABEL_SECOND_PARAM", secondLineLabel);

        JLabel operationLabel = new JLabel("Операция");
        operationLabel.setBounds(20, 170, 195, 30);
        allComponents.put("LABEL_OPERATOR_PARAM", operationLabel);

        JLabel resultLabel = new JLabel("Результат");
        resultLabel.setBounds(20, 310, 195, 30);
        allComponents.put("LABEL_RESULT", resultLabel);
    }

    private void createButtons(Map<String, Component> allComponents) {
        JButton calculateButton = new JButton("Посчитать");
        calculateButton.setBounds(28, 225, 230, 60);
        calculateButton.setBackground(FRAME_COLOR);
        calculateButton.setBorder(new BevelBorder(BevelBorder.RAISED));

        PressCalculateListener pressCalculateListener = new PressCalculateListener(
                (TextField) allComponents.get("TEXT_FIELD_FIRST_PARAM"),
                (TextField) allComponents.get("TEXT_FIELD_SECOND_PARAM"),
                (TextField) allComponents.get("TEXT_FIELD_OPERATOR_PARAM"),
                (TextField) allComponents.get("TEXT_FIELD_RESULT")
        );

        calculateButton.addActionListener(pressCalculateListener);
        allComponents.put("BUTTON_CALCULATE", calculateButton);
    }
}
