package com.github.balls;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.atomic.AtomicInteger;

public class BallsPanel extends JPanel {

    private final AtomicInteger counter = new AtomicInteger(0);

    public BallsPanel() {
        setLayout(null);
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Ball ball = new Ball(e.getPoint());
                add(ball);
                Thread thread = new Thread(ball, "ball-" + counter.incrementAndGet());
                thread.start();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        setVisible(Boolean.TRUE);
    }
}
