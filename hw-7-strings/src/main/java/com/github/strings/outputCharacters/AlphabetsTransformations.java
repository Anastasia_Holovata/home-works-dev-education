package com.github.strings.outputCharacters;

import java.util.ArrayList;
import java.util.List;

public class AlphabetsTransformations {

    public static void main(String[] args) {
        System.out.println(englishAlphabetAToZ());
        System.out.println(englishAlphabetZToA());
        System.out.println(русскийАлфавитАДоЯ());
        System.out.println(digits0To9());
        System.out.println(symbolsASCII());
    }

    private static List<Character> englishAlphabetAToZ() {
        List<Character> alphabetSymbols = new ArrayList<>();
        for (char i = 'A'; i <= 'Z'; i++) {
            alphabetSymbols.add(i);
        }
        return alphabetSymbols;
    }

    private static List<Character> englishAlphabetZToA() {
        List<Character> alphabetSymbols = new ArrayList<>();
        for (char i = 'z'; i >= 'a'; i--) {
            alphabetSymbols.add(i);
        }
        return alphabetSymbols;
    }

    private static List<Character> русскийАлфавитАДоЯ() {
        List<Character> alphabetSymbols = new ArrayList<>();
        for (char i = 'а'; i <= 'я'; i++) {
            alphabetSymbols.add(i);
        }
        return alphabetSymbols;
    }

    private static List<Character> digits0To9() {
        List<Character> digitSymbols = new ArrayList<>();
        for (char i = '0'; i <= '9'; i++) {
            digitSymbols.add(i);
        }
        return digitSymbols;
    }

    private static List<Character> symbolsASCII() {
        List<Character> symbols = new ArrayList<>();
        for (char i = 32; i <= 126; i++) {
            symbols.add(i);
        }
        return symbols;
    }
}
