package com.github.strings.conversionFunctions;

public class Conversions {

    public static void main(String[] args) {
        System.out.println(intNumberToString(5));
        System.out.println(intNumberToString(25));
        System.out.println(doubleNumberToString(3.175));
        System.out.println(doubleNumberToString(3.d));
        System.out.println(stringToIntNumber("1234"));
        System.out.println(stringToIntNumber("a"));
        System.out.println(stringToDoubleNumber("1234.0"));
    }

    protected static String intNumberToString(int number) {
        return Integer.toString(number);
    }

    protected static String doubleNumberToString(double number) {
        return Double.toString(number);
    }

    protected static int stringToIntNumber(String line) {
        return Integer.parseInt(line);
    }

    protected static double stringToDoubleNumber(String line) {
        return Double.parseDouble(line);
    }
}
