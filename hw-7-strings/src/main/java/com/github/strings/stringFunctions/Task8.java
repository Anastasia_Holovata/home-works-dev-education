package com.github.strings.stringFunctions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * В заданной строке удалить последнее слово
 */
public class Task8 {

    public static void main(String[] args) throws IOException {
        System.out.println("Input the line:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("The modified line is " + deleteTheLastWordInLine(reader.readLine()));
    }

    protected static String deleteTheLastWordInLine(String line) {
        int index = line.lastIndexOf(" ");
        return line.substring(0, index);
    }
}
