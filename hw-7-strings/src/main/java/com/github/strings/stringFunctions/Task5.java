package com.github.strings.stringFunctions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Подсчитать количество слов во введенной пользователем строке.
 */
public class Task5 {

    public static void main(String[] args) throws IOException {
        System.out.println("Input the line: ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("The amount of words in line equals " + calculateTheAmountOfWords(reader.readLine()));
    }

    protected static int calculateTheAmountOfWords(String line) {
        if (line == null || line.isEmpty()) {
            return 0;
        }
        String[] words = line.split("\\s+");
        return words.length;
    }
}
