package com.github.strings.stringFunctions;

import java.util.Arrays;

/**
 * Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"
 */
public class Task2 {

    private static final String[] WORDS = {"Apple", "Banana", "Cat", "Door", "Elephant", "Fish", "Giraffe", "Hat"};

    public static void main(String[] args) {
        System.out.println(Arrays.toString(replaceLastThreeSymbolsInDefinedLengthString(WORDS, 3)));
    }

    protected static String[] replaceLastThreeSymbolsInDefinedLengthString(String[] arrayStrings, int definedLength) {
        for (int i = 0; i < arrayStrings.length; i++) {
            if (arrayStrings[i].length() == definedLength) {
                arrayStrings[i] = replaceLast(arrayStrings[i]);
            }
        }

        return arrayStrings;
    }

    private static String replaceLast(String text) {
        return text.replaceFirst(".{3}$", "\\$");
    }
}
