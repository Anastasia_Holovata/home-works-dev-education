package com.github.strings.stringFunctions;

/**
 * Удалить из строки ее часть с заданной позиции и заданной длины.
 */
public class Task6 {

    private static final String LINE = "Is this a real life? Is this just fantasy?";

    public static void main(String[] args) {
        System.out.println(removeThePartFromLine(LINE, 4, 9));
    }

    protected static String removeThePartFromLine(String line, int startPosition, int lengthOfRemoving) {
        return line.substring(startPosition, lengthOfRemoving + startPosition);
    }
}
