package com.github.strings.stringFunctions;

/**
 * Дана строка, состоящая из слов, разделенных пробелами и знаками препинания. Определить длину самого короткого слова.
 */
public class Task1 {

    public static void main(String[] args) {
        System.out.println(getLengthOfTheShortestWord("Я люблю свою страну, люблю свою жену, люблю свою собаку!"));
    }

    protected static int getLengthOfTheShortestWord(String stringLine) {
        String[] words = getArrayOfWordsFromString(stringLine);
        String shortestWord = words[0];

        for (String word : words) {
            if (shortestWord.length() > word.length()) {
                shortestWord = word;
            }
        }

        return shortestWord.length();
    }

    private static String[] getArrayOfWordsFromString(String stringLine) {
        String stringLineAfterReplacement = stringLine.replaceAll("[.:,]", " ");
        return stringLineAfterReplacement.split("\\s\\s|\\s");
    }
}
