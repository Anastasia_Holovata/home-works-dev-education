package com.github.strings.stringFunctions;

/**
 * Добавить в строку пробелы после знаков препинания, если они там отсутствуют.
 */
public class Task3 {

    private static final String LINE = "Маша,любит кашу.Кот любит рыбу.  ";

    public static void main(String[] args) {
        System.out.println(addSpacesWhereTheyAreAbsent(LINE));
    }

    protected static String addSpacesWhereTheyAreAbsent(String stringLine) {
        return stringLine.replaceAll("[.:,!?]", "$0 ").replaceAll("\\s+", " ");
    }
}
