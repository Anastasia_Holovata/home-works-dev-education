package com.github.strings.stringFunctions;

/**
 * Оставить в строке только один экземпляр каждого встречающегося символа.
 */
public class Task4 {

    private static final String LINE = "AA...BCdDeFFFfghiJ))";

    public static void main(String[] args) {
        System.out.println(getLineWithNotRepeatedSymbols(LINE));
    }

    protected static String getLineWithNotRepeatedSymbols(String lineWithRepeatedSymbols) {
        StringBuilder result = new StringBuilder();
        String modifiedLine = lineWithRepeatedSymbols.toUpperCase();

        for (int i = 0; i < modifiedLine.length(); i++) {
            if (!result.toString().contains(String.valueOf(modifiedLine.charAt(i)))) {
                result.append(modifiedLine.charAt(i));
            }
        }

        return result.toString();
    }
}
