package com.github.strings.stringFunctions;

/**
 * Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
 */
public class Task7 {

    private static String LINE = "Is this a real life? Is this just fantasy?";

    public static void main(String[] args) {
        System.out.println(inverseString(LINE));
    }

    protected static String inverseString(String string) {
        char[] chars = string.toCharArray();

        for (int i = 0; i < chars.length / 2; i++) {
            char tmp = chars[i];
            chars[i] = chars[chars.length - i - 1];
            chars[chars.length - i - 1] = tmp;
        }

        return new String(chars);
    }
}
