package com.github.strings.conversionFunctions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConversionsTest {

    private static final double DELTA = 1e-15;

    @Test
    public void intNumberToString() {
        assertEquals("5", Conversions.intNumberToString(5));
        assertEquals("1", Conversions.intNumberToString(00001));
        assertEquals("0", Conversions.intNumberToString((int) Double.NaN));
        assertEquals("15", Conversions.intNumberToString(Math.abs(-15)));
        assertEquals("3", Conversions.intNumberToString((int) Math.sqrt((15))));
    }

    @Test
    public void doubleNumberToString() {
        assertEquals("1234.0", Conversions.doubleNumberToString(1234.d));
        assertEquals("0.0", Conversions.doubleNumberToString(3 / 5));
        assertEquals("0.6", Conversions.doubleNumberToString((double) 3 / 5));
        assertEquals("NaN", Conversions.doubleNumberToString(Double.NaN));
        assertEquals("3.141592653589793", Conversions.doubleNumberToString(Math.PI));
    }

    @Test
    public void stringToIntNumber() {
        assertEquals(12, Conversions.stringToIntNumber("12"));
        assertEquals(1234, Conversions.stringToIntNumber("1234"));
        assertEquals(0, Conversions.stringToIntNumber("0"));
        assertEquals(0, Conversions.stringToIntNumber("-0"));
        assertEquals(-123, Conversions.stringToIntNumber("-123"));
    }

    @Test
    public void stringToDoubleNumber() {
        assertEquals(1234.d, Conversions.stringToDoubleNumber("1234.d"), DELTA);
        assertEquals(Math.sin(12), Conversions.stringToDoubleNumber("-0.5365729180004349"), DELTA);
        assertEquals(0.000000d, Conversions.stringToDoubleNumber("0.0"), DELTA);
        assertEquals(-365.0, Conversions.stringToDoubleNumber("-365.0"), DELTA);
        assertEquals(-365.0000000000009, Conversions.stringToDoubleNumber("-365.0000000000009"), DELTA);
    }
}