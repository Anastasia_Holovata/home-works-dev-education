package com.github.strings.stringFunctions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task1Test {

    private int expectedIntMinimumLength;
    private String actualString;

    public Task1Test(int expectedIntMinimumLength, String actualString) {
        this.expectedIntMinimumLength = expectedIntMinimumLength;
        this.actualString = actualString;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][]{
                {1, "Я люблю свою страну, люблю свою жену, люблю свою собаку!"},
                {1, "Мы едем, едем, едем, В далекие края. Счастливые соседи, Счастливые: друзья"},
                {4, "null null null"}
        });
    }

    @Test
    public void getLengthOfTheShortestWord() {
        assertEquals(expectedIntMinimumLength, Task1.getLengthOfTheShortestWord(actualString));
    }
}