package com.github.strings.stringFunctions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task4Test {

    private String expectedModifiedString;
    private String actualString;

    public Task4Test(String expectedModifiedString, String actualString) {
        this.expectedModifiedString = expectedModifiedString;
        this.actualString = actualString;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][]{
                {"A.BCDEFGHIJ)", "AA...BCdDeFFFfghiJ))"},
                {"ABCDEFGHIJKLM", "AaBBbbbCDdEFgGgHIiiiJjKkllLMm"},
                {"МАШ, ЛЮБИТКУ.ОРЫ", "Маша, любит кашу.Кот любит рыбу."}
        });
    }

    @Test
    public void getLineWithNotRepeatedSymbols() {
        assertEquals(expectedModifiedString, Task4.getLineWithNotRepeatedSymbols(actualString));
    }
}