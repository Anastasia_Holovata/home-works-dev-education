package com.github.strings.stringFunctions;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Task7Test {

    private String expectedLine;
    private String actualLine;

    public Task7Test(String expectedLine, String actualLine) {
        this.expectedLine = expectedLine;
        this.actualLine = actualLine;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][]{
                {"!dlroW ,olleH", "Hello, World!"},
                {"Hello, World, my friend!", "!dneirf ym ,dlroW ,olleH"},
                {" ", " "},
                {"abba", "abba"},
                {"!dlrow siht evol I", "I love this world!"}
        });
    }

    @Test
    public void inverseString() {
        Assert.assertEquals(expectedLine, Task7.inverseString(actualLine));
    }
}