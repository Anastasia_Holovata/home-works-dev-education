package com.github.strings.stringFunctions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Task8Test {

    private String expectedLineWithoutLastWord;
    private String actualString;

    public Task8Test(String expectedLineWithoutLastWord, String actualString) {
        this.expectedLineWithoutLastWord = expectedLineWithoutLastWord;
        this.actualString = actualString;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][]{
                {"Hello,", "Hello, World!"},
                {"Hello, World, my", "Hello, World, my friend!"},
                {"", " "},
                {"I love this", "I love this world!"}
        });
    }

    @Test
    public void deleteTheLastWordInLine() {
        assertEquals(expectedLineWithoutLastWord, Task8.deleteTheLastWordInLine(actualString));
    }
}