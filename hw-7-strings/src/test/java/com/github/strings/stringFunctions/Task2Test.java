package com.github.strings.stringFunctions;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class Task2Test {

    @Test
    public void replaceLastThreeSymbolsInDefinedLengthString() {
        assertArrayEquals(new String[]{"Apple", "Banana", "Cat", "D$", "Elephant", "F$", "Giraffe", "Hat"},
                Task2.replaceLastThreeSymbolsInDefinedLengthString(new String[]
                        {"Apple", "Banana", "Cat", "Door", "Elephant", "Fish", "Giraffe", "Hat"}, 4));
        assertArrayEquals(new String[]{"Apple", "Banana", "$", "Door", "Elephant", "Fish", "Giraffe", "$"},
                Task2.replaceLastThreeSymbolsInDefinedLengthString(new String[]
                        {"Apple", "Banana", "Cat", "Door", "Elephant", "Fish", "Giraffe", "Hat"}, 3));
        assertArrayEquals(new String[]{"Apple", "Ban$", "Cat", "Door", "Elephant", "Fish", "Giraffe", "Hat"},
                Task2.replaceLastThreeSymbolsInDefinedLengthString(new String[]
                        {"Apple", "Banana", "Cat", "Door", "Elephant", "Fish", "Giraffe", "Hat"}, 6));
    }
}