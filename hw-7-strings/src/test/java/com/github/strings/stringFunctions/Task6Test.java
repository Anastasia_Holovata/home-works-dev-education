package com.github.strings.stringFunctions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Task6Test {

    @Test
    public void removeThePartFromLine() {
        assertEquals("a real life?",
                Task6.removeThePartFromLine("Is this a real life? Is this just fantasy?",
                        8, 12));
        assertEquals("to America?",
                Task6.removeThePartFromLine("Have you ever been to America?",
                        19, 11));
        assertEquals("I keep my hands",
                Task6.removeThePartFromLine("I keep my hands to myself",
                        0, 15));
    }
}