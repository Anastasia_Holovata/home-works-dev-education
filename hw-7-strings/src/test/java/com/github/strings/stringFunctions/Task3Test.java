package com.github.strings.stringFunctions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class Task3Test {

    private String expectedModifiedString;
    private String actualString;

    public Task3Test(String expectedModifiedString, String actualString) {
        this.expectedModifiedString = expectedModifiedString;
        this.actualString = actualString;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][]{
                {"Я люблю, свою страну, люблю свою жену , люблю свою собаку! ",
                        "Я люблю,свою страну,люблю свою жену , люблю свою собаку!   "},
                {"Мы едем, едем, едем, В далекие края. Счастливые соседи, Счастливые: друзья",
                        "Мы едем,едем,  едем,В далекие края.Счастливые соседи, Счастливые:друзья"},
                {"Маша, любит кашу. Кот любит рыбу. ", "Маша,любит кашу.Кот любит рыбу.  "}
        });
    }

    @Test
    public void addSpacesWhereTheyAreAbsent() {
        assertEquals(expectedModifiedString, Task3.addSpacesWhereTheyAreAbsent(actualString));
    }
}