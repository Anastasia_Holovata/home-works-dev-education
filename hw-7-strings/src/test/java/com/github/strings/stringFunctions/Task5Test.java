package com.github.strings.stringFunctions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class Task5Test {

    private int expectedIntNumberOfWordsInLine;
    private String actualString;

    public Task5Test(int expectedIntNumberOfWordsInLine, String actualString) {
        this.expectedIntNumberOfWordsInLine = expectedIntNumberOfWordsInLine;
        this.actualString = actualString;
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new Object[][]{
                {2, "Hello, World!"},
                {4, "Hello, World, my friend!"},
                {0, " "}
        });
    }

    @Test
    public void calculateTheAmountOfWords() {
        assertEquals(expectedIntNumberOfWordsInLine, Task5.calculateTheAmountOfWords(actualString));
    }
}