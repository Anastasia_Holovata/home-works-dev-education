package com.github.collections.tree;

public class BSTreeTest extends AbstractTreeTest {

    @Override
    protected ITree getTreeInstance() {
        return new BSTree();
    }
}