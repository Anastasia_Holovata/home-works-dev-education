package com.github.collections.tree;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public abstract class AbstractTreeTest {

    protected ITree tree;

    @Before
    public void setup() {
        tree = getTreeInstance();
    }

    protected abstract ITree getTreeInstance();

    @Test
    public void initMany() {
        int[] array = {1, 2, 3, -5, -3, 0};
        tree.init(array);
        assertArrayEquals(new int[]{-5, -3, 0, 1, 2, 3}, tree.toArray());
    }

    @Test
    public void initTwo() {
        int[] array = {1, -3};
        tree.init(array);
        assertArrayEquals(new int[]{-3, 1}, tree.toArray());
    }

    @Test
    public void initOne() {
        int[] array = {2};
        tree.init(array);
        assertArrayEquals(new int[]{2}, tree.toArray());
    }

    @Test
    public void initEmptyTree() {
        int[] array = {};
        tree.init(array);
        assertArrayEquals(new int[]{}, tree.toArray());
    }

    @Test
    public void initZeroTree() {
        int[] array = {0};
        tree.init(array);
        assertArrayEquals(new int[]{0}, tree.toArray());
    }

    @Test
    public void clearMany() {
        initMany();
        tree.clear();
        assertArrayEquals(new int[0], new int[0]);
    }

    @Test
    public void clearTwo() {
        initTwo();
        tree.clear();
        assertArrayEquals(new int[0], new int[0]);
    }

    @Test
    public void clearOne() {
        initOne();
        tree.clear();
        assertArrayEquals(new int[0], new int[0]);
    }

    @Test
    public void clearEmpty() {
        initEmptyTree();
        tree.clear();
        assertArrayEquals(new int[0], new int[0]);
    }

    @Test
    public void clearZero() {
        initZeroTree();
        tree.clear();
        assertArrayEquals(new int[0], new int[0]);
    }

    @Test
    public void addMany() {
        initMany();
        tree.add(8);
        assertArrayEquals(new int[]{-5, -3, 0, 1, 2, 3, 8}, tree.toArray());
    }


    @Test
    public void addTwo() {
        initTwo();
        tree.add(8);
        assertArrayEquals(new int[]{-3, 1, 8}, tree.toArray());
    }

    @Test
    public void addOne() {
        initOne();
        tree.add(8);
        assertArrayEquals(new int[]{2, 8}, tree.toArray());
    }

    @Test
    public void addEmpty() {
        initEmptyTree();
        tree.add(8);
        assertArrayEquals(new int[]{8}, tree.toArray());
    }

    @Test
    public void addZero() {
        initZeroTree();
        tree.add(5);
        assertArrayEquals(new int[]{0, 5}, tree.toArray());
    }

    @Test
    public void addStartNodeToMany() {
        initMany();
        tree.add(-7);
        assertArrayEquals(new int[]{-7, -5, -3, 0, 1, 2, 3}, tree.toArray());
    }

    @Test
    public void addNodeToMany() {
        initMany();
        tree.add(-2);
        assertArrayEquals(new int[]{-5, -3, -2, 0, 1, 2, 3}, tree.toArray());
    }

    @Test
    public void addStartNodeToTwo() {
        initTwo();
        tree.add(-7);
        assertArrayEquals(new int[]{-7, -3, 1}, tree.toArray());
    }

    @Test
    public void addNodeToTwo() {
        initTwo();
        tree.add(0);
        assertArrayEquals(new int[]{-3, 0, 1}, tree.toArray());
    }

    @Test
    public void addNodeToOne() {
        initOne();
        tree.add(0);
        assertArrayEquals(new int[]{0, 2}, tree.toArray());
    }

    @Test
    public void addNodeToZero() {
        initZeroTree();
        tree.add(-2);
        assertArrayEquals(new int[]{-2, 0}, tree.toArray());
    }

    @Test(expected = RuntimeException.class)
    public void addDuplicateNode() {
        tree.add(-2);
        tree.add(-5);
        tree.add(3);
        tree.add(-5);
        tree.add(1);
    }

    @Test
    public void sizeMany() {
        initMany();
        assertEquals(6, tree.size());
    }

    @Test
    public void sizeTwo() {
        initTwo();
        assertEquals(2, tree.size());
    }

    @Test
    public void sizeOne() {
        initOne();
        assertEquals(1, tree.size());
    }

    @Test
    public void sizeEmpty() {
        initEmptyTree();
        assertEquals(0, tree.size());
    }

    @Test
    public void sizeZero() {
        initOne();
        assertEquals(1, tree.size());
    }

    @Test
    public void leavesMany() {
        initMany();
        assertEquals(2, tree.leaves());
    }

    @Test
    public void leavesTwo() {
        initTwo();
        assertEquals(1, tree.leaves());
    }

    @Test
    public void leavesOne() {
        initOne();
        assertEquals(1, tree.leaves());
    }

    @Test
    public void leavesEmpty() {
        initEmptyTree();
        assertEquals(0, tree.leaves());
    }

    @Test
    public void leavesZero() {
        initZeroTree();
        assertEquals(1, tree.leaves());
    }

    @Test
    public void nodesMany() {
        initMany();
        assertEquals(6, tree.nodes());
    }

    @Test
    public void nodesTwo() {
        initTwo();
        assertEquals(2, tree.nodes());
    }

    @Test
    public void nodesOne() {
        initOne();
        assertEquals(1, tree.nodes());
    }


    @Test
    public void nodesEmpty() {
        initEmptyTree();
        assertEquals(0, tree.nodes());
    }

    @Test
    public void nodesZero() {
        initZeroTree();
        assertEquals(1, tree.nodes());
    }

    @Test
    public void heightMany() {
        initMany();
        assertEquals(4, tree.getHeight());
    }

    @Test
    public void heightTwo() {
        initTwo();
        assertEquals(2, tree.getHeight());
    }

    @Test
    public void heightOne() {
        initOne();
        assertEquals(1, tree.getHeight());
    }

    @Test
    public void heightEmpty() {
        initEmptyTree();
        assertEquals(0, tree.getHeight());
    }

    @Test
    public void heightZero() {
        initZeroTree();
        assertEquals(1, tree.getHeight());
    }

    @Test
    public void widthMany() {
        initMany();
        assertEquals(2, tree.getWidth());
    }

    @Test
    public void widthTwo() {
        initTwo();
        assertEquals(1, tree.getWidth());
    }

    @Test
    public void widthOne() {
        initOne();
        assertEquals(1, tree.getWidth());
    }

    @Test
    public void widthEmpty() {
        initEmptyTree();
        assertEquals(0, tree.getWidth());
    }

    @Test
    public void widthZero() {
        initZeroTree();
        assertEquals(1, tree.getWidth());
    }

    @Test
    public void reverseMany() {
        initMany();
        tree.reverse();
        assertArrayEquals(new int[]{3, 2, 1, 0, -3, -5}, tree.toArray());
    }

    @Test
    public void reverseTwo() {
        initTwo();
        tree.reverse();
        assertArrayEquals(new int[]{1, -3}, tree.toArray());
    }

    @Test
    public void reverseOne() {
        initOne();
        tree.reverse();
        assertArrayEquals(new int[]{2}, tree.toArray());
    }

    @Test
    public void reverseEmpty() {
        initEmptyTree();
        tree.reverse();
        assertArrayEquals(new int[]{}, tree.toArray());
    }

    @Test
    public void reverseZero() {
        initZeroTree();
        tree.reverse();
        assertArrayEquals(new int[]{0}, tree.toArray());
    }

    @Test
    public void deleteMany() {
        initMany();
        tree.delete(3);
        assertArrayEquals(new int[]{-5, -3, 0, 1, 2}, tree.toArray());
    }

    @Test
    public void deleteTwo() {
        initTwo();
        tree.delete(1);
        assertArrayEquals(new int[]{-3}, tree.toArray());
    }

    @Test
    public void deleteOne() {
        initOne();
        tree.delete(2);
        assertArrayEquals(new int[]{}, tree.toArray());
    }

    @Test
    public void deleteEmpty() {
        initEmptyTree();
        tree.delete(0);
        assertArrayEquals(new int[0], tree.toArray());
    }

    @Test
    public void deleteZero() {
        initZeroTree();
        tree.delete(0);
        assertArrayEquals(new int[]{}, tree.toArray());
    }

    @Test
    public void deleteRootFromMany() {
        initMany();
        tree.delete(-5);
        assertArrayEquals(new int[]{-3, 0, 1, 2, 3}, tree.toArray());
    }

    @Test
    public void deleteRootFromTwo() {
        initTwo();
        tree.delete(-3);
        assertArrayEquals(new int[]{1}, tree.toArray());
    }

    @Test
    public void deleteNodeFromMany() {
        initMany();
        tree.delete(0);
        assertArrayEquals(new int[]{-5, -3, 1, 2, 3}, tree.toArray());
    }

    @Test
    public void deleteEmptyNotExisted() {
        initTwo();
        tree.delete(10);
        assertArrayEquals(new int[]{-3, 1}, tree.toArray());
    }
}
