package com.github.collections.tree;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AVLTreeTest extends AbstractTreeTest {

    @Override
    protected ITree getTreeInstance() {
        return new AVLTree();
    }

    @Test
    public void widthMany() {
        initMany();
        assertEquals(3, tree.getWidth());
    }

    @Test
    public void leavesMany() {
        initMany();
        assertEquals(3, tree.leaves());
    }

    @Test
    public void heightMany() {
        initMany();
        assertEquals(3, tree.getHeight());
    }
}