package com.github.collections.tree;

public interface ITree {

    void init(int[] init);

    void clear();

    void add(int val);

    int size();

    int leaves();

    int nodes();

    int getHeight();

    int getWidth();

    void reverse();

    void delete(int val);

    String toString();

    int[] toArray();
}
