package com.github.collections.tree;

import java.util.Objects;

public class BSTree implements ITree {

    private Node root;

    public static int countOfNodes(Node node) {
        if (null == node) {
            return 0;
        }
        return 1 + countOfNodes(node.left) + countOfNodes(node.right);
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
            return;
        }
        addNode(val, this.root);
    }

    private void addNode(int val, Node p) {
        if (val < p.value) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(val, p.left);
            }
        } else if (val > p.value) {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(val, p.right);
            }
        } else {
            throw new RuntimeException("duplicate Key!");
        }
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.right);
        count++;
        count += sizeNode(p.left);
        return count;
    }

    @Override
    public int leaves() {
        return getLeafCount(this.root);
    }

    private int getLeafCount(Node node) {
        if (node == null)
            return 0;
        if (node.left == null && node.right == null)
            return 1;
        else
            return getLeafCount(node.left) + getLeafCount(node.right);
    }

    @Override
    public int nodes() {
        return countOfNodes(this.root);
    }

    @Override
    public int getHeight() {
        return height(this.root);
    }

    private int height(Node node) {
        if (node == null)
            return 0;
        else {
            int lHeight = height(node.left);
            int rHeight = height(node.right);

            return (lHeight > rHeight) ? (lHeight + 1)
                    : (rHeight + 1);
        }
    }

    @Override
    public int getWidth() {
        return getMaxWidth(this.root);
    }

    private int getWidth(Node node, int level) {
        if (node == null) {
            return 0;
        }
        if (level == 1) {
            return 1;
        } else if (level > 1) {
            return getWidth(node.left, level - 1)
                    + getWidth(node.right, level - 1);
        }
        return 0;
    }

    private int getMaxWidth(Node node) {
        int maxWidth = 0;
        int width;
        int h = height(node);
        int i;

        for (i = 1; i <= h; i++) {
            width = getWidth(node, i);
            if (width > maxWidth) {
                maxWidth = width;
            }
        }
        return maxWidth;
    }

    @Override
    public void reverse() {
        root = reverse(root);
    }

    private Node reverse(Node node) {
        if (node == null) {
            return node;
        }
        Node left = reverse(node.left);
        Node right = reverse(node.right);

        node.left = right;
        node.right = left;

        return node;
    }

    @Override
    public void delete(int val) {
        root = deleteRecursive(root, val);
    }

    private Node deleteRecursive(Node root, int key) {
        //tree is empty
        if (root == null) {
            return root;
        }

        if (key < root.value) {
            root.left = deleteRecursive(root.left, key);
        } else if (key > root.value) {
            root.right = deleteRecursive(root.right, key);
        } else {
            if (root.left == null) {
                return root.right;
            } else if (root.right == null) {
                return root.left;
            }

            root.value = minValue(root.right);

            root.right = deleteRecursive(root.right, root.value);
        }
        return root;
    }

    private int minValue(Node root) {
        int minval = root.value;
        while (root.left != null) {
            minval = root.left.value;
            root = root.left;
        }
        return minval;
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    private static class Node {
        int value;
        Node right;
        Node left;

        public Node(int value) {
            this.value = value;
        }
    }

    private static class Counter {
        int index = 0;
    }
}
