package com.github.balls2;

import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class BallsDispatcher {

    private static final List<Ball> balls = new CopyOnWriteArrayList<>();

    public static void addBall(Ball ball) {
        balls.add(ball);
    }

    public static void removeBall(Ball ball) {
        balls.remove(ball);
    }

    public static boolean isBallLapping(Ball firstBall) {
        Edges firstBallEdges = getBallEdges(firstBall);

        Optional<Ball> secondBall = balls.stream()
                .filter(ball -> !ball.equals(firstBall))
                .filter(ball -> isOverlapping(firstBallEdges, getBallEdges(ball)))
                .findFirst();

        if (secondBall.isPresent()) {
            BallsPanel ballsPanel = (BallsPanel) firstBall.getParent();
            ballsPanel.changeBall(firstBall, secondBall.get());
            return true;
        } else {
            return false;
        }
    }

    public static synchronized Optional<Ball> isBallLapping2(Ball firstBall) {
        Edges firstBallEdges = getBallEdges(firstBall);

        Optional<Ball> secondBall = balls.stream()
                .filter(ball -> !ball.equals(firstBall))
                .filter(ball -> isOverlapping(firstBallEdges, getBallEdges(ball)))
                .findFirst();

        if (secondBall.isPresent()) {
            balls.remove(firstBall);
            return secondBall;
        } else {
            return Optional.empty();
        }
    }

    private static boolean isOverlapping(Edges e1, Edges e2) {
        if (e1.topRight.getY() < e2.bottomLeft.getY()
                || e1.bottomLeft.getY() > e2.topRight.getY()) {
            return false;
        }
        if (e1.topRight.getX() < e2.bottomLeft.getX()
                || e1.bottomLeft.getX() > e2.topRight.getX()) {
            return false;
        }
        return true;
    }

    private static Edges getBallEdges(Ball ball) {
        Point point = ball.getPoint();
        int shift = ball.getBallSize() / 2;

        Point topLeft = new Point((int) point.getX() - shift, (int) point.getY() + shift);
        Point topRight = new Point((int) point.getX() + shift, (int) point.getY() + shift);
        Point bottomRight = new Point((int) point.getX() + shift, (int) point.getY() - shift);
        Point bottomLeft = new Point((int) point.getX() - shift, (int) point.getY() - shift);

        return new Edges(topLeft, topRight, bottomRight, bottomLeft);
    }

    private static class Edges {
        Point topLeft;
        Point topRight;
        Point bottomRight;
        Point bottomLeft;

        public Edges(Point topLeft, Point topRight, Point bottomRight, Point bottomLeft) {
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomRight = bottomRight;
            this.bottomLeft = bottomLeft;
        }
    }


}
