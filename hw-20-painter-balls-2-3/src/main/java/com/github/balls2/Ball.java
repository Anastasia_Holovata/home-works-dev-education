package com.github.balls2;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import static com.github.balls2.BallsDispatcher.*;

public class Ball extends JPanel implements Runnable {

    private final Random random = new Random();
    private int ballSize;
    private int dx;
    private int dy;
    private final float r = random.nextFloat();
    private final float g = random.nextFloat();
    private final float b = random.nextFloat();
    private final Color randomColor;
    private final Point point;

    public Ball(Point point) {
        this.point = point;
        this.ballSize = random.nextInt(Math.min(600, 800) / 8);
        this.randomColor = new Color(r, g, b);
        this.dx = this.random.nextInt(10) - 3;
        this.dy = this.random.nextInt(10) - 3;
        setSize(ballSize + 10, ballSize + 10);
        setOpaque(Boolean.FALSE);
    }

    public int getBallSize() {
        return ballSize;
    }

    public Point getPoint() {
        return point;
    }

    public void setBallSize(int ballSize) {
        this.ballSize = ballSize;
    }

    private void move() {
        JPanel panel = (JPanel) getParent();
        if (this.point.x + dx < 0 || this.point.x + ballSize + dx > panel.getWidth()) {
            dx = -dx;
        }

        if (this.point.y + dy < 0 || this.point.y + ballSize + dy >= panel.getHeight()) {
            dy = -dy;
        }

        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(randomColor);
        g2d.fillOval(1, 1, ballSize, ballSize);
    }

    @Override
    public void run() {
        try {
            while (true) {
                Optional<Ball> ballLapping = isBallLapping2(this);
                if (ballLapping.isPresent()) {
                    BallsPanel ballsPanel = (BallsPanel) this.getParent();

                    ballsPanel.remove(this);
//                    BallsDispatcher.removeBall(this);

                    ballsPanel.changeBall(ballLapping.get(), this);

                    ballsPanel.repaint();
                    stop();
                    return;
                }
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void stop() {
        Thread.currentThread().interrupt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return ballSize == ball.ballSize && dx == ball.dx && dy == ball.dy && Float.compare(ball.r, r) == 0 && Float.compare(ball.g, g) == 0 && Float.compare(ball.b, b) == 0 && Objects.equals(random, ball.random) && Objects.equals(randomColor, ball.randomColor) && Objects.equals(point, ball.point);
    }

    @Override
    public int hashCode() {
        return Objects.hash(random, ballSize, dx, dy, r, g, b, randomColor, point);
    }
}
