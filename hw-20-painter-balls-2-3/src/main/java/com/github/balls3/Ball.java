package com.github.balls3;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private final Random random = new Random();
    private final int ballSize;
    private int dx;
    private int dy;
    private final float r = random.nextFloat();
    private final float g = random.nextFloat();
    private final float b = random.nextFloat();
    private final Color randomColor;
    private final Point point;

    boolean running = true;

    public Ball(Point point) {
        this.point = point;
        this.ballSize = random.nextInt(Math.min(600, 800) / 8);
        this.randomColor = new Color(r, g, b);
        this.dx = this.random.nextInt(10) - 3;
        this.dy = this.random.nextInt(10) - 3;
        setSize(ballSize + 10, ballSize + 10);
        setOpaque(Boolean.FALSE);
    }

    public int getBallSize() {
        return ballSize;
    }

    public Point getPoint() {
        return point;
    }

    private void move() {
        JPanel panel = (JPanel) getParent();
        if (this.point.x + dx < 0 || this.point.x + ballSize + dx > panel.getWidth()) {
            dx = -dx;
        }

        if (this.point.y + dy < 0 || this.point.y + ballSize + dy >= panel.getHeight()) {
            dy = -dy;
        }

        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(randomColor);
        g2d.fillOval(1, 1, ballSize, ballSize);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

