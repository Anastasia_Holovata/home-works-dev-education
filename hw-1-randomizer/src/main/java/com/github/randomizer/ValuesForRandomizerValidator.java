package com.github.randomizer;

public class ValuesForRandomizerValidator {

    public static void validate(int min, int max) {
        if (min < 1) {
            throw new IllegalArgumentException("You entered too small value! Increase the minimum!");
        }
        if (max > 500) {
            throw new IllegalArgumentException("You entered too big value! Decrease the maximum!");
        }
        if (min > max) {
            throw new IllegalArgumentException("Your min value is bigger than max value!");
        }
        System.out.println("The minimum equals " + min + ";\nThe maximum equals " + max + ".\n");
    }
}
