package com.github.randomizer;

public class Menu {

    public static void processMenu() {
        System.out.print("Enter the minimum value: ");
        int minRandomValue = InputHelper.getIntValueFromInput();

        System.out.print("Enter the maximum value: ");
        int maxRandomValue = InputHelper.getIntValueFromInput();

        ValuesForRandomizerValidator.validate(minRandomValue, maxRandomValue);

        int userChoice;
        do {
            MenuHelper.showMenu();
            userChoice = InputHelper.getIntValueFromInput();

            switch (userChoice) {
                case 1:
                    MenuCommands.generate(minRandomValue, maxRandomValue);
                    break;
                case 2:
                    MenuCommands.help();
                    break;
                case 3:
                    MenuCommands.exit();
                    break;
            }

        } while (true);
    }
}
