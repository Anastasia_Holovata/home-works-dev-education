package com.github.randomizer;

import java.util.HashSet;
import java.util.Set;

public class MenuCommands {

    private static final Set<Integer> uniqueRandomValues = new HashSet<>();

    public static void generate(int minRandomValue, int maxRandomValue) {
        System.out.println(MathRandom.generateRandomUniqueIntValue(minRandomValue, maxRandomValue, uniqueRandomValues));
    }

    public static void exit() {
        do {
            System.out.print("\nAre you sure that you want to quit the app? [yes:1 | no:0]: ");
            int userChoose = InputHelper.getIntValueFromInput();

            if (userChoose == 1) {
                System.out.println("The game is over");
                System.exit(0);
            }
        } while (true);
    }

    public static void help() {
        String helper = "Randomizer is a program, that has been created for people, who want get a number (or some) " +
                "by random way.\nThere are some commands:\n - generate() - user calls this command and then input " +
                "the minimum and maximum values. By random way, the program returns you accidental value within adjusted limits.\n - exit() - " +
                "user calls this command to stop generating values. The user also gets message with a question. " +
                "If user says Yes, the program stops. Otherwise, the game is being continued.";
        System.out.println(helper);
    }
}
