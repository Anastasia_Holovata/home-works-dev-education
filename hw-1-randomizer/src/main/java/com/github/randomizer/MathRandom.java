package com.github.randomizer;

import java.util.Random;
import java.util.Set;

public class MathRandom {

    private static int counterOfGeneratedValues = 0;

    private static final Random RANDOM = new Random();

    public static int generateRandomIntValue(int min, int max) {
        return RANDOM.nextInt(max - min + 1) + min;
    }

    public static int generateRandomUniqueIntValue(int min, int max, Set<Integer> uniqueRandomValues) {
        int diff = max - min;

        while (true) {
            int generatedRandomValue = MathRandom.generateRandomIntValue(min, max);

            if (!uniqueRandomValues.contains(generatedRandomValue)) {
                uniqueRandomValues.add(generatedRandomValue);
                counterOfGeneratedValues++;
                return generatedRandomValue;
            } else if (counterOfGeneratedValues >= diff + 1) {
                throw new RuntimeException("You have already generated all of available values!");
            }
        }
    }
}
