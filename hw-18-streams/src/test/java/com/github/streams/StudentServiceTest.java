package com.github.streams;

import com.github.streams.entity.Student;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    @org.mockito.Mock
    public StudentsRepository studentsRepository;

    @InjectMocks
    public StudentService studentService;

    @Before
    public void setup() {
        when(studentsRepository.getAll()).thenReturn(getMockListStudents());
    }

    @Test
    public void getStudentsByFacMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro",
                "0999997653", "Software Engineering", "2", "SE-2-19"));

        exp.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro",
                "0958794525", "Software Engineering", "3", "SE-3"));

        exp.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro", "0501239845", "Software Engineering",
                "2", "SE-2-19"));

        exp.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro", "0683452617", "Software Engineering",
                "2", "SE-2-19"));

        List<Student> act = studentService.getStudentsByFac("Software Engineering");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByFacOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro", "0501239845", "Management",
                "2", "MN-2-19"));
        List<Student> act = studentService.getStudentsByFac("Management");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByFacNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studentService.getStudentsByFac("");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByFacAndCourseMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro", "0999997653", "Software Engineering",
                "2", "SE-2-19"));

        exp.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro", "0501239845", "Software Engineering",
                "2", "SE-2-19"));

        exp.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro", "0683452617", "Software Engineering",
                "2", "SE-2-19"));

        List<Student> act = studentService.getStudentsByFacAndCourse("Software Engineering", "2");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByFacAndCourseOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro", "0994567234", "Law and juristic",
                "4", "LJ-1-17"));

        List<Student> act = studentService.getStudentsByFacAndCourse("Law and juristic", "4");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByFacAndCourseNull() {
        List<Student> exp = new ArrayList<>();

        List<Student> act = studentService.getStudentsByFacAndCourse("", "");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsBornAfterMany() {
        List<Student> exp = new ArrayList<>();

        exp.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro", "0958794525", "Software Engineering",
                "3", "SE-3"));

        exp.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro", "0501239845", "Software Engineering",
                "2", "SE-2-19"));

        exp.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro", "0501239845", "Management",
                "2", "MN-2-19"));

        exp.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev", "0501239845", "International Pravo",
                "2", "IP-1-19"));

        exp.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro", "0505677685", "International Relationships",
                "2", "IR-2-20"));

        exp.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro", "0683452617", "Software Engineering",
                "2", "SE-2-19"));

        List<Student> act = studentService.getStudentsBornAfter(2000);
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsBornAfterNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studentService.getStudentsBornAfter(2002);
        assertEquals(exp, act);
    }

    @Test
    public void getDefinedStudentByYear() {
        List<Student> exp = new ArrayList<>();

        exp.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro", "0501239845", "Software Engineering",
                "2", "SE-2-19"));

        List<Student> act = studentService.getDefinedStudentByYear(2001);
        assertEquals(exp, act);
    }

    @Test
    public void getDefinedStudentByYearNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studentService.getDefinedStudentByYear(2002);
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByGroupMany() {
        List<String> exp = new ArrayList<>();
        exp.add("Tkachenko Mariia");
        exp.add("Holovata Anastasia");
        exp.add("Solomenniy Andrey");

        List<String> act = studentService.getStudentsByGroup("SE-2-19");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByGroupOne() {
        List<String> exp = new ArrayList<>();
        exp.add("Shevchenko Taras");

        List<String> act = studentService.getStudentsByGroup("IR-1-14");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByGroupNull() {
        List<String> exp = new ArrayList<>();

        List<String> act = studentService.getStudentsByGroup("SUPERSTAR");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsByGroupEmpty() {
        List<String> exp = new ArrayList<>();

        List<String> act = studentService.getStudentsByGroup("");
        assertEquals(exp, act);
    }

    @Test
    public void getCountStudentsOfFacultyMany() {
        long exp = 3;
        long act = studentService.getCountStudentsOfFaculty("International Relationships");
        assertEquals(exp, act);
    }

    @Test
    public void getCountStudentsOfFacultyOne() {
        long exp = 1;
        long act = studentService.getCountStudentsOfFaculty("International Pravo");
        assertEquals(exp, act);
    }

    @Test
    public void getCountStudentsOfFacultyNull() {
        long act = studentService.getCountStudentsOfFaculty("International Engineering");
        assertEquals(0, act);
    }

    @Test
    public void getCountStudentsOfFacultyEmpty() {
        long act = studentService.getCountStudentsOfFaculty("");
        assertEquals(0, act);
    }

    @Test
    public void changeFacultyMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Ivan", "Ivanov", 1999,
                "Dnipro", "0961234567", "Ukrainian Relationships", "3",
                "IR-3-18"));

        exp.add(new Student(4, "Taras", "Shevchenko", 1995, "Dnipro", "0983673556", "Ukrainian Relationships",
                "7", "IR-1-14"));

        exp.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro", "0505677685", "Ukrainian Relationships",
                "2", "IR-2-20"));
        List<Student> act = studentService.changeFaculty("International Relationships", "Ukrainian Relationships");
        assertEquals(exp, act);
    }

    @Test
    public void changeFacultyOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro", "0994567234", "Software Engineering",
                "4", "LJ-1-17"));
        List<Student> act = studentService.changeFaculty("Law and juristic", "Software Engineering");
        assertEquals(exp, act);
    }

    @Test
    public void changeFacultyNull() {
        List<Student> exp = new ArrayList<>();

        List<Student> act = studentService.changeFaculty("Law and dinosaurs", "Software Engineering");
        assertEquals(exp, act);
    }

    @Test
    public void changeGroupMany() {
        List<Student> exp = new ArrayList<>();

        exp.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro", "0999997653", "Software Engineering",
                "2", "SE-1-19"));

        exp.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro", "0501239845", "Software Engineering",
                "2", "SE-1-19"));

        exp.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro", "0683452617", "Software Engineering",
                "2", "SE-1-19"));

        List<Student> act = studentService.changeGroup("SE-2-19", "SE-1-19");
        assertEquals(exp, act);
    }

    @Test
    public void changeGroupOne() {
        List<Student> exp = new ArrayList<>();

        exp.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev", "0501239845", "International Pravo",
                "2", "NEW_GROUP"));

        List<Student> act = studentService.changeGroup("IP-1-19", "NEW_GROUP");
        assertEquals(exp, act);
    }

    @Test
    public void changeGroupNull() {
        List<Student> exp = new ArrayList<>();

        List<Student> act = studentService.changeGroup("IP-2-19", "NEW_GROUP");
        assertEquals(exp, act);
    }

    @Test
    public void getCountOfStudentsWithFind() {
        long exp = 1;
        long act = studentService.getCountOfStudentsWithFind("International Relationships");
        assertEquals(exp, act);
    }

    @Test
    public void getStudentsToString() {
        String exp =
                "Student{id=1, firstName='Ivan', lastName='Ivanov', yearOfBirth=1999, address='Dnipro', " +
                        "telephone='0961234567', faculty='International Relationships', course='3', group='IR-3-18'}" +
                        "Student{id=2, firstName='Mariia', lastName='Tkachenko', yearOfBirth=2000, address='Dnipro', " +
                        "telephone='0999997653', faculty='Software Engineering', course='2', group='SE-2-19'}" +
                        "Student{id=3, firstName='Iliia', lastName='Iliyin', yearOfBirth=2001, address='Dnipro', " +
                        "telephone='0958794525', faculty='Software Engineering', course='3', group='SE-3'}" +
                        "Student{id=4, firstName='Taras', lastName='Shevchenko', yearOfBirth=1995, address='Dnipro', " +
                        "telephone='0983673556', faculty='International Relationships', course='7', group='IR-1-14'}" +
                        "Student{id=5, firstName='Anastasia', lastName='Holovata', yearOfBirth=2002, address='Dnipro', " +
                        "telephone='0501239845', faculty='Software Engineering', course='2', group='SE-2-19'}" +
                        "Student{id=6, firstName='Daria', lastName='Volkova', yearOfBirth=2002, address='Dnipro', " +
                        "telephone='0501239845', faculty='Management', course='2', group='MN-2-19'}" +
                        "Student{id=7, firstName='Katerina', lastName='Ionova', yearOfBirth=2002, address='Kiev', " +
                        "telephone='0501239845', faculty='International Pravo', course='2', group='IP-1-19'}" +
                        "Student{id=8, firstName='Katerina', lastName='Sonkina', yearOfBirth=2002, address='Dnipro', " +
                        "telephone='0505677685', faculty='International Relationships', course='2', group='IR-2-20'}" +
                        "Student{id=9, firstName='Maria', lastName='Volkova', yearOfBirth=2000, address='Dnipro', " +
                        "telephone='0994567234', faculty='Law and juristic', course='4', group='LJ-1-17'}" +
                        "Student{id=10, firstName='Andrey', lastName='Solomenniy', yearOfBirth=2001, address='Dnipro', " +
                        "telephone='0683452617', faculty='Software Engineering', course='2', group='SE-2-19'}";

        String act = studentService.getStudentsToString();
        assertEquals(exp, act);
    }

    @Test
    public void groupByFaculty() {
        List<Student> list1 = new ArrayList<>();

        list1.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro",
                "0999997653", "Software Engineering", "2", "SE-2-19"));
        list1.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro",
                "0958794525", "Software Engineering", "3", "SE-3"));
        list1.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro",
                "0501239845", "Software Engineering", "2", "SE-2-19"));
        list1.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro",
                "0683452617", "Software Engineering", "2", "SE-2-19"));

        List<Student> list2 = new ArrayList<>();

        list2.add(new Student(1, "Ivan", "Ivanov", 1999,
                "Dnipro", "0961234567", "International Relationships", "3",
                "IR-3-18"));
        list2.add(new Student(4, "Taras", "Shevchenko", 1995, "Dnipro",
                "0983673556", "International Relationships", "7", "IR-1-14"));
        list2.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro",
                "0505677685", "International Relationships", "2", "IR-2-20"));

        List<Student> list3 = new ArrayList<>();

        list3.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro",
                "0501239845", "Management", "2", "MN-2-19"));

        List<Student> list4 = new ArrayList<>();
        list4.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro",
                "0994567234", "Law and juristic", "4", "LJ-1-17"));

        List<Student> list5 = new ArrayList<>();

        list5.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev",
                "0501239845", "International Pravo", "2", "IP-1-19"));

        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("Software Engineering", list1);
        exp.put("International Relationships", list2);
        exp.put("Management", list3);
        exp.put("Law and juristic", list4);
        exp.put("International Pravo", list5);
        Map<String, List<Student>> act = studentService.groupByFaculty();

        assertEquals(exp, act);
    }

    @Test
    public void groupByCourse() {
        List<Student> list1 = new ArrayList<>();

        list1.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro",
                "0999997653", "Software Engineering", "2", "SE-2-19"));
        list1.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro",
                "0501239845", "Software Engineering", "2", "SE-2-19"));
        list1.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro",
                "0501239845", "Management", "2", "MN-2-19"));
        list1.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev",
                "0501239845", "International Pravo", "2", "IP-1-19"));
        list1.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro",
                "0505677685", "International Relationships", "2", "IR-2-20"));
        list1.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro",
                "0683452617", "Software Engineering", "2", "SE-2-19"));

        List<Student> list2 = new ArrayList<>();

        list2.add(new Student(1, "Ivan", "Ivanov", 1999,
                "Dnipro", "0961234567", "International Relationships", "3",
                "IR-3-18"));
        list2.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro",
                "0958794525", "Software Engineering", "3", "SE-3"));

        List<Student> list3 = new ArrayList<>();

        list3.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro",
                "0994567234", "Law and juristic", "4", "LJ-1-17"));

        List<Student> list4 = new ArrayList<>();

        list4.add(new Student(4, "Taras", "Shevchenko", 1995, "Dnipro",
                "0983673556", "International Relationships", "7", "IR-1-14"));

        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("2", list1);
        exp.put("3", list2);
        exp.put("4", list3);
        exp.put("7", list4);
        Map<String, List<Student>> act = studentService.groupByCourse();

        assertEquals(exp, act);
    }

    @Test
    public void groupByGroup() {
        List<Student> list1 = new ArrayList<>();

        list1.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro",
                "0994567234", "Law and juristic", "4", "LJ-1-17"));

        List<Student> list2 = new ArrayList<>();

        list2.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev",
                "0501239845", "International Pravo", "2", "IP-1-19"));

        List<Student> list3 = new ArrayList<>();

        list3.add(new Student(1, "Ivan", "Ivanov", 1999,
                "Dnipro", "0961234567", "International Relationships", "3",
                "IR-3-18"));

        List<Student> list4 = new ArrayList<>();

        list4.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro",
                "0501239845", "Management", "2", "MN-2-19"));

        List<Student> list5 = new ArrayList<>();

        list5.add(new Student(4, "Taras", "Shevchenko", 1995, "Dnipro",
                "0983673556", "International Relationships", "7", "IR-1-14"));

        List<Student> list6 = new ArrayList<>();

        list6.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro",
                "0505677685", "International Relationships", "2", "IR-2-20"));

        List<Student> list7 = new ArrayList<>();

        list7.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro",
                "0999997653", "Software Engineering", "2", "SE-2-19"));
        list7.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro",
                "0501239845", "Software Engineering", "2", "SE-2-19"));
        list7.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro",
                "0683452617", "Software Engineering", "2", "SE-2-19"));

        List<Student> list8 = new ArrayList<>();

        list8.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro",
                "0958794525", "Software Engineering", "3", "SE-3"));

        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("LJ-1-17", list1);
        exp.put("IP-1-19", list2);
        exp.put("IR-3-18", list3);
        exp.put("MN-2-19", list4);
        exp.put("IR-1-14", list5);
        exp.put("IR-2-20", list6);
        exp.put("SE-2-19", list7);
        exp.put("SE-3", list8);
        Map<String, List<Student>> act = studentService.groupByGroup();

        assertEquals(exp, act);
    }

    @Test
    public void areAllStudentsAtFaculty() {
        assertFalse(studentService.areAllStudentsAtFaculty("Software Engineering"));
    }

    @Test
    public void isStudentAtFaculty() {
        assertTrue(studentService.isStudentAtFaculty("Software Engineering"));
        assertFalse(studentService.isStudentAtFaculty("NEMA"));
    }

    @Test
    public void areAllStudentsInGroup() {
        assertFalse(studentService.areAllStudentsInGroup("IR-1-14"));
    }

    @Test
    public void isStudentsInGroup() {
        assertTrue(studentService.isStudentsInGroup("IR-1-14"));
        assertFalse(studentService.isStudentsInGroup("NEMA"));
    }

    private List<Student> getMockListStudents() {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student(1, "Ivan", "Ivanov", 1999,
                "Dnipro", "0961234567", "International Relationships", "3",
                "IR-3-18"));

        studentList.add(new Student(2, "Mariia", "Tkachenko", 2000, "Dnipro",
                "0999997653", "Software Engineering",
                "2", "SE-2-19"));

        studentList.add(new Student(3, "Iliia", "Iliyin", 2001, "Dnipro",
                "0958794525", "Software Engineering",
                "3", "SE-3"));

        studentList.add(new Student(4, "Taras", "Shevchenko", 1995, "Dnipro",
                "0983673556", "International Relationships",
                "7", "IR-1-14"));

        studentList.add(new Student(5, "Anastasia", "Holovata", 2002, "Dnipro",
                "0501239845", "Software Engineering",
                "2", "SE-2-19"));

        studentList.add(new Student(6, "Daria", "Volkova", 2002, "Dnipro",
                "0501239845", "Management",
                "2", "MN-2-19"));

        studentList.add(new Student(7, "Katerina", "Ionova", 2002, "Kiev",
                "0501239845", "International Pravo",
                "2", "IP-1-19"));

        studentList.add(new Student(8, "Katerina", "Sonkina", 2002, "Dnipro",
                "0505677685", "International Relationships",
                "2", "IR-2-20"));

        studentList.add(new Student(9, "Maria", "Volkova", 2000, "Dnipro",
                "0994567234", "Law and juristic",
                "4", "LJ-1-17"));

        studentList.add(new Student(10, "Andrey", "Solomenniy", 2001, "Dnipro",
                "0683452617", "Software Engineering",
                "2", "SE-2-19"));

        return studentList;
    }
}