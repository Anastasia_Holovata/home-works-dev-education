package com.github.streams;

import com.github.streams.entity.Student;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentService {

    private final StudentsRepository studentsRepository;

    public StudentService(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    public List<Student> getStudents() {
        return studentsRepository.getAll();
    }

    public List<Student> getStudentsByFac(String faculty) {
        return getStudents().stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public List<Student> getStudentsByFacAndCourse(String faculty, String course) {
        return getStudents().stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .filter(student -> student.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public List<Student> getStudentsBornAfter(int year) {
        return getStudents().stream()
                .filter(student -> student.getYearOfBirth() > year)
                .collect(Collectors.toList());
    }

    public List<Student> getDefinedStudentByYear(int year) {
        return getStudents().stream()
                .filter(student -> student.getYearOfBirth() > year)
                .findFirst()
                .stream().collect(Collectors.toList());
    }

    public List<String> getStudentsByGroup(String group) {
        return getStudents().stream()
                .filter(student -> student.getGroup().equals(group))
                .flatMap(student -> Stream.of(student.getLastName() + " " + student.getFirstName()))
                .collect(Collectors.toList());
    }

    public long getCountStudentsOfFaculty(String faculty) {
        return getStudents().stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .count();
    }

    public List<Student> changeFaculty(String lastFac, String newFac) {
        return getStudents().stream()
                .filter(student -> student.getFaculty().equals(lastFac))
                .map(student -> {
                    student.setFaculty(newFac);
                    return student;
                }).collect(Collectors.toList());
    }

    public List<Student> changeGroup(String lastGroup, String newGroup) {
        return getStudents().stream()
                .filter(student -> student.getGroup().equals(lastGroup))
                .map(student -> {
                    student.setGroup(newGroup);
                    return student;
                }).collect(Collectors.toList());
    }

    public long getCountOfStudentsWithFind(String faculty) {
        return getStudents().stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .findAny().stream().count();
    }

    public String getStudentsToString() {
        return getStudents().stream()
                .flatMap(student -> Stream.of(student.toString()))
                .reduce("", (x, y) -> x + y);
    }

    public Map<String, List<Student>> groupByFaculty() {
        return getStudents().stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
    }

    public Map<String, List<Student>> groupByCourse() {
        return getStudents().stream()
                .collect(Collectors.groupingBy(Student::getCourse));
    }

    public Map<String, List<Student>> groupByGroup() {
        return getStudents().stream()
                .collect(Collectors.groupingBy(Student::getGroup));
    }

    public boolean areAllStudentsAtFaculty(String faculty) {
        return getStudents().stream().allMatch(student -> student.getFaculty().equals(faculty));
    }

    public boolean isStudentAtFaculty(String faculty) {
        return getStudents().stream()
                .anyMatch(student -> student.getFaculty().equals(faculty));
    }

    public boolean areAllStudentsInGroup(String group) {
        return getStudents().stream()
                .allMatch(student -> student.getGroup().equals(group));
    }

    public boolean isStudentsInGroup(String group) {
        return getStudents().stream()
                .anyMatch(student -> student.getGroup().equals(group));
    }

}
