package com.github.streams;

import com.github.streams.entity.Student;

import java.util.List;

public interface StudentsRepository {
    List<Student> getAll();
}
